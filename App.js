
import React from 'react';
import MainNavigator from './src/navigators/MainNavigator';
import store from './src/redux/store/store';
import { Provider } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native'
import { LogBox, Platform } from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import {View} from 'react-native';
const App = () => {

  LogBox.ignoreAllLogs()
  Platform.OS=="android"? SplashScreen.hide():null
 
  return (
    <View style={{flex:1,backgroundColor:'#000'}}>
     <Provider store={store}>
       <NavigationContainer>
       <MainNavigator/>
      </NavigationContainer>
     </Provider>
     </View>
  );
};


export default App;
