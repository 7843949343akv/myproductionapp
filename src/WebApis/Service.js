// import {  useSelector, useDispatch } from 'react-redux';
// import * as types  from '../redux/types';
export const baseUrl = 'https://www.nileprojects.in/my_production_assistant/'
//

export const paymentUrl = 'https://www.nileprojects.in/my_production_assistant/subscription/plan/'

//API END POINT LISTS

export const register = 'register'
export const login = 'login'
export const get_subscription_plan = 'get-subscription-plan'
export const get_subscription_status = 'get-subscription-status'
export const add_worker = 'add-worker'
export const save_favorite_color = 'save-favorite-color'
export const remove_favorite_color = 'remove-favorite-color'
export const favorite_color_list = 'favorite-color-list'
export const my_profile = 'my-profile'
export const change_status_of_worker = 'change-status-of-worker'
export const regularization_request = 'regularization-request'
export const regularization_request_list = 'regularization-request-list?worker_id='
export const change_status_request = 'change-status-request'
export const check_in_and_check_out = 'check-in-and-check-out'
export const get_worker_list = 'get-worker-list'
export const worker_attendance_list = 'worker-attendance-list'
export const add_task = 'add-task'
export const get_task_list = 'get-task-list'
export const save_favorite_fixcer = 'save-favorite-fixcer'
export const favorite_fixcer_list = 'favorite-fixcer-list'
export const remove_favorite_fixcer = 'remove-favorite-fixcer?favorite_fixcer_id='
export const get_attendance_detail ='get-attendance-detail?date='
export const remove_worker = 'remove-worker'
export const buy_subscription = 'buy-subscription'
export const success_token= 'success?token=' //
export const paymentRes= 'success' //
export const forgot_password= 'forgot_password'
export const complete_task= 'complete-task'
export const get_assign_task_list= 'get-assign-task-list'
export const change_password= 'change-password' 
export const submit_job_type= 'submit-job-type'
export const delete_job_type= 'delete-job-type'
export const get_job_type= 'get-job-type' 
export const download_task_list= 'download-task-list' 
export const purchage_subscription= 'purchage-subscription'

export const requestPostApi = async (endPoint,body,method,token) => 
{
  console.log('the token is :-',token)
  var header = {}
  if(token!='' && token!=undefined)
  {
    header = {'Content-Type': 'multipart/form-data','Accept':'application/json','Authorization': 'Bearer '+ token,'Cache-Control': 'no-cache'
  }
  }else{
    header = {}
  }

  var url = baseUrl + endPoint
  console.log('post Request Url:-' + url + '\n')
  console.log(body)
  // console.log(header + '\n')

  try {
      let response = await fetch(url, {
        method: method,
        body:body,
        headers:header,
      }
      )
      let code = await response.status 
      console.log('API responce is',response)
      if(code==200){
        let responseJson = await response.json();
        console.log( responseJson)
        return {responseJson:responseJson,err:null}
      }else if(code == 400 || code == 402)
      {
        let responseJson = await response.json();
        //Completion block 
        return {responseJson:null,err:responseJson.message}
      }else{
        return {responseJson:null,err:'Something went wrong!'}
      }
    } catch (error) {
      console.log('the error is',error)
     return {responseJson:null,err:'Something Went Wrong! Please check your internet connection.'}
      
    }
  }

  export const requestGetApi = async (endPoint,body,method,token) => 
{
  console.log('the token is :-',token)
  var header = {}
  var url = baseUrl + endPoint

  if(token!='' && token!=undefined)
  {
    header = {'Content-Type': 'multipart/form-data','Accept':'application/json','Authorization': 'Bearer '+ token,'Cache-Control': 'no-cache'}
  }else{
    header = {}
  }

   //url = url + objToQueryString(body)
  console.log('Request Url:-' + url + '\n')
  try {
      let response = await fetch(url, {
        method: method,
        headers:header,
      }
      )
      let code = await response.status
      console.log(code)   
      if(code==200){
        let responseJson = await response.json();
        console.log('Code 200==>>',responseJson)
        return {responseJson:responseJson,err:null,code:code}
      }else if(code == 400)
      {
        return {responseJson:null,err:responseJson.message,code:code}

      }else if(code == 500)
      {
        console.log(response)   

        return {responseJson:null,err:'Something Went Wrong',code:code}

      }else{
        console.log(response)   

        return {responseJson:null,err:'Something went wrong!',code:code}
      }
    } catch (error) {
      console.error(error);
      return {responseJson:null,err:'Something Went Wrong! Please check your internet connection.',code:500}
      
    }
  }

  export const requestPostApiMedia = async (endPoint,formData,method,token) => 
{
  var header = {}
 
  if(token!='' && token!=undefined)
  {
    header = {'Content-type': 'multipart/form-data','apitoken':token,'Cache-Control': 'no-cache'
  }
  }else{
    if(endPoint != signUpApi){
      header = { 'Content-type': 'multipart/form-data' , 'Cache-Control': 'no-cache'
    }
  }
  }

  var url = baseUrl + endPoint
  console.log('Request Url:-' + url + '\n')
  console.log(formData + '\n')

  try {
      let response = await fetch(url, {
        method: method,
        body:formData,
        
        headers:header,
       
      }
      )

      let code = await response.status
      console.log(code )   

      if(code==200){
        let responseJson = await response.json();
        console.log( responseJson)
        return {responseJson:responseJson,err:null}
      }else if(code == 400)
      {
        let responseJson = await response.json();
        return {responseJson:null,err:responseJson.message}

      }else{

        return {responseJson:null,err:'Something went wrong!'}
      }
    } catch (error) {
      console.error('the error of the uploade image is ==>>',error);
      return {responseJson:null,err:'Something Went Wrong! Please check your internet connection.'}
      
    }
  }

  export const requestPostApiSignUp = async (endPoint,formData,method) => 
  {
    var url = baseUrl + endPoint
    console.log('Request Url:-' + url + '\n')
    console.log(formData + '\n')
  
    try {
        let response = await fetch(url, {
          method: method,
          body:formData,
        }
        )
  
        let code = await response.status
        console.log(code )   
  
        if(code==200){
          let responseJson = await response.json();
          console.log( responseJson)
          return {responseJson:responseJson,err:null}
        }else if(code == 400 || 402)
        {
          let responseJson = await response.json();
          console.log( responseJson)

          return {responseJson:null,err:responseJson.msg}
  
        }else{
  
          return {responseJson:null,err:'Something went wrong!'}
        }
      } catch (error) {
  
        return {responseJson:null,err:'Something Went Wrong! Please check your internet connection.'}
        console.error(error);
      }
    }  
  
  const objToQueryString=(obj)=> {

    const keyValuePairs = [];
    for (const key in obj) {
      keyValuePairs.push(encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]));
    }
    return keyValuePairs.length==0 ? '' :  '?'+  keyValuePairs.join('&');
  }