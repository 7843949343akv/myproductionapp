import React, { Component } from 'react';
import { Alert, Text, Image,StatusBar, ScrollView, ToastAndroid, Platform, TextInput, View, StyleSheet, TouchableOpacity, ActivityIndicator, Keyboard, Dimensions, FlatList ,BackHandler} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage'
import { SafeAreaView } from 'react-native-safe-area-context';
import { DrawerContentScrollView,DrawerItem } from '@react-navigation/drawer';
import { useDispatch } from 'react-redux';
import * as types  from '../redux/types';

var width = Dimensions.get('window').width
var height = Dimensions.get('window').height
const MyView=(props)=>{
  return(
    <>
   <TouchableOpacity style={{width:'98%',height:50,backgroundColor:'#1b2c5b',flexDirection:'row',alignItems:'center',marginTop:10}}onPress={props.touch}>
    <Image source={props.img} style={{width:28,height:28,left:18}}></Image>
    <Text style={{color:'white',fontSize:15,marginLeft:'15%'}}>{props.name}</Text>
    <View style={{position:'absolute',right:8}}>
    {/* <Image source={require('../assets/arrow3.png')} style={{width:20,height:20}}></Image> */}
    </View>
   </TouchableOpacity>
   </>
  );
}
const CoustomDrawer = (props)=>{
  const dispatch =  useDispatch();
    return (
      <SafeAreaView style={{flex:1}}>
    
<ScrollView>

  
   {/* <MyView name="Home" touch={()=>{props.navigation.navigate('Home')}} img={require('../assets/home4.png')}/> 
  <MyView name="Account" touch={()=>{props.navigation.navigate('Profile')  }} img={require('../assets/profile.png')}/>
 <MyView name="Change Password" touch={()=>{props.navigation.navigate('ChangPass')}} img={require('../assets/password-1.png')}/>
<MyView name="Contact Us" touch={()=>{props.navigation.navigate('ContactUs')}} img={require('../assets/contact-us.png')}/>
<MyView name="Terms & Conditions" touch={()=>{props.navigation.navigate('TermCondition',{key:'1'})}} img={require('../assets/T&C.png')}/>
<MyView name="Privacy Policy" touch={()=>{props.navigation.navigate('PrivacyPolicy')}} img={require('../assets/privacy-policy.png')}/> */}

<MyView name="Logout" touch={
 async() =>{
Alert.alert(
  'Logout',
  'Are you sure want to logout?',
  [
  {
    text: 'Cancel',
    onPress: () => console.log('Cancel Pressed'),
    style: 'cancel'
  },
  {
    text: 'OK',
    onPress: () => {
      AsyncStorage.removeItem('userdata'); 
      dispatch({
        type: types.LOADING,
        isLoading:false
    });
  }
  }
  ],
  {
  cancelable: false
  }
);}} img={require('../assets/logout.png')}/>

</ScrollView>

   

      </SafeAreaView>
    );
   
  }

  
export default CoustomDrawer;