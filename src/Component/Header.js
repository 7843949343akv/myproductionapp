import React from 'react'
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Text,
  ScrollView,
  Dimensions,
  View,
  Image,
  Alert,
} from 'react-native';
import {TouchableOpacity } from 'react-native-gesture-handler';
import { dimensions , MyColor} from '../utils/constants';
var width = Dimensions.get('window').width
var height = Dimensions.get('window').height
    // const common = useSelector(state => state.apiReducer); //reducer name from store
    // const listReducer = useSelector(state => state.listReducer);



// const Home = (props) => {
const Header = (props) => {

 
  return (
       <View style={{width:'100%',height:55,backgroundColor:MyColor.HEADERCOLOR,alignSelf:'center',justifyContent:'center',borderBottomLeftRadius:20,borderBottomRightRadius:20}}>
             <Text style={{fontWeight:'600',fontSize:16,color:MyColor.TEXTCOLOR,textAlign:'center',position:'absolute',alignSelf:'center'}}>{props.Lable}</Text>

      <View style={{left:10,top:10,height:55,width:27}}>
          <TouchableOpacity onPress={props.Nav}  style={{width:27,height:35,zIndex:999,backgroundColor:MyColor.BUTTONCOLOR,paddingHorizontal:19,borderRadius:20}}>
      <Image source={require('../assets/left-arrow.png')}style={{width:20,height:20,alignSelf:'center',zIndex:-999,top:8}}resizeMode='cover'></Image>
      </TouchableOpacity>
      </View>
       </View>
  );
};

let styles = StyleSheet.create({
    container:{
      flex: 1,
   
    },
 
})

export default Header;
