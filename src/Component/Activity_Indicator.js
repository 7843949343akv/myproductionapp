import React from 'react'
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Text,
  ScrollView,
  Dimensions,
  View,
  Image,
  Alert,
  ActivityIndicator
} from 'react-native';
import {TouchableOpacity } from 'react-native-gesture-handler';
import { dimensions , MyColor} from '../utils/constants';
var width = Dimensions.get('window').width
var height = Dimensions.get('window').height

const Activity_Indicator = (props) => {

 
  return (
      <View
          style={[
          StyleSheet.absoluteFill,
          { backgroundColor: 'rgba(0, 0, 0, 0.6)', justifyContent: 'center' }
          ]}
          >
              <ActivityIndicator color={"red"} size={40} />
      </View> 
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
   
    },
 
})

export default Activity_Indicator;
