import 'react-native-gesture-handler';
import React from  'react' ;
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator, StackView } from '@react-navigation/stack'

import Login from '../screens/Login';
import Signup from '../screens/Signup';
import Subscription from '../screens/Subscription'
import MyWebView from '../screens/MyWebView';


const LoginStack = (props) => {


    const Stack = createStackNavigator();
    return(
       
            <Stack.Navigator>
                    <Stack.Screen
                    component = {Login}
                    name="Login"
                    options={{
                        headerShown:false,
                        headerTitle:"Login",
                        headerTintColor:'white',
                        headerTitleAlign:'center',
                        headerStyle: {
                           // height:40,
                            backgroundColor: '#2C684F',
                        },
                    }}
                
                />

                <Stack.Screen
                    component = {Signup}
                    name="Signup"
                    options={{
                        headerShown:false,
                        headerTitle:"Signup",
                        headerTintColor:'white',
                        headerTitleAlign:'center',
                          headerStyle: {
                           // height:40,
                            backgroundColor: '#2C684F',
                        },
                    }}
                
                />
               <Stack.Screen
                    component = {Subscription}
                    name="Subscription"
                    options={{
                        headerShown:false,
                        headerTitleAlign:'center',
                          headerStyle: {
                           // height:40,
                            backgroundColor: '#2C684F',
                        },
                    }}
                
                />
               <Stack.Screen
                    component = {MyWebView}
                    name="MyWebView"
                    options={{
                        headerShown:false,
                        headerTitleAlign:'center',
                          headerStyle: {
                           // height:40,
                            backgroundColor: '#2C684F',
                        },
                    }}
                
                />
                
            </Stack.Navigator>

    )
}

export default LoginStack;
