import React, { useEffect, useState } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import BottomNavigator from './BottomNavigator';
import MainStackNav from './MainStackNav'
import Splash from './Splash';
import {View ,StyleSheet,ActivityIndicator } from 'react-native';
import {  useSelector, useDispatch } from 'react-redux';
import {getAsyncStorage,saveUserToken, setLoading,saveUserResult} from '../redux/actions/user_action';

    const MainNavigator =() => {


     const [showSplash , setShowSplash ]  = useState(true);
     const dispatch =  useDispatch();
     const isSignedIn  = useSelector(state => state.common.loading)
     const getAllValues = async() => {
         const user = await AsyncStorage.getItem("user");
         const token = await AsyncStorage.getItem("token");
         setShowSplash(false);
        dispatch(getAsyncStorage(JSON.parse(user)))
        dispatch(saveUserToken(JSON.parse(token)))
        return user;
    }

    useEffect(()=> {

        const timeout = setTimeout(async () => {

            getAllValues();
          

        },2000); 

        return () => {
            timeout;
        }
      
    },[]);


    if(showSplash){
        return <Splash /> 
    }
   

    return (
        
            isSignedIn == false?
                (
                    <BottomNavigator />
                )
            :
                (
                  <BottomNavigator />
                )
        
      
    )
}
export default MainNavigator ;