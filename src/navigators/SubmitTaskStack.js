import React from  'react' ;
import { createStackNavigator, StackView } from '@react-navigation/stack'
import Home from '../screens/Home';
import HomeUser from '../screens/HomeUser';
import AllFavourite from '../screens/AllFavourite';
import RgbColor from '../screens/RgbColor';
import TrussCalculator from '../screens/TrussCalculator';
import DmxDigital from '../screens/DmxDigital'
import StringLight from '../screens/StringLight'
import Subscription from '../screens/Subscription'
import FavouriteFixture from '../screens/FavouriteFixture';
import MyWebView from '../screens/MyWebView';
import EmployeeList from '../screens/EmployeeList';
import WorkerProfile from '../screens/WorkerProfile';
import RegularisationReq from '../screens/RegularisationReq'
import SubmitTask from '../screens/SubmitTask'
import TaskAndTimeDetails from '../screens/TaskAndTimeDetails'
import Notifications from '../screens/Notifications';
import {  useSelector, useDispatch } from 'react-redux';

const SubmitTaskStack = (props) => {
  
    const isSignedIn  = useSelector(state => state.user.user_details)

    const Stack = createStackNavigator();
    return(
       
            <Stack.Navigator>
                 <Stack.Screen
                    component = {SubmitTask}
                    name="SubmitTask"
                    options={{
                        headerShown:false,
                    }}
                
                />
                <Stack.Screen
                    component = {Home}
                    name="Home"
                    options={{
                        headerShown:false,
                    }}
                
                />
             <Stack.Screen
                    component = {TrussCalculator}
                    name="TrussCalculator"
                    options={{
                        headerShown:false,
                    }}
                
                />
              <Stack.Screen
                    component = {RgbColor}
                    name="RgbColor"
                    options={{
                        headerShown:false,
                    }}
                
                />
                  <Stack.Screen
                    component = {AllFavourite}
                    name="AllFavourite"
                    options={{
                        headerShown:false,
                    }}
                
                />
                  <Stack.Screen
                    component = {DmxDigital}
                    name="DmxDigital"
                    options={{
                        headerShown:false,
                    }}
                
                />
              
                 <Stack.Screen
                    component = {StringLight}
                    name="StringLight"
                    options={{
                        headerShown:false,
                    }}
                
                />
                  <Stack.Screen
                    component = {FavouriteFixture}
                    name="FavouriteFixture"
                    options={{
                        headerShown:false,
                    }}
                
                />
                <Stack.Screen
                    component = {Subscription}
                    name="Subscription"
                    options={{
                        headerShown:false,
                    }}
                
                />
                <Stack.Screen
                    component = {MyWebView}
                    name="MyWebView"
                    options={{
                        headerShown:false,
                    }}
                
                />
                <Stack.Screen
                    component = {EmployeeList}
                    name="EmployeeList"
                    options={{
                        headerShown:false,
                    }}
                
                />
                <Stack.Screen
                    component = {RegularisationReq}
                    name="RegularisationReq"
                    options={{
                        headerShown:false,
                    }}
                
                />
                
                 <Stack.Screen
                    component = {WorkerProfile}
                    name="WorkerProfile"
                    options={{
                        headerShown:false,
                    }}
                
                />
                 <Stack.Screen
                    component = {TaskAndTimeDetails}
                    name="TaskAndTimeDetails"
                    options={{
                        headerShown:false,
                    }}
                
                />
                 <Stack.Screen
                    component = {Notifications}
                    name="Notifications"
                    options={{
                        headerShown:false,
                    }}
                />
            </Stack.Navigator>

    )
}

export default SubmitTaskStack;