import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import {createDrawerNavigator } from '@react-navigation/drawer';
import BottomNavigator from './BottomNavigator';
// import CoustomDrawer from '../Component/CoustomDrawer';
const DrawerNavigator = (props) => {

    const Drawer = createDrawerNavigator();
    return(
                 <Drawer.Navigator 
      initialRouteName="Home" 
      screenOptions={{ headerShown: false}}
    //   drawerContent={props =><CoustomDrawer {...props} />}
      >
                <Drawer.Screen 
                     component = {BottomNavigator}
                     name="Home" 
                />
                 {/* <Drawer.Screen 
                     component = {Location}
                     name="Location" 
                /> */}
                
                
            </Drawer.Navigator>
      
    )
}

export default DrawerNavigator;