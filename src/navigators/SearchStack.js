import React from  'react' ;
import { createStackNavigator, StackView } from '@react-navigation/stack'
import Search from '../screens/Search';
import FavouriteFixture from '../screens/FavouriteFixture'
import Sharedwith from '../screens/Sharedwith'

const SearchStack = (props) => {

    const Stack = createStackNavigator();
    return(
       
            <Stack.Navigator>
                    <Stack.Screen
                    component = {Search}
                    name="Search"
                    options={{
                        headerShown:false,
                        headerTitle:"Search",
                        headerTintColor:'white',
                        headerTitleAlign:'center',
                        headerTitleStyle:{
                            fontFamily:14,
                            // fontFamily:'roboto-medium',
                        },
                        headerStyle: {
                           // height:40,
                            backgroundColor: '#2C684F',
                        },
                    }}
                
                />
          <Stack.Screen
                    component = {FavouriteFixture}
                    name="FavouriteFixture"
                    options={{
                        headerShown:false,
                        headerTitle:"FavouriteFixture",
                        headerTintColor:'white',
                        headerTitleAlign:'center',
                        headerTitleStyle:{
                            fontFamily:14,
                            // fontFamily:'roboto-medium',
                        },
                        headerStyle: {
                           // height:40,
                            backgroundColor: '#2C684F',
                        },
                    }}
                
                />
               <Stack.Screen
                    component = {Sharedwith}
                    name="Sharedwith"
                    options={{
                        headerShown:false,
                        headerTitle:"Sharedwith",
                        headerTintColor:'white',
                        headerTitleAlign:'center',
                        headerTitleStyle:{
                            fontFamily:14,
                            // fontFamily:'roboto-medium',
                        },
                        headerStyle: {
                           // height:40,
                            backgroundColor: '#2C684F',
                        },
                    }}
                
                />
              

               

            </Stack.Navigator>

    )
}

export default SearchStack;