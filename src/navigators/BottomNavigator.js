import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import React, { useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Text,
  ScrollView,
  Linking,
  Image,
} from 'react-native';
import HomeStack from './HomeStack';
import LoginStack from './LoginStack';
import ProfileStack from './ProfileStack';
import EmployeeListStack from './EmployeeListStack';
import SubmitTaskStack from './SubmitTaskStack';
import Subscription from '../screens/Subscription';
import MyWebView from '../screens/MyWebView';
import {  useSelector, useDispatch } from 'react-redux';
const Tab = createBottomTabNavigator();
import { NavigationContainer ,StackActions,CommonActions,NavigationActions} from '@react-navigation/native';
import { paymentUrl,success_token ,paymentRes,requestGetApi} from '../WebApis/Service'; 

function BottomNavigator() {
const isSignedIn  = useSelector(state => state.user.user_details)
useEffect(()=>{

  console.log('hihihihihij22222',isSignedIn)
},[])
  return (
    <Tab.Navigator
     initialRouteName="Home"
   
    tabBarOptions={
      
      {
       activeTintColor: '#fff',
       inactiveTintColor: 'gray',
       inactiveBackgroundColor:'transparent',
       activeBackgroundColor:'blue',
      style:{
        backgroundColor: '#34333a',
        // borderRadius:10
      }
    }}
    >
   
      <Tab.Screen name="Home" component={HomeStack} 
      screenOptions={{
        tabBarActiveTintColor: '#000',
      }}
      options={{  
        tabBarLabel:'Home',  
        shifting:false,
        tabBarIcon: ({ focused }) => (  
          focused
          ? <Image source={require('../assets/ic-home.png')}  style={{width:21,height:21}} />
          : <Image source={require('../assets/ic-home-grey.png')}  style={{width:21,height:21}} />
        ), 
       }
    }  
    listeners={({navigation, route }) => ({
      tabPress: e => {      
        //navigation.dispatch(StackActions.replace('Home'))
      },
    })}
      />

            {isSignedIn!=null ?
                    isSignedIn.user_group=='2' ? //isSignedIn.data.user_group=='2' ?
                    <Tab.Screen name="SubmitTask" component={SubmitTaskStack} 
                      options={{  
                        tabBarLabel:'Submit Task',  
                        shifting:false,
                        tabBarIcon: ({ focused }) => (  
                          focused
                          ? <Image source={require('../assets/time-attendance-white.png')}  style={{width:30,height:30}} />
                          : <Image source={require('../assets/time-attendance.png')}  style={{width:30,height:30}} />
                        ), 
                      }
                    }
                      />
                :
                <Tab.Screen name="EmployeeList" component={EmployeeListStack} 
                options={{  
                  tabBarLabel:'Time Attendance Management',  
                  shifting:false,
                  tabBarIcon: ({ focused }) => (  
                    focused
                    ? <Image source={require('../assets/time-attendance-white.png')}  style={{width:30,height:30}} />
                    : <Image source={require('../assets/time-attendance.png')}  style={{width:30,height:30}} />
                  ), 
                }
              }
                />
                      :
                <Tab.Screen name="EmployeeList" component={EmployeeListStack} 
                      options={{  
                        tabBarLabel:'Time Attendance Management',  
                        shifting:false,
                        tabBarIcon: ({ focused }) => (  
                          focused
                          ? <Image source={require('../assets/time-attendance-white.png')}  style={{width:30,height:30}} />
                          : <Image source={require('../assets/time-attendance.png')}  style={{width:30,height:30}} />
                        ), 
                      }
                    }
                      />
                }

   



{ isSignedIn == null ?

      <Tab.Screen name="Login" component={LoginStack} 
       options={{  
        tabBarLabel:'signup/in',  
        shifting:false,
        tabBarIcon: ({ focused }) => (  
          focused
          ? <Image source={require('../assets/ic-profile-white.png')}  style={{width:21,height:21}} />
          : <Image source={require('../assets/ic-profile-grey.png')}  style={{width:21,height:21}} />
        ), 
       }
    }
      />

:

<Tab.Screen name="Profile" component={ProfileStack} 
       options={{  
        tabBarLabel:'Profile',  
        shifting:false,
        tabBarIcon: ({ focused }) => (  
          focused
          ? <Image source={require('../assets/ic-profile-white.png')}  style={{width:21,height:21}} />
          : <Image source={require('../assets/ic-profile-grey.png')}  style={{width:21,height:21}} />
        ), 
       }
    }
      />
}
    </Tab.Navigator>
  );
}

export default BottomNavigator;