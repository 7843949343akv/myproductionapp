import React, { Component } from 'react';
import {View,Image,Text, ImageBackground,StyleSheet,Animated,Easing,SafeAreaView, Alert,Dimensions} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage'

const Splash = (props) => {

     return(
    <SafeAreaView style={styles.container}>
    <ImageBackground source={require('../assets/splash-bg.png')}style={{width:'100%',height:'100%'}}resizeMode='cover'>
     <View style={{alignSelf:'center',justifyContent:'center',width:Dimensions.get('window').width * 0.8,
                  height:Dimensions.get('window').height * 0.20,marginTop:'65%'
                }}>
    
     </View>
     </ImageBackground>
         </SafeAreaView>
     );
  }
const styles = StyleSheet.create({

  container: {
    flex: 1,  
    backgroundColor:'#fff'
  },
});
export default Splash