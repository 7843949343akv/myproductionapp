import React from  'react' ;
import { createStackNavigator, StackView } from '@react-navigation/stack'
import BottomNavigator from './BottomNavigator';
const MainStackNav = (props) => {


    const Stack = createStackNavigator();
    return(
       
            <Stack.Navigator>
                    <Stack.Screen
                    component = {BottomNavigator}
                    name="BottomNavigator"
                    options={{
                        headerShown:false,
                        headerTitle:"BottomNavigator",
                        headerTintColor:'white',
                        headerTitleAlign:'center',
                        headerTitleStyle:{
                            fontFamily:14,
                            // fontFamily:'roboto-medium',
                        },
                        headerStyle: {
                           // height:40,
                            backgroundColor: '#2C684F',
                        },
                    }}
                
                />

            </Stack.Navigator>

    )
}

export default MainStackNav;