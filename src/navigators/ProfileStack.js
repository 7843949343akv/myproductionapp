import 'react-native-gesture-handler';
import React from  'react' ;
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator, StackView } from '@react-navigation/stack'

import Profile from '../screens/Profile';
import WorkerProfile from '../screens/WorkerProfile';
import Home from '../screens/Home';
import HomeUser from '../screens/HomeUser';
import AllFavourite from '../screens/AllFavourite';
import RgbColor from '../screens/RgbColor';
import TrussCalculator from '../screens/TrussCalculator';
import DmxDigital from '../screens/DmxDigital'
import StringLight from '../screens/StringLight'
import Subscription from '../screens/Subscription'
import FavouriteFixture from '../screens/FavouriteFixture';
import MyWebView from '../screens/MyWebView';
import EmployeeList from '../screens/EmployeeList';
import RegularisationReq from '../screens/RegularisationReq'
import SubmitTask from '../screens/SubmitTask'
import TaskAndTimeDetails from '../screens/TaskAndTimeDetails'
import Notifications from '../screens/Notifications';


const ProfileStack = (props) => {


    const Stack = createStackNavigator();
    return(
       
            <Stack.Navigator>
                    <Stack.Screen
                    component = {Profile}
                    name="Profile"
                    options={{
                        headerShown:false,
                        headerTitleAlign:'center',
                        headerStyle: {
                           // height:40,
                            backgroundColor: '#2C684F',
                        },
                    }}
                
                />
                <Stack.Screen
                    component = {WorkerProfile}
                    name="WorkerProfile"
                    options={{
                        headerShown:false,
                        headerTitleAlign:'center',
                        headerStyle: {
                           // height:40,
                            backgroundColor: '#2C684F',
                        },
                    }}
                
                />
                 <Stack.Screen
                    component = {AllFavourite}
                    name="AllFavourite"
                    options={{
                        headerShown:false,
                    }}
                
                />
                 <Stack.Screen
                    component = {FavouriteFixture}
                    name="FavouriteFixture"
                    options={{
                        headerShown:false,
                    }}
                
                />
               
              
                
            </Stack.Navigator>

    )
}

export default ProfileStack;
