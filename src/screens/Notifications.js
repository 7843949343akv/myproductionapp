import React, { useState } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Text,
  ScrollView,
  Dimensions,
  View,
  TextInput,
  Alert,
  Image,
  Platform,
} from 'react-native';
import Modal from 'react-native-modal';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Header from '../Component/Header'
var width = Dimensions.get('window').width
var height = Dimensions.get('window').height
var tinycolor = require("tinycolor2");
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {  useSelector, useDispatch } from 'react-redux';
import * as types  from '../redux/types';
import Activity_Indicator from '../Component/Activity_Indicator';
import Toast from 'react-native-simple-toast';
import { dimensions, MyColor,urls } from '../utils/constants';
import {add_task,save_favorite_color,get_attendance_detail,requestPostApi,requestGetApi} from '../WebApis/Service'
// var Slider = require('react-native-slider');
import Slider from '@react-native-community/slider'

const Notifications = (props) => {
  const token  = useSelector(state => state.user.token)
  const [loading, setLoading] = useState(false);
  const [redColor, setredColor] = useState(0);
  const [greenColor, setgreenColor] = useState(0);
  const [blueColor, setblueColor] = useState(0);
  const [modlevisual, setmodlevisual] = useState(false);
  const [sv, setsv] = useState(0);

  const color1 = tinycolor({ r: redColor, g: greenColor, b: blueColor });
  const hexaColorCode = color1.toHexString();
  const MySaveFavourite_Click=()=>{
    if(token){
      setmodlevisual(true)
    }else{
      props.navigation.navigate('Login')
    }
  }

const In_Modle_MyFavorate_Click=async()=>{
  setLoading(true)
  let formdata = new FormData();
    formdata.append("color_code",hexaColorCode);
  const{responseJson,err}  = await requestPostApi(save_favorite_color,formdata,'POST',token) 
  setLoading(false)
  if(err==null){
      if(responseJson.status){
        Toast.show(responseJson.message);
        setmodlevisual(false)
      }else{
        Toast.show(responseJson.message);
      }
  }else{
    Toast.show(err);
  }
     
}

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <Header Lable="Notification" Nav={() => props.navigation.goBack()} />
      <ScrollView>
       




      <View style={{width:100,height:100}} />
      </ScrollView>
      { loading ?
        <Activity_Indicator />
        :
        null
        }

    </SafeAreaView>
  );
};

let styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: MyColor.BGCOLOR,
  },
  input: {
    height: 45,
    width: '100%',
    fontSize: 17,
    borderColor: null,
    backgroundColor: MyColor.HEADERCOLOR,
    borderRadius: 28,
    color: '#fff',
    paddingHorizontal: 10

  },


})

export default Notifications;
