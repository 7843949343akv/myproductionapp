import React, { useEffect, useState } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Text,
  ScrollView,
  Dimensions,
  View,
  FlatList,
  Image,
} from 'react-native';
import {TouchableOpacity } from 'react-native-gesture-handler';
import { dimensions, MyColor ,urls} from '../utils/constants';
import Header from '../Component/Header'
var width = Dimensions.get('window').width
var height = Dimensions.get('window').height
import * as types  from '../redux/types';
import Activity_Indicator from '../Component/Activity_Indicator';
import {  useSelector, useDispatch } from 'react-redux';
import Toast from 'react-native-simple-toast';
import MyAlert from '../Component/MyAlert';    

import {remove_favorite_color,favorite_color_list,requestPostApi,requestGetApi} from '../WebApis/Service'
const AllFavourite = (props) => {
  const token  = useSelector(state => state.user.token)
  const [loading,setLoading]=useState(false)
  const [favColor,setFavColor]=useState([])
  const [updatestate,setupdate]=useState(true)
  const [removeid,setremoveid]=useState('')
  const [My_Alert, setMy_Alert] = useState(false)
  const [alert_sms, setalert_sms] = useState('')
 useEffect(()=>{
  MyFavorate_List()
 },[])
const MyFavorate_List=async()=>{
  //Api firing heare
  setupdate(!updatestate)
  setFavColor([])
     setLoading(true)
     const{responseJson,err}  = await requestGetApi(favorite_color_list,{},'GET',token)
     setLoading(false)
          if(err==null){
               if(responseJson.status){
                setFavColor(responseJson.data)
                }else{ 
                  Toast.show(responseJson.message);
                }
            }else{
              Toast.show(err);
            }
}



const Remove_favorate_color=async()=>{  
  setLoading(true)
    let formdata = new FormData();
    formdata.append("color_id",removeid);
const{responseJson,err}  = await requestPostApi(remove_favorite_color,formdata,'POST',token) 
setLoading(false)
if(err==null){
    if(responseJson.status){
           Toast.show(responseJson.message);
           setMy_Alert(false)
            setTimeout(()=>{
              setMy_Alert(false)
              MyFavorate_List()
            },200)
    }else{
      setMy_Alert(false)
      Toast.show(responseJson.message);
    }
}else{
  setMy_Alert(false)
  Toast.show(err);
}
}

const Remove_favorate_colorClick=async (id)=>{
  setremoveid(id)
  setalert_sms('Do  you want to remove the color from your favorites')
  setMy_Alert(true)
}
  return (
     <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <Header Lable="Favorites Colors" Nav={() => props.navigation.goBack()} />
<ScrollView>
<View>
<FlatList
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={{ alignItems: 'center' }}
              data={favColor}
              keyExtractor={item => item.id}
              renderItem={({ item }) => {
                return (
              <View style={{ width: width*93/100, height: 60, borderRadius: 20, marginVertical: 7,backgroundColor:item.color_code}}>
              <View style={{flexDirection:'row',justifyContent:'space-between',paddingHorizontal:15,alignItems:'center',marginVertical:10}}>
  {/* ************************************************* */}

  <TouchableOpacity style={{height:60,}}>
{/* <Image source={require('../assets/copy.png')} style={{ width: 14, height: 16,right:5}}></Image> */}
<Text style={{color:MyColor.TEXTCOLOR,fontSize:11}}>{item.title}</Text>
<Text style={{color:MyColor.TEXTCOLOR,fontSize:13}}>Color-Code : {item.color_code}</Text>
</TouchableOpacity>
<View style={{height:60}}> 
  <TouchableOpacity style={{height:25,backgroundColor:'#000',flexDirection:'row',borderRadius:10,alignItems:'center',paddingHorizontal:13,top:5}}
onPress={()=>{Remove_favorate_colorClick(item.id)}}>
<Image source={require('../assets/bin.png')} style={{ width: 11, height: 14,right:5}}></Image>
<Text style={{color:MyColor.TEXTCOLOR,fontSize:13}}>Remove</Text>
</TouchableOpacity>
</View>
                  </View>
              </View> 
                )
              }}
            />
  </View>    
  <View style={{width:100,height:100}} /> 
</ScrollView>  

{My_Alert ? <MyAlert sms={alert_sms}  okPress={()=>{ Remove_favorate_color() }} canclePress={()=>{setMy_Alert(false)}}/> : null }

        { loading ?
        <Activity_Indicator />
        :
        null
        }
      </SafeAreaView>
   
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
      backgroundColor:MyColor.BGCOLOR
    },
 
})

export default AllFavourite;
