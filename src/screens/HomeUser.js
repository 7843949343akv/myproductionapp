import React, { useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Text,
  ScrollView,
  View,
  Image,
  Dimensions,
  TextInput,
  FlatList,
  Alert,
  Linking,
} from 'react-native';
import {TouchableOpacity } from 'react-native-gesture-handler';
import {  useSelector, useDispatch } from 'react-redux';
import { useState } from 'react';
import Modal from 'react-native-modal';
import { Picker} from "native-base";
import DatePicker from 'react-native-datepicker'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
var width = Dimensions.get('window').width
var height = Dimensions.get('window').height
import * as types  from '../redux/types';
import Activity_Indicator from '../Component/Activity_Indicator';
import Toast from 'react-native-simple-toast';
import { dimensions, MyColor,urls } from '../utils/constants';
import { paymentUrl } from '../WebApis/Service';
const HomeUser  = (props) => {
  const dispatch =  useDispatch();
  const token  = useSelector(state => state.user.token)
  const loading  = useSelector(state => state.common.loading)
  const userData  = useSelector(state => state.user.user_details)
  const [notify,setNotify]=useState(true)
  const [forgot_check_out,setforgot_check_out]=useState(false)
  const [checkin,setCheckin]=useState(false)
  const [checkin_time,setCheckin_time]=useState(false)
  const [checkintime,setCheckintime]=useState(false)
  const [checkout,setCheckout]=useState(false)
  const [checkout_time,setCheckout_time]=useState(false)
  const [checkincheck,setCheckincheck]=useState(false)
  const[myHours,setmyHours]=useState('00:00:00')
  const [modlevisual, setmodlevisual] = useState(false);
  const [data,setData]=useState([
    {label:"Forgot to check Out", value:"1"},
    {label:"Forgot to check Out & check In", value:"2"},
    {label:"Forgot to check In", value:"3"},
])
const[reason,setReason]=useState('')
const[requestType,setRequestType]=useState('')
const [date, setDate] = useState(new Date())
const [startTime,setStartTime]=useState('');
const [apistartTime,setapiStartTime]=useState('');
const [apiendTime,setapiEndTime]=useState('');
const [endTime,setEndTime]=useState('');
const [startTimeTrue,setStartTimeTrue]=useState(false);
const [endTimeTrue,setEndTimeTrue]=useState(false);
const[job,setjob]=useState('')
const [jobdata,setjobdata]=useState([])
useEffect(()=>{


},[])


  const Mycomp=(img,lable,action,size)=>{
    return(
     <TouchableOpacity style={{width:'100%',height:height*10/100,backgroundColor:'#201f28',borderRadius:15,flexDirection:'row',justifyContent:'center',marginBottom:10}} onPress={action}>
     <View style={{backgroundColor:'#282732',width:'25%',height:'100%',borderBottomLeftRadius:15,borderTopLeftRadius:15,borderBottomRightRadius:55,position:'absolute',left:0,justifyContent:'center'}}>
     <Image source={img}style={{width:27,height:size,alignSelf:'center'}}resizeMode='cover'></Image>
     </View>
     <View style={{width:'50%',height:'100%',justifyContent:'center',marginLeft:'10%'}}>
      <Text style={{textAlign:'center',color:'white',alignSelf:'center'}}>{lable}</Text>
     </View>
     </TouchableOpacity>
    );
  };
const BySubscriptionPress=()=>{
  if(userData){
    if(userData.plan=='0' && userData.user_group=='1')
    {
    
      //props.navigation.navigate('Subscription')
     // props.navigation.navigate('MyWebView',{url:paymentUrl+userData.id,planid:'2'})
      Linking.openURL(paymentUrl+userData.user_id)
    }
  }else{
    props.navigation.navigate('Login')
       }
}

const msToTime = (duration) =>{
  var milliseconds = parseInt((duration % 1000) / 100),
  seconds = Math.floor((duration / 1000) % 60),
  minutes = Math.floor((duration / (1000 * 60)) % 60),
  hours = Math.floor((duration / (1000 * 60 * 60)));

hours = (hours < 10) ? "0" + hours : hours;
minutes = (minutes < 10) ? "0" + minutes : minutes;
seconds = (seconds < 10) ? "0" + seconds : seconds;
if(hours.length>2 || hours=='NaN')
{
  setmyHours('NaN')
}
 else{
  setmyHours(hours + ":" + minutes + ":" + seconds)
     }
}

const myCheckout=(c_o,diff)=>{
  setCheckout(true)
  setCheckout_time(c_o)
  //const diffrent_Time= new Date(c_o)-new Date(checkintime);
  //msToTime(diffrent_Time)
  setmyHours(diff)
  setCheckincheck(false)
}
const myCheckin=(c_i)=>{
  if(!checkincheck)
  {
    setCheckout(false)
    setCheckin(true)
    setCheckincheck(true)
    setCheckout_time('')
    setmyHours('00:00:00')
    setCheckintime(c_i)
    //new Date()
    setCheckin_time(c_i)
    //new Date().toLocaleString().substring(10,24)
  }
}

const CheckIn_Out = (mystatus)=>{
  // console.log('the status is ',mystatus)
  if(job==''){
    Toast.show('Please Select Job Type')
  }else{
  dispatch({
    type: types.LOADING,
    isLoading:true
          });
    let formdata = new FormData();
    formdata.append("flag",mystatus);
    formdata.append("task_type",job);
    console.log('formdata is',formdata)
       fetch(urls.base_url+urls.check_in_and_check_out, {
             method: 'POST',
             headers: {
              'Content-Type': 'multipart/form-data',
              'Accept':'application/json',
              'Authorization': 'Bearer '+ token
            },
             body:formdata,
           }).then((response) => response.json())
                 .then((responseJson) => {
                   dispatch({
                    type: types.LOADING,
                    isLoading:false
                          });
                          console.log('Chick in ,check out',responseJson)
          if(responseJson.status){
            if(mystatus=='Check-in'){
              myCheckin(responseJson.check_in_time)
            }
            else{
              myCheckout(responseJson.check_out_time,responseJson.diff)
            }
            Toast.show(responseJson.message);
           
        }else{
          Toast.show(responseJson.message);
        }
                 }).catch((error) => {
                   console.log(error)
                    dispatch({
                        type: types.LOADING,
                        isLoading:false
                    });
                 });
}}

const MytimeAttendance_Click=()=>{
  props.navigation.navigate('SubmitTask') 
}

const Myvalidation =()=>{
  if(requestType == '')
  {
    Toast.show('Please select request type')
  }else if(reason == '' || reason.trim().length == 0){
    Toast.show('Please enter reason')

  }else if(startTime == '' || startTime.trim().length == 0){
    Toast.show('Please select start time')

  }else if(endTime == '' || endTime.trim().length == 0){
    Toast.show('Please select end time')

  }else{
    SubmitRegularisationReq()
  }
}

const SubmitRegularisationReq = ()=>{
    dispatch({
      type: types.LOADING,
      isLoading:true
      });
      let formdata = new FormData();
      formdata.append("date",date);
      formdata.append("request_type",requestType);
      formdata.append("check_in",apistartTime);
      formdata.append("check_out",apiendTime);
      formdata.append("reason",reason);
      formdata.append("task_type",job);
         fetch(urls.base_url+urls.regularization_request, {
               method: 'POST',
               headers: {
                'Content-Type': 'multipart/form-data',
                'Accept':'application/json',
                'Authorization': 'Bearer '+ token
              },
               body:formdata,
             }).then((response) => response.json())
                   .then((responseJson) => {
                     dispatch({
                      type: types.LOADING,
                      isLoading:false
                            });
                            console.log('Submit Regularization',responseJson)
            if(responseJson.status){
              Toast.show(responseJson.message);
              setRequestType('');
              setReason('')
              setjob('')
              setDate(new Date())
              setmodlevisual(false)
          }else{
            Toast.show(responseJson.message);
          }
                   }).catch((error) => {
                    setmodlevisual(false)
                    console.log('hi',error)
                      dispatch({
                          type: types.LOADING,
                          isLoading:false
                      });
                   });
}

const tConvert = (time)=> {
  // Check correct time format and split into components
  time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

  if (time.length > 1) { // If time format correct
    time = time.slice (1);  // Remove full string match value
    time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM 
    time[0] = +time[0] % 12 || 12; // Adjust hours
  }
  return time.join (''); // return adjusted time or original string
}


  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" />
       <View style={{width:'100%',height:55,backgroundColor:'#34333a',alignSelf:'center',justifyContent:'center',borderBottomLeftRadius:20,borderBottomRightRadius:20}}>
      <Text style={{fontWeight:'bold',fontSize:18,color:'#fff',textAlign:'center'}}>Home</Text>
     {notify ?
<View style={{position:'absolute',right:15,width:30,height:30}}>
{/* <TouchableOpacity onPress={()=>{props.navigation.navigate('Notifications')}}>
 <Image source={require('../assets/notification.png')}style={{width:25,height:25,alignSelf:'center'}}resizeMode='cover'></Image> 
 </TouchableOpacity> */}
   {/* <View style={{width:20,height:20,backgroundColor:'red',borderRadius:10,top:-30,left:15,justifyContent:'center'}}>
<Text style={{color:'white',fontSize:12,alignSelf:'center'}}>1</Text>
   </View>  */}
  </View>
      :
      null
      }
       </View>
    <ScrollView showsVerticalScrollIndicator={false}>
 
<View style={{width:width*94/100,alignSelf:'center'}}>
<Text style={{fontWeight:'bold',fontSize:18,color:'#fff',textAlign:'center',margin:20}}>What you are looking for ?</Text>
{Mycomp(require('../assets/calculator.png'),'Truss Calculator',()=> {props.navigation.navigate('TrussCalculator')},33)}
{/* ************************************************************ */}
{Mycomp(require('../assets/DMX.png'),'DMX Analogue Dip switch calculator',()=> {props.navigation.navigate('DmxDigital')},27)}
{/* ************************************************************ */}
{Mycomp(require('../assets/rgb.png'),'RGB COLOR',()=> {props.navigation.navigate('RgbColor')},27)}
{/* ************************************************************ */}
{Mycomp(require('../assets/time-attendace.png'),'Submit Task',()=> MytimeAttendance_Click(),26)}
{/* ************************************************************ */}
{Mycomp(require('../assets/calculator.png'),'String light Calculator',()=> {props.navigation.navigate('StringLight')},33)}
{/* ************************************************************ */}
{ !notify ?
<View style={{width:'100%',backgroundColor:MyColor.BOXCOLOR,borderRadius:15,padding:10}}>
{/* **************forgot checkout ************************ */}
{ forgot_check_out ?
  <View>
<View style={{flexDirection:'row',justifyContent:'space-between'}}>
<Text style={{color:'red',fontSize:14,alignSelf:'center',marginTop:5,textAlign:'center'}}>You forgot your attendace</Text> 
<Text style={{color:'#FFF',fontSize:14,alignSelf:'center',marginTop:5}} onPress={()=>{setforgot_check_out(false)}}>Regularization</Text>
</View>
{/* //{new Date().toString().substring(3,15)} */}
<Text style={{color:'#FFF',fontSize:12,alignSelf:'center',marginTop:10,textAlign:'center'}}>Kindly submit regularization request for attendance approval</Text>
   <TouchableOpacity style={{width:'70%',height:34,borderRadius:15,backgroundColor:'blue',alignSelf:'center',justifyContent:'center',marginTop:20}} onPress={()=>{setmodlevisual(true)}}>
   <Text style={{color:MyColor.TEXTCOLOR,fontSize:11,alignSelf:'center'}}>Submit Regularization Request</Text>
   </TouchableOpacity>
</View>
:

<View style={{}}>
<View style={{flexDirection:'row',justifyContent:'space-between'}}>
<Text style={{color:'#FFF',fontSize:14,alignSelf:'center',marginTop:5}}>Today Working Hours</Text>
<Text style={{color:'#FFF',fontSize:14,alignSelf:'center',marginTop:5,color:'red'}} onPress={()=>{
   setjob('')
  setmodlevisual(true)
  }}>Regularization</Text>
</View>

{/* <Text style={{color:'#fff',fontSize:15 ,marginTop:10}}>Bulb Wattage</Text> */}
 
<View style={{width:'100%',height:40,borderRadius:10,backgroundColor:'#34333a',marginTop:15}}>

      <Picker
        placeholder={"Select Job Type"}
        selectedValue={job}
        style={styles.input}
        textStyle={{color:'#fff'}}
        onValueChange={(itemValue, itemIndex) =>{
            setjob(itemValue)
        }}
      >
    <Picker.Item label="Select Job Type" value="" />
    {jobdata.map((item, index) => {
          return <Picker.Item key={index} label={item.label} value={item.value} />
      })}
  </Picker>

 </View>

<Text style={{color:'#FFF',fontSize:14,alignSelf:'center',top:10}}>{myHours} Hours</Text>
<View style={{flexDirection:'row',justifyContent:'space-between',marginTop:20,width:'90%',alignSelf:'center'}}>
<View style={{width:'48%',height:30,backgroundColor:'green',borderRadius:15,justifyContent:'center',flexDirection:'row'}}>
<Text style={{color:MyColor.TEXTCOLOR,fontSize:13,alignSelf:'center'}} onPress={()=>{
   CheckIn_Out('Check-in')
  }}>CHECK-IN</Text>
{checkin ? 
<View style={{width:18,height:18,borderRadius:9,alignSelf:'center',backgroundColor:'#FFF',justifyContent:'center',left:15}}>
<Image source={require('../assets/check.png')} style={{ width: 13, height: 13,borderRadius:8,alignSelf:'center'}}></Image> 
</View>
  : null }
</View>
<View style={{width:'48%',height:30,backgroundColor:'red',borderRadius:15,justifyContent:'center',flexDirection:'row'}}>
<Text style={{color:MyColor.TEXTCOLOR,fontSize:13,alignSelf:'center'}}onPress={()=>{
   CheckIn_Out('Check-out')
  }}>CHECK-OUT</Text>
{checkout ? 
<View style={{width:18,height:18,borderRadius:9,alignSelf:'center',backgroundColor:'#FFF',justifyContent:'center',left:15}}>
<Image source={require('../assets/check.png')} style={{ width: 13, height: 13,borderRadius:8,alignSelf:'center'}}></Image> 
</View>
  : null }
</View>
</View>
<View style={{flexDirection:'row',justifyContent:'space-between',width:'77%',alignSelf:'center',marginTop:10}}>
<Text style={{color:MyColor.TEXTCOLOR,fontSize:13,alignSelf:'center'}}>{checkin_time}</Text>
<Text style={{color:MyColor.TEXTCOLOR,fontSize:13,alignSelf:'center'}}>{checkout_time}</Text>
</View>
</View>

}
{/* **************forgot checkout ************************ */}
</View>
:
<>
{userData ? 
<View>
{userData.plan !='1' ?
<TouchableOpacity style={{width:'75%',height:55,backgroundColor:MyColor.BUTTONCOLOR,alignSelf:'center',borderRadius:30,flexDirection:'row',justifyContent:'center'}} onPress={()=>{ BySubscriptionPress()}}>
<Image source={require('../assets/subscription.png')}style={{width:22,height:25,alignSelf:'center',marginRight:10}}resizeMode='cover'></Image>
<Text style={{color:'#FFF',fontSize:16,alignSelf:'center'}}>Buy Subscription </Text>
</TouchableOpacity>
: null
}
</View>
:
<TouchableOpacity style={{width:'75%',height:55,backgroundColor:MyColor.BUTTONCOLOR,alignSelf:'center',borderRadius:30,flexDirection:'row',justifyContent:'center'}} onPress={()=>{ BySubscriptionPress()}}>
<Image source={require('../assets/subscription.png')}style={{width:22,height:25,alignSelf:'center',marginRight:10}}resizeMode='cover'></Image>
<Text style={{color:'#FFF',fontSize:16,alignSelf:'center'}}>Buy Subscription </Text>
</TouchableOpacity>
}
</>
}
</View>
<View style={{width:100,height:100}} />
       </ScrollView>
       <Modal
        isVisible={modlevisual}
        swipeDirection="down"
        onSwipeComplete={(e) => {
          setmodlevisual(false)
          setjob('')
        }}
          scrollTo={() => {}}
          scrollOffset={1}
          propagateSwipe={true}
        coverScreen={false}
        backdropColor='transparent'
        style={{ justifyContent: 'flex-end', margin: 0, backgroundColor: 'rgba(25,25,25,10)',color:'white' }}
      >
        <View style={{ height: '79%', backgroundColor: MyColor.BOXCOLOR, borderTopLeftRadius: 30, borderTopRightRadius: 30, padding: 20 }}>
          <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
          <Text style={{ color: MyColor.TEXTCOLOR, fontSize: 21,alignSelf:'center',marginTop:20}}>Submit Regularization Request</Text>
{/* ********************************************************   */}
<Text style={{color:'#fff',fontSize:15 ,marginTop:15}}>Select Date</Text>
<View style={{width:'100%',height:39,borderRadius:28,marginTop:10,zIndex:999}}>

            <DatePicker
                      customStyles={{
                        dateInput: {borderColor:'transparent',left:-90},
                        dateText: {color:'#fff'},
                        dateIcon: styles.dateIcon,
                        dateplaceholder: {
                          alignContent: 'flex-start',
                         
                        },
                        placeholderText: {
                          fontSize: 15,
                          color: '#FFF',
                          marginLeft: '5%',
                          left:-100
                        },
                       
                      }}
                      androidMode={'spinner'}
                      readOnly={true}
                      style={[styles.datePickerSelectInput]}
                      date={date}
                      mode="date"
                      placeholder={'Select date'}
                      maxDate={new Date ()}
                      format='YYYY-MM-DD'
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      iconSource={require ('../assets/time-attendace.png')}
                      onDateChange={date => {
                        setDate(date)
                      }}
                    />



</View>
{/* ******************************************************** */}

<View style={{width:'100%',height:60,borderRadius:28,marginTop:15,flexDirection:'row',justifyContent:'space-between'}}>
<View style={{width:'45%',height:60,}}>
  <Text style={{color:'#FFF',fontSize:15,marginVertical:4}}>Start Time</Text>
<View style={{width:'100%',height:40,backgroundColor:'#34333a',borderTopLeftRadius:28,borderBottomLeftRadius:28}}>
           {Platform.OS=='ios' ? 
            
              <DatePicker
                      customStyles={{
                        dateInput: {borderColor:'transparent'},
                        dateText: {color:'#fff'},
                        dateIcon: styles.dateIcon,
                        dateplaceholder: {
                          alignContent: 'flex-start',
                         
                        },
                        placeholderText: {
                          fontSize: 15,
                          color: '#FFF',
                 
                        },
                      }}
                      showIcon={false}
                      androidMode={'spinner'}
                      is24Hour={false}
                      readOnly={true}
                      style={[styles.datePickerSelectInput]}
                   //   date={startTime}
                      mode="Time"
                      placeholder={startTime!='' ?startTime:'Select Time' }
                     // maxDate={new Date ()}
                     //format='YYYY-MM-DD'
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      // iconSource={require ('../assets/time-attendace.png')}
                      onDateChange={sTime => {
                        let st=sTime.substring(11,19)
                        let mt= tConvert(st)
                        setStartTime(mt)
                      }}
                    />
                    : 
                    startTimeTrue ? 
                    <View>
                      <DateTimePicker
                        value={new Date()}
                        mode='time'
                        is24Hour={false}
                        display="spinner"
                        onChange={(event,sTime) => {
                          setStartTimeTrue(false) 
                        let st=sTime.toString()
                        let nst=st.substring(16,24) 
                        var mt= tConvert(nst)
                          setStartTime(mt)
                          
                        }}
                      /> 
                      </View>
                    :
                    <TouchableOpacity style={{width:'100%',height:'100%',justifyContent:'center'}}>
                    <Text style={{alignSelf:'center',fontSize:15,color:'#fff'}} onPress={()=>{setStartTimeTrue(true)}}>{startTime!='' ?startTime:'Select Time'}</Text>
                    </TouchableOpacity>
                
      }
       
</View>
</View>

<View style={{width:'45%',height:60,}}>
<Text style={{color:'#FFF',fontSize:15,marginVertical:4}}>End Time</Text>
<View style={{width:'100%',height:40,backgroundColor:'#34333a',borderTopRightRadius:28,borderBottomRightRadius:28}}>
            
{Platform.OS=='ios' ? 
               <DatePicker
                      customStyles={{
                        dateInput: {borderColor:'transparent'},
                        dateText: {color:'#fff'},
                        dateIcon: styles.dateIcon,
                        dateplaceholder: {
                          alignContent: 'flex-start',
                        },
                        placeholderText: {
                          fontSize: 15,
                          color: '#FFF',
                        },
                      }}
                      showIcon={false}
                      androidMode={'spinner'}
                      is24Hour={false}
                      readOnly={true}
                      style={[styles.datePickerSelectInput]}
                     // date={endTime}
                      mode="Time"
                      placeholder={endTime!='' ?endTime:'Select Time' }
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      onDateChange={sTime => {
                        let st=sTime.substring(11,19)
                        let mt= tConvert(st)
                        setapiEndTime(st)
                        setEndTime(mt)
                      }}
                    />
                    : 
                    endTimeTrue ? 
                    <View>
                       <DateTimePicker
                        value={new Date()}
                        mode='time'
                        is24Hour={false}
                        display="spinner"
                        onChange={(event,sTime) => {
                          setEndTimeTrue(false) 
                        let st=sTime.toString()
                        let nst=st.substring(16,24)
                        let mt= tConvert(nst) 
                          setapiEndTime(nst)
                          setEndTime(mt)
                        }}
                      /> 
                      </View>
                    :
                    <TouchableOpacity style={{width:'100%',height:'100%',justifyContent:'center'}}>
                    <Text style={{alignSelf:'center',fontSize:15,color:'#fff'}} onPress={()=>{setEndTimeTrue(true)}}>{endTime!='' ?endTime:'Select Time'}</Text>
                    </TouchableOpacity>
                
      }
</View>
</View>

</View>

{/* ********************************************************   */}
<Text style={{color:'#fff',fontSize:15 ,marginTop:25}}>Request Type</Text>
<View style={{width:'100%',height:39,borderRadius:28,marginTop:5}}>

     <Picker
        
        placeholder={"Select"}
        selectedValue={requestType}
        style={styles.input}
        textStyle={{color:'#fff'}}
        onValueChange={(itemValue, itemIndex) =>{
            setRequestType(itemValue)
        }}
      >
    <Picker.Item label="Select" value="" />
    {data.map((item, index) => {
          return <Picker.Item key={index} label={item.label} value={item.value} />
      })}
  </Picker>


 </View>

 {/* ********************************************************   */}

 <Text style={{color:'#fff',fontSize:15 ,marginTop:20}}>Job Type</Text>
 <View style={{width:'100%',height:40,borderRadius:20,backgroundColor:'#34333a',marginTop:5}}>

<Picker
  placeholder={"Select Job Type"}
  selectedValue={job}
  style={styles.input}
  textStyle={{color:'#fff'}}
  onValueChange={(itemValue, itemIndex) =>{
      setjob(itemValue)
  }}
>
<Picker.Item label="Select Job Type" value="" />
{jobdata.map((item, index) => {
    return <Picker.Item key={index} label={item.label} value={item.value} />
})}
</Picker>

</View>

 {/* ********************************************************   */}
<Text style={{color:'#fff',fontSize:15 ,marginTop:10}}>Reason</Text>
<View style={{width:'100%',height:100,borderRadius:20,marginTop:10}}>

  <TextInput
        value={reason}
        onChangeText={(e) => setReason(e)}
        placeholder={''}
        placeholderTextColor="#bbbbbb"
        multiline={true}
        maxLength={500}
       // keyboardType="number-pad"
        autoCapitalize = 'none'
        style={[styles.input,{height:100,borderRadius:20,paddingHorizontal:15,paddingVertical:10}]}
      />


 </View>


           <View style={{ width: '85%', height: 45, backgroundColor: '#0000ff', alignSelf: 'center', borderRadius: 30, flexDirection: 'row', justifyContent: 'center', marginTop: 40 }}  >
              <Text style={{ color: MyColor.TEXTCOLOR, fontSize: 16, alignSelf: 'center' }} onPress={() => { Myvalidation() }}>Submit for Admin Approval</Text>
            </View>

            <View style={{width:100,height:100}} />
        </KeyboardAwareScrollView>
        { loading ?
        <Activity_Indicator />
        :
        null
        }
        </View>
     </Modal>
       { loading ?
        <Activity_Indicator />
        :
        null
        }
      </SafeAreaView>
  
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
      backgroundColor:'#15141b',
      alignItems:'center'
    },
    input: {
      height: 39,
      width:'100%',
      fontSize: 15,
      borderColor: null,
      backgroundColor: '#34333a',
      borderRadius:28,
      color:'#fff'
    },
    dateIcon:{
      width:20,
      height:18,
      marginRight:20
    },
    datePickerSelectInput:{
      height: 39,
      width:'100%',
      fontSize: 15,
      borderColor: null,
      backgroundColor: '#34333a',
      borderRadius:28,
      color:'#fff',
    }
})

export default HomeUser;
