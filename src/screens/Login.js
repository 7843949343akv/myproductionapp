import React from 'react'
import { Alert, Text, Image, ScrollView,BackHandler, ToastAndroid, Platform, TextInput, View, StyleSheet, ActivityIndicator, StatusBar, Dimensions, ImageBackground } from 'react-native';
import {TouchableOpacity } from 'react-native-gesture-handler';
import { dimensions, MyColor ,urls} from '../utils/constants';
import DrawerNavigator from '../navigators/DrawerNavigator';
import {  useSelector, useDispatch } from 'react-redux';
import {getAsyncStorage, setLoading ,SignUp,saveUserResult,saveUserToken} from '../redux/actions/user_action';
import * as types  from '../redux/types';
import Modal from 'react-native-modal';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useState } from 'react';
var width = Dimensions.get('window').width
var height = Dimensions.get('window').height
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import  Toast  from 'react-native-simple-toast';
import Activity_Indicator from '../Component/Activity_Indicator';
import { NavigationContainer ,StackActions,CommonActions,NavigationActions} from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';

 const Login = (props) => {
  const loading  = useSelector(state => state.common.loading)
 const dispatch =  useDispatch();
 const[name,setName]=useState('')
const[email,setEmail]=useState('')
const[pass,setPass]=useState('')
const[cpass,setCpass]=useState('')
const[passView,setPassView]=useState(true)
const[cpassView,setCpassView]=useState(true)
const MySignup=()=>{
  dispatch(SignUp(name, email,pass))
  .then(async(response) => {
      if(response.data.status=='true'){
       Toast.show(response.data.message)
      }else{
        Toast.show(response.data.message)
      }
  })
  .catch(error => {
      console.log("error-->",error);
  })
  
} 


const Myvalidation=()=>{
if(name == '' || name.trim().length == 0)
{
Toast.show('Please Enter Name')
}
else if(email == '' || email.trim().length == 0){
  Toast.show('Enter Email Id')
}
else if(!(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(
  email))){
    Toast.show('Enter Valid Email Id')
}       
else if(pass == '' || pass.trim().length == 0){
  Toast.show('Password field is required')
}
 else if(!(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{3,200}$/.test(pass)) || !(/[a-z]/.test(pass)) || !(/[A-Z]/.test(pass)) ||  !(/[0-9]/.test(pass))){
  Toast.show('must have at least one uppercase one lowercase one numeric and one special character in Password field')
 }
 else if(pass.trim().length < 6 || pass.trim().length > 16){
  Toast.show('Password should be 6 to 16 characters long')
}
 else if(cpass == '' || cpass.trim().length == 0){
  Toast.show('Please Enter confirm Password')
}
 else if(pass != cpass){
  Toast.show('Password and confirm Password should be same')
 } else{
    MySignup()
 }
}

  return (
    
    <SafeAreaView style={styles.container}>
           <StatusBar barStyle="dark-content" backgroundColor="#000" />
<View style={{flex:1,backgroundColor:'#15141b'}}>
          <View style={{ alignSelf: 'center',marginTop:30 }}>
           <Image source={require('../assets/newlogo.png')} style={{ width: 200, height: 70, alignSelf: 'center', zIndex: -999 }} resizeMode='cover'></Image>
          </View>

<View style={{width:'95%',height:height*70/100,marginHorizontal:10,paddingHorizontal:15,borderTopLeftRadius:30,borderTopRightRadius:30,backgroundColor:'#201f28',position:'absolute',bottom:-30,alignSelf:'center'}}>
<KeyboardAwareScrollView showsVerticalScrollIndicator={false}>

       
<Text style={{color:'#FFF',fontSize:25,marginTop:25,}}>Sign Up</Text>
<Text style={{color:'#FFF',fontSize:13,marginTop:5,}}>Please fill up your details</Text>

 {/* ********************************************************   */}
<Text style={{color:'#fff',fontSize:15 ,marginTop:15}}>Full Name</Text>
<View style={{width:'100%',height:47,borderRadius:28,marginTop:10}}>

  <TextInput
        value={name}
        onChangeText={(e) => setName(e)}
        placeholder={''}
        placeholderTextColor="#bbbbbb"
       // keyboardType="number-pad"
        autoCapitalize = 'none'
        style={styles.input}
      />


 </View>
{/* ********************************************************   */}
<Text style={{color:'#fff',fontSize:15 ,marginTop:15,}}>Email ID</Text>
<View style={{width:'100%',height:47,borderRadius:28,marginTop:10}}>

  <TextInput
        value={email}
        onChangeText={(e) => setEmail(e)}
        placeholder={''}
        placeholderTextColor="#bbbbbb"
       // keyboardType="number-pad"
        autoCapitalize = 'none'
        style={styles.input}
      />
 </View>
 
 {/* ********************************************************   */}
<Text style={{color:'#fff',fontSize:15 ,marginTop:15}}>Password</Text>
<View style={{width:'100%',height:47,borderRadius:28,marginTop:10}}>

  <TextInput
        value={pass}
        onChangeText={(e) => setPass(e)}
        placeholder={''}
        placeholderTextColor="#bbbbbb"
       // keyboardType="number-pad"
       secureTextEntry={passView ? true : false}
        autoCapitalize = 'none'
        style={styles.input}
      />
       <View style={{width:25,height:22,position:'absolute',right:20,top:12}}>
        <TouchableOpacity onPress={()=>setPassView(!passView)}>
         <Image source={passView ? require('../assets/eye.png') : require('../assets/eyes.png')}style={{width:'100%',height:'100%',alignSelf:'center',zIndex:-999}}resizeMode='cover'></Image>
        </TouchableOpacity>
      </View>
 </View>
 {/* ********************************************************   */}
<Text style={{color:'#fff',fontSize:15 ,marginTop:15}}>Confirm Password</Text>
<View style={{width:'100%',height:47,borderRadius:28,marginTop:10}}>

  <TextInput
        value={cpass}
        onChangeText={(e) => setCpass(e)}
        placeholder={''}
        placeholderTextColor="#bbbbbb"
       // keyboardType="number-pad"
       secureTextEntry={cpassView ? true : false}
        autoCapitalize = 'none'
        style={styles.input}
      />
        <View style={{width:25,height:22,position:'absolute',right:20,top:12}}>
        <TouchableOpacity onPress={()=>setCpassView(!cpassView)}>
         <Image source={cpassView ? require('../assets/eye.png') : require('../assets/eyes.png')}style={{width:'100%',height:'100%',alignSelf:'center',zIndex:-999}}resizeMode='cover'></Image>
        </TouchableOpacity>
      </View>
 </View>
 {/* ********************************************************   */}

 <TouchableOpacity style={{width:'75%',height:55,backgroundColor:'#0000ff',alignSelf:'center',borderRadius:30,flexDirection:'row',justifyContent:'center',marginTop:20}} onPress={()=>{Myvalidation()}}>
<Text style={{color:'#FFF',fontSize:16,alignSelf:'center'}}>Sign Up</Text>
</TouchableOpacity>

<Text style={{color:'#FFF',fontSize:16,alignSelf:'center',margin:5}} onPress={()=>{props.navigation.navigate('Signup')}}>Already have an account? Sign In</Text>

<View style={{width:100,height:100}} />

       </KeyboardAwareScrollView>


</View>

</View>

     { loading ?
        <Activity_Indicator />
        :
        null
        }
      </SafeAreaView>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
      backgroundColor:'#15141b',
    },
  
  input: {
    paddingLeft: 15,
    paddingRight:50,
    height: 45,
    width:'100%',
    fontSize: 17,
    borderColor: null,
    backgroundColor: '#34333a',
    borderRadius:28,
    color:'#fff'

  },
   
   

})

export default Login;
