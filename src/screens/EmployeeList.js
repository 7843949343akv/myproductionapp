import React, { useEffect, useState } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Text,
  ScrollView,
  Dimensions,
  View,
  FlatList,
  Image,
  Alert,
  TextInput,
  TouchableOpacity,
  RefreshControl,
} from 'react-native';

import Modal from 'react-native-modal';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import DatePicker from 'react-native-datepicker'
import Header from '../Component/Header'
var width = Dimensions.get('window').width
var height = Dimensions.get('window').height
import {  useSelector, useDispatch } from 'react-redux';
import * as types  from '../redux/types';
import Activity_Indicator from '../Component/Activity_Indicator';
import Toast from 'react-native-simple-toast';
import { dimensions, MyColor,urls } from '../utils/constants';
import {add_worker,get_worker_list,baseUrl,submit_job_type,delete_job_type,get_job_type,paymentUrl,requestPostApi,requestGetApi} from '../WebApis/Service'
import {setEmpolyeeList} from '../redux/actions/user_action'
import { Picker} from "native-base";
import { Mycolors } from '../utils/Mycolors';

const EmployeeList = (props) => {
  const token  = useSelector(state => state.user.token)
  const userdata  = useSelector(state => state.user)
  const dispatch = useDispatch()
     const [loading, setLoading] = useState(false);
     const [modlevisual, setmodlevisual] = useState(false);
     const [jobTypeModal, setjobTypeModal] = useState(false);
     const [data,setData]=useState([])
     const[name,setName]=useState('')
     const[showdate,setshowdate]=useState(false)
     const[email,setEmail]=useState('')
     const [date, setDate] = useState(new Date())
     const [subscriptiondetail,setsubscriptiondetail]=useState('')
     const [my_job_type,setmy_job_type] = useState('')
     const [dropdata,setdropData]=useState(null)
  const [refreshing, setRefreshing] = useState(false);
  const[string,setString]=useState('')

     useEffect(()=>{
    
      employee_List()
      getJobtype()
     },[])

  const wait = (timeout) => {
      return new Promise(resolve => setTimeout(resolve, timeout));
    }
    
  const onRefresh = React.useCallback(() => {
    setRefreshing(true); 
    employee_List()
    wait(2000).then(() => {
      setRefreshing(false)
    });
  }, []);

     const AddnewUserDesign=(click)=>{
         return(
            <TouchableOpacity style={{width:'75%',height:55,backgroundColor:MyColor.BUTTONCOLOR,alignSelf:'center',borderRadius:30,flexDirection:'row',justifyContent:'center'}} onPress={click}>
            <Image source={require('../assets/add-user.png')}style={{width:24,height:25,alignSelf:'center',marginRight:10}}resizeMode='cover'></Image>
            <Text style={{color:'#FFF',fontSize:16,alignSelf:'center'}} onPress={click}>Add New Workers</Text>
            </TouchableOpacity>
         )
     }
const validation=()=>{
  if(name == '' || name.trim().length == 0)
{
Toast.show('Please Enter Name')
}
else if(email == '' || email.trim().length == 0){
  Toast.show('Enter Email Id')
}
else if(!(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(
  email))){
    Toast.show('Enter Valid Email Id')
}
else if(!showdate){
    Toast.show('Please Select date')
}
else{
  Add_new_worker()
} 
}
 const Add_new_worker=async()=>{
        let formdata = new FormData();
        formdata.append("name",name);
        formdata.append("email",email);
        // formdata.append("worker_type",string);
        formdata.append("adding_date",date);
        setLoading(true)

const{responseJson,err}  = await requestPostApi(add_worker,formdata,'POST',token) 
        setLoading(false)
        if(err==null){
            if(responseJson.status){
              Toast.show(responseJson.message);
              setmodlevisual(false)
              setName('')
              setEmail('')
              employee_List()
            }else{
              Toast.show(responseJson.message);
            }
        }else{
          Alert.alert(err)
        }
     }

const employee_List=async()=>{
  setLoading(true)
  const{responseJson,err}  = await requestGetApi(get_worker_list,{},'GET',token)
  setLoading(false)
       if(err==null){
            if(responseJson.status){
              setData(responseJson.data)
              dispatch(setEmpolyeeList(responseJson.data))
              setsubscriptiondetail(responseJson.subscription_detail)
             }else{ 
              setsubscriptiondetail(responseJson.subscription_detail)
              dispatch(setEmpolyeeList(null))
              setData([])
              Toast.show(responseJson.message);
             }
         }else{
         // Alert.alert(err)
         }
}


const Add_new_jobType=async()=>{
  if(my_job_type==''){
    Toast.show('Please Enter Job Type')
  }else{
  let formdata = new FormData();
  formdata.append("job_type",my_job_type);
  setLoading(true)

const{responseJson,err}  = await requestPostApi(submit_job_type,formdata,'POST',token) 
  if(err==null){
      if(responseJson.status){
        Toast.show(responseJson.message);
        getJobtype()
        setmy_job_type('')
      }else{
        setLoading(false)
        Toast.show(responseJson.message);
      }
  }else{
    setLoading(false)
    Alert.alert(err)
  }
}
}


const Delete_new_jobType=async(item)=>{
 console.log('hiihi',item);
  let formdata = new FormData();
  formdata.append("job_type_id",item.id);
  setLoading(true)

const{responseJson,err}  = await requestPostApi(delete_job_type,formdata,'POST',token) 
 
  if(err==null){
      if(responseJson.status){
        // var dumydata=dropdata
        // if(index==0){
        //   const removed = dumydata.splice(0, 1)
        // }else{
        //   const mremoved = dumydata.splice(index-1, 1);
        // }
        // console.log('array==>>',dumydata);
        // setdropData(dumydata)
        // setRefreshing(!refreshing)  
        getJobtype()
      }else{
        setLoading(false)
        Toast.show(responseJson.message);
      }
  }else{
    setLoading(false)
    Alert.alert(err)
  }

}

const getJobtype=async()=>{
  setLoading(true)
  const{responseJson,err}  = await requestGetApi(get_job_type,{},'GET',token)
  setLoading(false)
  console.log('jdjjf',responseJson);
       if(err==null){
            if(responseJson.status){
              // var dumyArr=[]
              // for(let i=0;i<responseJson.data.length;i++){
              //   dumyArr.push( {id:i,label:responseJson.data[i].job_type, value:responseJson.data[i].job_type})
              // }
              // setdropData(dumyArr) 
              setdropData(responseJson.data)
             }else{ 
               setdropData(null)
              Toast.show(responseJson.message);
             }
         }else{
         // Alert.alert(err)
         }
}


  return (
     <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <Header Lable="Time / Attendance Management" Nav={() => props.navigation.goBack()} />
<ScrollView 
 refreshControl={
  <RefreshControl
    refreshing={refreshing}
    onRefresh={onRefresh}
  />
}>
<View style={{width:dimensions.SCREEN_WIDTH-20,alignSelf:'center',backgroundColor:MyColor.BOXCOLOR,borderRadius:20,marginTop:20,paddingVertical:10}}>

<View style={{position:'absolute',right:20,top:20}}>
<Text style={{color:'transparent'}}>Active Plan</Text>
</View>
<View style={{flexDirection:'row',width:'60%'}}>
<View style={{width:60,height:80,backgroundColor:MyColor.SUBBOXCOLOR,borderBottomRightRadius:60,borderTopLeftRadius:20,justifyContent:'center',top:-9}}>
<Image source={require('../assets/time-attendace.png')} style={{ width: 30, height: 30,left:10}}></Image>
</View>
<View>
<Text style={{color:MyColor.TEXTCOLOR,fontSize:15,marginLeft:10,marginTop:20}}>{subscriptiondetail.total_added_worker} / {subscriptiondetail.total_worker} Employees</Text>
{/* <Text style={{color:'#6661ee',fontSize:15,marginLeft:10,marginTop:10}}>{subscriptiondetail.title}</Text> */}
</View>
</View>
<View style={{paddingLeft:50,paddingRight:20}}>
<Text style={{color:MyColor.TEXTCOLOR,fontSize:12,marginVertical:4}}>{subscriptiondetail.description}</Text>
</View>
</View>
    <Text style={{color:MyColor.TEXTCOLOR,fontSize:18,marginLeft:10,marginVertical:20}}>Employee List</Text>
<View>
<FlatList
              showsHorizontalScrollIndicator={false}
              // numColumns={2}
              contentContainerStyle={{ alignItems: 'center' }}
              data={userdata.emp_list}
              keyExtractor={item => item.id}
              renderItem={({ item }) => {
                return (
                    <View style={{width:dimensions.SCREEN_WIDTH-20,height:65,backgroundColor:MyColor.BOXCOLOR,marginVertical:6,borderRadius:10,flexDirection:'row',alignItems:'center',paddingHorizontal:10}}>
                    <Image source={require('../assets/profile.png')} style={{ width: 50, height: 50}}></Image>
                    <View style={{marginLeft:15,top:-5}}>
                    <Text style={{color:MyColor.TEXTCOLOR,fontSize:20}}>{item.name}</Text>
                    <Text style={{color:MyColor.TEXTCOLOR,top:5}}>{item.email}</Text>
                    </View>
                        <View style={{position:'absolute',right:10,top:25}}>
                            <Text style={{color:MyColor.TEXTCOLOR,textAlign:'right',fontSize:11}} onPress={()=>{props.navigation.navigate('WorkerProfile',{id:item.id})}}>See Activities</Text>
                        </View>
                    </View>
                )
              }}
            />
  </View>    
  <View style={{width:100,height:100}} /> 
</ScrollView>  
<View style={{bottom:10}}>
 
  <View style={{width:'100%',flexDirection:'row',justifyContent:'space-around',alignItems:'center'}}>
  
   <TouchableOpacity style={{width:dimensions.SCREEN_WIDTH*40/100,height:45,borderRadius:10,backgroundColor:'blue',justifyContent:'center'}}
   onPress={()=>{
    if(dropdata!=null){
      setmodlevisual(true)
    }else{
      Toast.show('Please Add Job Type first')
    }
    
    }}>
   <Text style={{textAlign:'center',color:Mycolors.BG_COLOR,fontSize:13,fontWeight:'bold'}}>Add New Workers</Text>
    </TouchableOpacity>
   <TouchableOpacity style={{width:dimensions.SCREEN_WIDTH*40/100,height:45,borderRadius:10,backgroundColor:'blue',justifyContent:'center'}}
   onPress={()=>{
    getJobtype()
    setjobTypeModal(true)
    }}>
   <Text style={{textAlign:'center',color:Mycolors.BG_COLOR,fontSize:13,fontWeight:'bold'}}>Add Job Type</Text>
    </TouchableOpacity>
    </View>
    {/* :
   <TouchableOpacity style={{width:'90%',height:45,borderRadius:10,backgroundColor:'blue',justifyContent:'center',alignSelf:'center'}}
   onPress={()=>{
    getJobtype()
    setjobTypeModal(true)
    }}>
      <Text style={{textAlign:'center',color:Mycolors.BG_COLOR,fontSize:13,fontWeight:'bold'}}>Add Job Type</Text>
    </TouchableOpacity>
   } */}
{}
</View>
    <Modal
        isVisible={modlevisual}
        swipeDirection="down"
        onSwipeComplete={(e) => {
          setmodlevisual(false)
        }}
          scrollTo={() => {}}
          scrollOffset={1}
          propagateSwipe={true}
        coverScreen={false}
        backdropColor='transparent'
        style={{ justifyContent: 'flex-end', margin: 0, backgroundColor: 'rgba(20,20,20,1)' }}
      >
        <View style={{ height: '90%', backgroundColor: MyColor.SUBBOXCOLOR, borderTopLeftRadius: 30, borderTopRightRadius: 30, padding: 20 }}>
          <ScrollView showsVerticalScrollIndicator={false}>
          <View style={{ alignSelf: 'center' }}>
           <Image source={require('../assets/add-new-employee.png')} style={{ width: 172, height: 135, alignSelf: 'center', zIndex: -999 }} resizeMode='cover'></Image>
          </View>

          <Text style={{color:'#FFF',fontSize:25,marginTop:25,}}>Add New Employees</Text>
<Text style={{color:'#FFF',fontSize:13,marginTop:5,}}>Employee will receive login link via Email ID</Text>
 {/* ********************************************************   */}
<Text style={{color:'#fff',fontSize:15 ,marginTop:15}}>Full Name</Text>
<View style={{width:'100%',height:47,borderRadius:28,marginTop:10}}>

  <TextInput
        value={name}
        onChangeText={(e) => setName(e)}
        placeholder={''}
        placeholderTextColor="#bbbbbb"
       // keyboardType="number-pad"
        autoCapitalize = 'none'
        style={styles.input}
      />


 </View>
{/* ********************************************************   */}
<Text style={{color:'#fff',fontSize:15 ,marginTop:15}}>Email ID</Text>
<View style={{width:'100%',height:47,borderRadius:28,marginTop:10}}>

  <TextInput
        value={email}
        onChangeText={(e) => setEmail(e)}
        placeholder={''}
        placeholderTextColor="#bbbbbb"
       // keyboardType="number-pad"
        autoCapitalize = 'none'
        style={styles.input}
      />


 </View>
{/* ********************************************************   */}
<Text style={{color:'#fff',fontSize:15 ,marginTop:15}}>Select Date</Text>
<View style={{width:'100%',height:39,borderRadius:28,marginTop:10,zIndex:999}}>

            <DatePicker
                      customStyles={{
                        dateInput: {borderColor:'transparent',left:-90},
                        dateText: {color:showdate ? '#fff' : 'transparent'},
                        dateIcon: styles.dateIcon,
                        dateplaceholder: {
                          alignContent: 'flex-start',
                        },
                        placeholderText: {
                          fontSize: 15,
                          color: '#fff',
                          marginLeft: '5%',
                          left:-100
                        },
                      }}
                      androidMode={'spinner'}
                      readOnly={true}
                      style={[styles.datePickerSelectInput,{color: '#fff'}]}
                      date={date}
                      mode="date"
                      placeholder={'Select date'}
                      maxDate={"3024-12-31"}
                      format='YYYY-MM-DD'
                      minDate={new Date ()}
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      iconSource={require ('../assets/time-attendace.png')}
                      onDateChange={date => {
                        setDate(date)
                       setshowdate(true)
                      }}
                    />

</View>
<View style={{width:'100%',alignSelf:'center',marginTop:25}}>
        {AddnewUserDesign(()=> {validation()})}
        </View>
 {/* ********************************************************   */}
 <View style={{width:100,height:150}} />
         </ScrollView>
       
        { loading ?
        <Activity_Indicator />
        :
        null
        }
         </View>
    </Modal>

    <Modal
        isVisible={jobTypeModal}
        swipeDirection="down"
        onSwipeComplete={(e) => {
          setjobTypeModal(false)
        }}
          scrollTo={() => {}}
          scrollOffset={1}
          propagateSwipe={true}
        coverScreen={false}
        backdropColor='transparent'
        style={{ justifyContent: 'flex-end', margin: 0, backgroundColor: 'rgba(20,20,20,1)' }}
      >

        <View style={{ height: '95%', backgroundColor: MyColor.SUBBOXCOLOR, borderTopLeftRadius: 30, borderTopRightRadius: 30, padding: 20 }}>
                       <KeyboardAwareScrollView showsVerticalScrollIndicator={false} style={{}}>

        <Text style={{color:'#FFF',fontSize:20,marginTop:20,marginBottom:10}}>Added Job Types</Text>
         
          <View>
     {dropdata ?
            <FlatList
              showsHorizontalScrollIndicator={false}
              // numColumns={2}
              contentContainerStyle={{ alignItems: 'center' }}
              data={dropdata}
              keyExtractor={item => item.id}
              renderItem={({ item,index }) => {
                return (
                    <View style={{width:dimensions.SCREEN_WIDTH*90/100,height:45,backgroundColor:MyColor.BOXCOLOR,
                    marginVertical:6,borderRadius:10,flexDirection:'row',alignItems:'center',paddingHorizontal:10,justifyContent:'space-between'}}>
                    <View style={{flexDirection:'row'}}>
                    <Text style={{fontSize:16,color:'#fff',textAlign:'center',fontWeight:'bold'}}>Title :-</Text>
                    <Text style={{fontSize:15,color:'#fff',textAlign:'center',fontWeight:'400',left:4}}>{item.job_type}</Text>
                    </View>
                    <TouchableOpacity style={{width:35,height:35}}
                    onPress={()=>{Delete_new_jobType(item)}}>
                    <Image source={require('../assets/minus-card.png')} style={{width:35,height:35}}></Image>
                    </TouchableOpacity>
                    </View>
                )
              }}
            />
            :
            <View style={{alignSelf:'center'}}>
              <Text style={{fontSize:20,color:'#fff',textAlign:'center',fontWeight:'bold',marginTop:'20%'}}>No Jobs Added</Text>
              <Image source={require('../assets/empty-box.png')} style={{width:150,height:150}}></Image>
            </View>
        }
  </View>    
  <View style={{width:100,height:10}} /> 


 {/* ********************************************************   */}
 <View style={{width:100,}} />
        
         <Text style={{color:'#FFF',fontSize:20,marginTop:15,}}>Add New Job Type</Text>
    <Text style={{color:'#fff',fontSize:15 ,marginTop:15}}>Job Type</Text>
    <View style={{width:'100%',height:47,borderRadius:28,marginTop:10}}>

  <TextInput
        value={my_job_type}
        onChangeText={(e) => setmy_job_type(e)}
        placeholder={''}
        placeholderTextColor="#bbbbbb"
       // keyboardType="number-pad"
        autoCapitalize = 'none'
        style={styles.input}
      />
 </View>
      <TouchableOpacity style={{width:'100%',alignSelf:'center',backgroundColor:'blue',height:40,borderRadius:30,justifyContent:'center',marginTop:10}}
         onPress={()=>{Add_new_jobType()}}>
      <Text style={{color:Mycolors.BG_COLOR,textAlign:'center',fontSize:13}}>Add</Text>
        </TouchableOpacity>
       
        </KeyboardAwareScrollView>
       { loading ?
        <Activity_Indicator />
        :
        null
        }
         </View>
    </Modal>

    { loading ?
        <Activity_Indicator />
        :
        null
        }

      </SafeAreaView>
   
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
      backgroundColor:MyColor.BGCOLOR
    },
    input: {
        paddingLeft: 15,
        height: 45,
        width:'100%',
        fontSize: 17,
        borderColor: null,
        backgroundColor: '#34333a',
        borderRadius:28,
        color:'#fff'
    
      },
      dateIcon:{
        width:20,
        height:18,
        marginRight:20
      },
      datePickerSelectInput:{
        height: 39,
        width:'100%',
        fontSize: 15,
        borderColor: null,
        backgroundColor: '#34333a',
        borderRadius:28,
        
      }
       
})

export default EmployeeList;
