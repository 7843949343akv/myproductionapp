import React from 'react'
import { Alert, Text, Image, ScrollView,BackHandler, ToastAndroid, Platform, TextInput, View, StyleSheet, ActivityIndicator, StatusBar, Dimensions, ImageBackground } from 'react-native';
import {TouchableOpacity } from 'react-native-gesture-handler';
import { dimensions, MyColor } from '../utils/constants';
import {  useSelector, useDispatch } from 'react-redux';
import {getAsyncStorage, setLoading} from '../redux/actions/user_action';
import * as types  from '../redux/types';
import Modal from 'react-native-modal';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useState } from 'react';
var width = Dimensions.get('window').width
var height = Dimensions.get('window').height
import Header from '../Component/Header'
import { Picker} from "native-base";
import Toast from 'react-native-simple-toast';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

 const StringLight = (props) => {
 const[string,setString]=useState('')
const[bulb,setBulb]=useState('')
const[circut,setCircut]=useState('')
const[result,setResult]=useState('')
const [data,setData]=useState([
    {label:"25ft", value:"25ft"},
    {label:"50ft", value:"50ft"},
    {label:"100ft", value:"100ft"},
])
const [watdata,setWatData]=useState([
  {label:"60wt", value:"60wt"},
  {label:"100wt", value:"100wt"},
])
const [ampragdata,setAmpragData]=useState([
  {label:"15amp", value:"15amp"},
  {label:"20amp", value:"20amp"},
])
const[watperbulb,setwatperbulb]=useState('')
const[totalbulb,settotalbulb]=useState('')
const[linevoltage,setlinevoltage]=useState('')
const[ratingamps,setratingamps]=useState('')
const[bulbwatage,setbulbwatage]=useState('')
const[linevoltage2,setlinevoltage2]=useState('')
const[bulbspace,setbulbspace]=useState('')

const[totalwat,settotalwat]=useState('')
const[totalamp,settotalamp]=useState('')
const[maxrun,setmaxrun]=useState('')
const[maxlength,setmaxlength]=useState('')

const myResult2=()=>{
  if(ratingamps==''){
    Toast.show('Please Enter Wire Rating (Amps)') 
  }else if(linevoltage2==''){
    Toast.show('Please Enter Line voltage')
  }else if(bulbwatage==''){
    Toast.show('Please Enter Bulb Wattage')
  }else if(bulbspace==''){
    Toast.show('Please Enter Bulb Spacing (Inches)')
  }else{
    var Number_of_Bulbs_in_Max_Run=(ratingamps * linevoltage2) / bulbwatage
    // setmaxrun('Number of Bulbs in Max Run        :     '+Number_of_Bulbs_in_Max_Run.toString().substring(0,5))
    setmaxrun('Number of Bulbs in Max Run        :     '+parseFloat(Number_of_Bulbs_in_Max_Run).toFixed(2)) 
    var Maximum_Run_Length_in_Feet=(Number_of_Bulbs_in_Max_Run * bulbspace) / 12
    // setmaxlength('Maximum Run Length (Feet)        :     '+Maximum_Run_Length_in_Feet.toString().substring(0,5))
    setmaxlength('Maximum Run Length (Feet)        :     '+parseFloat(Maximum_Run_Length_in_Feet).toFixed(2))
       }
}


const myResult=()=>{
  if(watperbulb==''){
    Toast.show('Please Enter Watts per bulb')
    }else if(totalbulb==''){
      Toast.show('Please Enter Number of bulbs')
    }else if(linevoltage==''){
      Toast.show('Please Enter Line voltage')
    }else{
      var totalwatts=watperbulb * totalbulb
      // settotalwat('Total Wattage                                :     '+totalwatts.toString().substring(0,5))
      settotalwat('Total Wattage                                :     '+parseFloat(totalwatts).toFixed(2))
      var totalAmperage=totalwatts/linevoltage
      // settotalamp('Total Amperage                             :     '+totalAmperage.toString().substring(0,5))     
      settotalamp('Total Amperage                             :     '+parseFloat(totalAmperage).toFixed(2))     
    }
    // Alert.alert('hi')
    // Total Wattage = Watts per Bulb * Number of Bulbs
    // Total Amperage = Total Wattage / Line Voltage
    // Number of Bulbs in Max Run = floor((Wire Rating in Amps * Line Voltage) / Bulb Wattage)
    // Maximum Run Length in Feet = (Number of Bulbs in Max Run * Bulb Spacing in Inches) / 12
// if(watperbulb=='' && totalbulb=='' && linevoltage==''){
//             if(ratingamps=='' && linevoltage2=='' && bulbwatage=='' && bulbspace==''){
//               Toast.show('Please Enter Values')
//             }else{
//               if(ratingamps==''){
//                 Toast.show('Please Enter Wire Rating (Amps)') 
//               }else if(linevoltage2==''){
//                 Toast.show('Please Enter Line voltage')
//               }else if(bulbwatage==''){
//                 Toast.show('Please Enter Bulb Wattage')
//               }else if(bulbspace==''){
//                 Toast.show('Please Enter Bulb Spacing (Inches)')
//               }else{
//                 var Number_of_Bulbs_in_Max_Run=(ratingamps * linevoltage2) / bulbwatage
//                 // setmaxrun('Number of Bulbs in Max Run        :     '+Number_of_Bulbs_in_Max_Run.toString().substring(0,5))
//                 setmaxrun('Number of Bulbs in Max Run        :     '+parseFloat(Number_of_Bulbs_in_Max_Run).toFixed(2)) 
//                 var Maximum_Run_Length_in_Feet=(Number_of_Bulbs_in_Max_Run * bulbspace) / 12
//                 // setmaxlength('Maximum Run Length (Feet)        :     '+Maximum_Run_Length_in_Feet.toString().substring(0,5))
//                 setmaxlength('Maximum Run Length (Feet)        :     '+parseFloat(Maximum_Run_Length_in_Feet).toFixed(2))
//                    }
//               }
//               // num.toFixed(2)
// }else{
//   if(watperbulb==''){
//     Toast.show('Please Enter Watts per bulb')
//     }else if(totalbulb==''){
//       Toast.show('Please Enter Number of bulbs')
//     }else if(linevoltage==''){
//       Toast.show('Please Enter Line voltage')
//     }else{
//       var totalwatts=watperbulb * totalbulb
//       // settotalwat('Total Wattage                                :     '+totalwatts.toString().substring(0,5))
//       settotalwat('Total Wattage                                :     '+parseFloat(totalwatts).toFixed(2))
//       var totalAmperage=totalwatts/linevoltage
//       // settotalamp('Total Amperage                             :     '+totalAmperage.toString().substring(0,5))     
//       settotalamp('Total Amperage                             :     '+parseFloat(totalAmperage).toFixed(2))     
//     }
// }

// if(watperbulb!='' && totalbulb!='' && linevoltage!='' && ratingamps!='' && linevoltage2!='' && bulbwatage!='' && bulbspace!=''){
//   var totalwatts=watperbulb * totalbulb
//   settotalwat('Total Wattage                                :     '+parseFloat(totalwatts).toFixed(2))
//   var totalAmperage=totalwatts/linevoltage
//   settotalamp('Total Amperage                             :     '+parseFloat(totalAmperage).toFixed(2))     
//   var Number_of_Bulbs_in_Max_Run=(ratingamps * linevoltage2) / bulbwatage
//  setmaxrun('Number of Bulbs in Max Run        :     '+parseFloat(Number_of_Bulbs_in_Max_Run).toFixed(2)) 
//  var Maximum_Run_Length_in_Feet=(Number_of_Bulbs_in_Max_Run * bulbspace) / 12
//  setmaxlength('Maximum Run Length (Feet)        :     '+parseFloat(Maximum_Run_Length_in_Feet).toFixed(2))
// }

// if(watperbulb==''){
// Toast.show('Please Enter Watts per bulb')
// }else if(totalbulb==''){
//   Toast.show('Please Enter Number of bulb')
// }else if(linevoltage==''){
//   Toast.show('Please Enter Line voltage')
// }
// else if(ratingamps==''){
//   Toast.show('Please Enter Wire Rating (Amps)')
// }else if(linevoltage2==''){
//   Toast.show('Please Enter Line voltage')
// }else if(bulbwatage==''){
//   Toast.show('Please Enter Bulb Wattage')
// }else if(bulbspace==''){
//   Toast.show('Please Enter Bulb Spacing (Inches)')
// }else{
// var totalwatts=watperbulb * totalbulb
//  settotalwat('Total Wattage                                :     '+totalwatts.toString().substring(0,8))
// var totalAmperage=totalwatts/linevoltage
//  settotalamp('Total Amperage                             :     '+totalAmperage.toString().substring(0,8))
// var Number_of_Bulbs_in_Max_Run=(ratingamps * linevoltage2) / bulbwatage
//    setmaxrun('Number of Bulbs in Max Run        :     '+Number_of_Bulbs_in_Max_Run.toString().substring(0,8))
// var Maximum_Run_Length_in_Feet=(Number_of_Bulbs_in_Max_Run * bulbspace) / 12
// setmaxlength('Maximum Run Length (Feet)        :     '+Maximum_Run_Length_in_Feet.toString().substring(0,8))

// }
   


// if(ratingamps==''){
//   Toast.show('Please Enter Wire Rating (Amps)')
// }else if(linevoltage2==''){
//   Toast.show('Please Enter Line voltage')
// }else if(bulbwatage==''){
//   Toast.show('Please Enter Bulb Wattage')
// }else if(bulbspace==''){
//   Toast.show('Please Enter Bulb Spacing (Inches)')
// }else{
// var Number_of_Bulbs_in_Max_Run=(ratingamps * linevoltage2) / bulbwatage
//    setmaxrun('Number of Bulbs in Max Run        :     '+Number_of_Bulbs_in_Max_Run.toString().substring(0,4))
// var Maximum_Run_Length_in_Feet=(Number_of_Bulbs_in_Max_Run * bulbspace) / 12
// setmaxlength('Maximum Run Length (Feet)        :     '+Maximum_Run_Length_in_Feet.toString().substring(0,4))
// }



}

  return (
    
    <SafeAreaView style={styles.container}>
           <StatusBar barStyle="dark-content" backgroundColor="#000" />
           <Header Lable="String Light Calculator" Nav={() => props.navigation.goBack()} />

<View style={{flex:1,backgroundColor:'#15141b'}}>
    
<View style={{width:'100%',height:height,marginHorizontal:10,paddingHorizontal:15, backgroundColor:'#000',alignSelf:'center'}}>
<KeyboardAwareScrollView showsVerticalScrollIndicator={false}>

<Text style={{color:'#FFF',fontSize:25,marginTop:25,}}>Calculate Total Amperage</Text>

 {/* ********************************************************   */}
{/* <Text style={{color:'#fff',fontSize:15 ,marginTop:15}}>String Length</Text>
<View style={{width:'100%',height:40,borderRadius:28,marginTop:5,backgroundColor:'#34333a'}}>

     <Picker
        
        placeholder={"Select"}
        selectedValue={string}
        style={styles.input}
        textStyle={{color:'#fff'}}
        onValueChange={(itemValue, itemIndex) =>{
            setString(itemValue)
        }}
      >
    <Picker.Item label="Select" value="" />
    {data.map((item, index) => {
          return <Picker.Item key={index} label={item.label} value={item.value} />
      })}
  </Picker>

 </View> */}
{/* ********************************************************   */}
{/* <Text style={{color:'#fff',fontSize:15 ,marginTop:10}}>Bulb Wattage</Text>
<View style={{width:'100%',height:40,borderRadius:28,marginTop:5,backgroundColor:'#34333a'}}>

   <Picker
        
        placeholder={"Select"}
        selectedValue={bulb}
        style={styles.input}
        textStyle={{color:'#fff'}}
        onValueChange={(itemValue, itemIndex) =>{
            setBulb(itemValue)
        }}
      >
    <Picker.Item label="Select" value="" />
    {watdata.map((item, index) => {
          return <Picker.Item key={index} label={item.label} value={item.value} />
      })}
  </Picker>
 </View> */}
 {/* ********************************************************  */}
{/* <Text style={{color:'#fff',fontSize:15 ,marginTop:15}}>Circuit Ampage</Text>
<View style={{width:'100%',height:40,borderRadius:28,marginTop:5,backgroundColor:'#34333a'}}>
     <Picker
        placeholder={"Select"}
        selectedValue={circut}
        style={styles.input}
        textStyle={{color:'#fff'}}
        onValueChange={(itemValue, itemIndex) =>{
            setCircut(itemValue)
        }}
      >
    <Picker.Item label="Select" value="" />
    {ampragdata.map((item, index) => {
          return <Picker.Item key={index} label={item.label} value={item.value} />
      })}
  </Picker>

 </View> */}
 {/* ********************************************************   */}
<View style={{flexDirection:'row',justifyContent:'space-between',width:'100%'}}>
<View style={{width:'48%'}}>
         <Text style={{color:'#fff',fontSize:15 ,marginTop:15}}>Watts per bulb</Text>
         <View style={{width:'100%',height:47,borderRadius:28,marginTop:10}}>
          <TextInput
                value={watperbulb}
                onChangeText={(e) => setwatperbulb(e)}
                placeholder={''}
                placeholderTextColor="#bbbbbb"
                keyboardType="number-pad"
                autoCapitalize = 'none'
                style={styles.input}
              />
         </View>
</View>
<View style={{width:'48%'}}>
                <Text style={{color:'#fff',fontSize:15 ,marginTop:15}}>Number of bulbs</Text>
                <View style={{width:'100%',height:47,borderRadius:28,marginTop:10}}>
                <TextInput
                      value={totalbulb}
                      onChangeText={(e) => settotalbulb(e)}
                      placeholder={''}
                      placeholderTextColor="#bbbbbb"
                      keyboardType="number-pad"
                      autoCapitalize = 'none'
                      style={styles.input}
                    />
                </View>
</View>
</View>
<Text style={{color:'#fff',fontSize:15 ,marginTop:15}}>Line voltage</Text>
         <View style={{width:'100%',height:47,borderRadius:28,marginTop:10}}>
          <TextInput
                value={linevoltage}
                onChangeText={(e) => setlinevoltage(e)}
                placeholder={''}
                placeholderTextColor="#bbbbbb"
                keyboardType="number-pad"
                autoCapitalize = 'none'
                style={styles.input}
              />
         </View>
 {/* ********************************************************   */}
 

 <TouchableOpacity style={{width:'75%',height:40,backgroundColor:'#0000ff',alignSelf:'center',
borderRadius:30,flexDirection:'row',justifyContent:'center',marginTop:20,
shadowColor: '#fff',shadowOffset: { width: 5, height: 5 },shadowOpacity: 0.05,
shadowRadius: 2, elevation: 5}} 
onPress={()=>{myResult()}}>
<Text style={{color:'#FFF',fontSize:18,alignSelf:'center'}}>Calculate</Text>
</TouchableOpacity>
<Text style={{color:'#fff',fontSize:15 ,marginTop:10}}>Result</Text>
<View style={{width:'100%',height:80,borderRadius:20,marginTop:10, backgroundColor: '#34333a', paddingHorizontal:15,paddingVertical:10,}}>
<Text style={{color:'#fff',marginTop:5}}>{totalwat}</Text>
<Text style={{color:'#fff',marginTop:5}}>{totalamp}</Text>
 </View>
{/* ********************************************************   */}

<Text style={{color:'#FFF',fontSize:25,marginTop:20,}}>Calculate Max Run Length</Text>


 <View style={{flexDirection:'row',justifyContent:'space-between',width:'100%',marginTop:15}}>
<View style={{width:'48%'}}>
         <Text style={{color:'#fff',fontSize:15 ,marginTop:15}}>Wire Rating (Amps)</Text>
         <View style={{width:'100%',height:47,borderRadius:28,marginTop:10}}>
          <TextInput
                value={ratingamps}
                onChangeText={(e) => setratingamps(e)}
                placeholder={''}
                placeholderTextColor="#bbbbbb"
                keyboardType="number-pad"
                autoCapitalize = 'none'
                style={styles.input}
              />
         </View>
</View>
<View style={{width:'48%'}}>
                <Text style={{color:'#fff',fontSize:15 ,marginTop:15}}>Line Voltage</Text>
                <View style={{width:'100%',height:47,borderRadius:28,marginTop:10}}>
                <TextInput
                      value={linevoltage2}   
                      onChangeText={(e) => setlinevoltage2(e)}
                      placeholder={''}
                      placeholderTextColor="#bbbbbb"
                      keyboardType="number-pad"
                      autoCapitalize = 'none'
                      style={styles.input}
                    />
                </View>
</View>
</View> 

<View style={{flexDirection:'row',justifyContent:'space-between',width:'100%'}}>
<View style={{width:'48%'}}>
         <Text style={{color:'#fff',fontSize:15 ,marginTop:15}}>Bulb Wattage</Text>
         <View style={{width:'100%',height:47,borderRadius:28,marginTop:10}}>
          <TextInput
                value={bulbwatage}
                onChangeText={(e) => setbulbwatage(e)}
                placeholder={''}
                placeholderTextColor="#bbbbbb"
                keyboardType="number-pad"
                autoCapitalize = 'none'
                style={styles.input}
              />
         </View>
</View>
<View style={{width:'48%'}}>
                <Text style={{color:'#fff',fontSize:15 ,marginTop:15}}>Bulb Spacing (Inches)</Text>
                <View style={{width:'100%',height:47,borderRadius:28,marginTop:10}}>
                <TextInput
                      value={bulbspace}
                      onChangeText={(e) => setbulbspace(e)}
                      placeholder={''}
                      placeholderTextColor="#bbbbbb"
                      keyboardType="number-pad"
                      autoCapitalize = 'none'
                      style={styles.input}
                    />
                </View>
</View>
</View> 


 {/* ********************************************************   */}
                {/* <Text style={{color:'#fff',fontSize:15 ,marginTop:15}}>Bulb Spacing (Inches)</Text>
                  <View style={{width:'100%',height:47,borderRadius:28,marginTop:10}}>
                  <TextInput
                      value={bulbspace}
                      onChangeText={(e) => setbulbspace(e)}
                      placeholder={''}
                      placeholderTextColor="#bbbbbb"
                      keyboardType="number-pad"
                      autoCapitalize = 'none'
                      style={styles.input}
                    />
                </View> */}
  {/* ********************************************************   */}



 <TouchableOpacity style={{width:'75%',height:40,backgroundColor:'#0000ff',alignSelf:'center',
borderRadius:30,flexDirection:'row',justifyContent:'center',marginTop:20,
shadowColor: '#fff',shadowOffset: { width: 5, height: 5 },shadowOpacity: 0.05,
shadowRadius: 2, elevation: 5}} 
onPress={()=>{myResult2()}}>
<Text style={{color:'#FFF',fontSize:18,alignSelf:'center'}}>Calculate</Text>
</TouchableOpacity>
<Text style={{color:'#fff',fontSize:15 ,marginTop:10}}>Result</Text>
<View style={{width:'100%',height:80,borderRadius:20,marginTop:10, backgroundColor: '#34333a', paddingHorizontal:15,paddingVertical:10,}}>
<Text style={{color:'#fff',marginTop:5}}>{maxrun}</Text>
<Text style={{color:'#fff',marginTop:5}}>{maxlength}</Text>
 </View>
 {/* ********************************************************   */}
 <View style={{width:100,height:250}} />
       </KeyboardAwareScrollView>

</View>

</View>

      </SafeAreaView>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
      backgroundColor:'#15141b',
    },
  
  input: {
    height: 45,
    width:'100%',
    // fontSize: 15,
    paddingHorizontal:10,
    borderColor: null,
    backgroundColor: '#1e1e1e',
    borderRadius:10,
     color:'#fff',
    overflow:'hidden'
  },
   
   

})

export default StringLight;
