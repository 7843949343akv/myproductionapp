import React, { useEffect, useState } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Text,
  ScrollView,
  Dimensions,
  View,
  FlatList,
  Image,
  TextInput,
  Alert,
  Platform,
  RefreshControl
} from 'react-native';
import {TouchableOpacity } from 'react-native-gesture-handler';
import { dimensions, MyColor ,urls} from '../utils/constants';
import Header from '../Component/Header'
import moment from 'moment';
import Modal from 'react-native-modal';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Picker} from "native-base";
import DatePicker from 'react-native-datepicker'
import * as types  from '../redux/types';
import Activity_Indicator from '../Component/Activity_Indicator';
import {  useSelector, useDispatch } from 'react-redux';
import Toast from 'react-native-simple-toast';
import {add_task,get_attendance_detail,get_job_type,get_assign_task_list,complete_task,get_task_list,requestPostApi,requestGetApi} from '../WebApis/Service'
import DateTimePicker from '@react-native-community/datetimepicker';

const SubmitTask = (props) => {
  const token  = useSelector(state => state.user.token)   
  const user_details  = useSelector(state => state.user.user_details)
  const [loading,setLoading]=useState(false);
  const [data,setData]=useState([]);
  const [taskdata,setTaskData]=useState(null);
  const [clickId,setClickId]=useState('');
  const [modlevisual, setmodlevisual] = useState(false);
  const [modlevisual2, setmodlevisual2] = useState(false); 
  const [regularization_model, setregularization_model] = useState(false);
  const [projectname,setProjectName]=useState('');
  const [location,setLocation]=useState('');
  const [tasknote,setTasknote]=useState('');
  const [date, setDate] = useState('')  //new Date()    setDate('') ,setEndTime('') ,setStartTime(''), setjob(''), setReason(''), setRequestType(''),setProjectName(''),setLocation(''),setTasknote('')
  const [startTime,setStartTime]=useState('');
  const [endTime,setEndTime]=useState('');
  const [apistartTime,setapiStartTime]=useState('');
  const [apiendTime,setapiEndTime]=useState('');
  const [startTimeTrue,setStartTimeTrue]=useState(false);
  const [endTimeTrue,setEndTimeTrue]=useState(false);
  const[job,setjob]=useState('')
  const [jobdata,setjobdata]=useState([])

  const [reglurasitionItem,setreglurasitionItem]=useState(true)
  const [forgot_check_out,setforgot_check_out]=useState(false)
  const [checkin,setCheckin]=useState(false)
  const [checkin_time,setCheckin_time]=useState(false)
  const [checkintime,setCheckintime]=useState(false)
  const [checkout,setCheckout]=useState(false)
  const [checkout_time,setCheckout_time]=useState(false)
  const [checkincheck,setCheckincheck]=useState(false)
  const[myHours,setmyHours]=useState('00:00:00')
  const[itemdata,setitemdata]=useState('')

  const [reg_data,setreg_data]=useState([
    {label:"Forgot to check Out", value:"1"},
    {label:"Forgot to check Out & check In", value:"2"},
    {label:"Forgot to check In", value:"3"},
])
const[reason,setReason]=useState('')
const[requestType,setRequestType]=useState('')
const [refreshing, setRefreshing] = useState(false);


useEffect(()=>{
 myDate();
//  tastList(clickId);
console.log('The User Is',user_details)
getJobtype()
},[])

const getJobtype=async()=>{
  setLoading(true)
  const{responseJson,err}  = await requestGetApi(get_job_type,{},'GET',token)
  setLoading(false)
  console.log('jdjjf',responseJson);
       if(err==null){
            if(responseJson.status){
              var dumyArr=[]
              for(let i=0;i<responseJson.data.length;i++){
                dumyArr.push( {label:responseJson.data[i].job_type, value:responseJson.data[i].id})
              }
              setjobdata(dumyArr) 
             }else{ 
              Toast.show(responseJson.message);
             }
         }else{
         // Alert.alert(err)
         }
}


const wait = (timeout) => {
  return new Promise(resolve => setTimeout(resolve, timeout));
}

const onRefresh = React.useCallback(() => {
setRefreshing(true);
   myDate();
wait(2000).then(() => {
  setRefreshing(false)
});
}, []);

const myCheckout=(c_o,diff)=>{
  setCheckout(true)
  setCheckout_time(c_o)
  //const diffrent_Time= new Date(c_o)-new Date(checkintime);
  //msToTime(diffrent_Time)
  setmyHours(diff)
  setCheckincheck(false)
}
const myCheckin=(c_i)=>{
  if(!checkincheck)
  {
    setCheckout(false)
    setCheckin(true)
    setCheckincheck(true)
    setCheckout_time('')
    setmyHours('00:00:00')
    setCheckintime(c_i)
    //new Date()
    setCheckin_time(c_i)
    //new Date().toLocaleString().substring(10,24)
  }
}

const CheckIn_Out = (mystatus,item)=>{
   console.log('the status is ',mystatus)
 
  var task_types=''
  for(let i=0;i<jobdata.length;i++){
    console.log('kkkkk',jobdata[i].label);
    console.log('lllll',item.task_type);
    if(jobdata[i].label==item.task_type){
      task_types=jobdata[i].value
    }
  }
  
  setLoading(true)
    let formdata = new FormData();
    formdata.append("task_id",item.id);
    formdata.append("flag",mystatus);
    formdata.append("task_type",task_types);
    console.log('formdata is',formdata)
       fetch(urls.base_url+urls.check_in_and_check_out, {
             method: 'POST',
             headers: {
              'Content-Type': 'multipart/form-data',
              'Accept':'application/json',
              'Authorization': 'Bearer '+ token
            },
             body:formdata,
           }).then((response) => response.json())
                 .then((responseJson) => {
                  setLoading(false)
                          console.log('Chick in ,check out',responseJson)
          if(responseJson.status){
            if(mystatus=='Check-in'){
             // myCheckin(responseJson.check_in_time)
              tastList(clickId)
            }
            else{
             // myCheckout(responseJson.check_out_time,responseJson.diff)
              markAsDoneTask(item.id)
            }
            Toast.show(responseJson.message);
           
        }else{
          Toast.show(responseJson.message);
        }
                 }).catch((error) => {
                   console.log(error)
                   setLoading(false)
                 });
}

const Myvalidation =()=>{
  if(requestType == '')
  {
    Toast.show('Please select request type')
  }else if(date == '' || date.trim().length == 0){
    Toast.show('Please Select Date')

  }else if(reason == '' || reason.trim().length == 0){
    Toast.show('Please enter reason')

  }else if(startTime == '' || startTime.trim().length == 0){
    Toast.show('Please select start time')

  }else if(endTime == '' || endTime.trim().length == 0){
    Toast.show('Please select end time')

  }else{
    SubmitRegularisationReq()
  }
}

const SubmitRegularisationReq = ()=>{

  var R_task_types=''

  for(let i=0;i<jobdata.length;i++){
    if(jobdata[i].label==itemdata.task_type){
      R_task_types=jobdata[i].value
    }
  }
     setLoading(true)
      let formdata = new FormData();
      formdata.append("date",date);
      formdata.append("request_type",requestType);
      formdata.append("check_in",startTime);
      formdata.append("check_out",endTime);
      formdata.append("reason",reason);
      formdata.append("task_type",R_task_types);
      formdata.append("task_id",itemdata.id);
console.log(formdata);
         fetch(urls.base_url+urls.regularization_request, {
               method: 'POST',
               headers: {
                'Content-Type': 'multipart/form-data',
                'Accept':'application/json',
                'Authorization': 'Bearer '+ token
              },
               body:formdata,
             }).then((response) => response.json())
                   .then((responseJson) => {
                    setLoading(false)
                            console.log('Submit Regularization',responseJson)
            if(responseJson.status){
              Toast.show(responseJson.message);
              cleareData()
              setitemdata('')
              setregularization_model(false)
          }else{
            Toast.show(responseJson.message);
          }
                   }).catch((error) => {
                    setregularization_model(false)
                    console.log('hi',error)
                    setLoading(false)
                   });
}

const cleareData=()=>{
  setDate('') 
  setEndTime('') 
  setStartTime('')
  setjob('')
  setReason('')
 setRequestType('')
 setProjectName('')
 setLocation('')
 setTasknote('')
}

const submitTask= async ()=>{
  if(projectname=='' || startTime=='' || endTime=='' || location =='' || tasknote=='' || date=='' || job==''){
    Alert.alert('All are mandatory fields *')
  }else{
   let formdata = new FormData();
    formdata.append('worker_id',user_details.id);
    formdata.append('project_name',projectname);
    formdata.append('start_time',apistartTime);
    formdata.append('end_time',apiendTime);
    formdata.append('location',location);
    formdata.append( 'note',tasknote);
    formdata.append("task_type",job);
    formdata.append('date',date);
    console.log('the submitTask formdata is==>',formdata)
setLoading(true)
const{responseJson,err}  = await requestPostApi(add_task,formdata,'POST',token) 
setLoading(false)
if(err==null){
    if(responseJson.status){
         cleareData()
         setmodlevisual(false)
         setmodlevisual2(true)
    }else{
      Toast.show(responseJson.message);
    }
}else{
  Toast.show(err);
}
  }
}

const markAsDoneTask= async (taskid)=>{
  setLoading(true)
 const{responseJson,err}  = await requestGetApi(complete_task+'?task_id='+taskid,{},'GET',token)
 setLoading(false)
       if(err==null){
            if(responseJson.status){
                console.log('the API responce for Cart data',responseJson)
                tastList(clickId)
             }else{ 
               Alert.alert(responseJson.msg) 
             }
         }else{
          Alert.alert(err)
         }
 }

const tastList=async (idss)=>{
  setClickId(idss)
  setLoading(true)
const{responseJson,err}  = await requestGetApi(get_assign_task_list+'?from_date='+idss+'&to_date='+idss,{},'GET',token)
setLoading(false)
      if(err==null){
           if(responseJson.status){
            console.log('tasklist',responseJson.data);
            setTaskData(responseJson.data)
            }else{ 
              setTaskData(null)
              Toast.show(responseJson.message);
            }
        }else{
          setTaskData(null)
          Toast.show(err);
        }
}
  const myDate=()=>{
    let today = moment();
    let day = today.clone().startOf('month');
    let thisMonth = [];
    let totaldates=day.toString()
    let monthss=totaldates.substring(4,7)
    let dates=totaldates.substring(8,10)
    let year=totaldates.substring(11,16)
    var ids=(d,m,y)=>{
      if(m=='Jan'){
        return d+'-01-'+y
      }else if(m=='Fab'){
        return d+'-02-'+y
      } else if(m=='Mar'){
        return d+'-03-'+y
      }else if(m=='Apr'){
        return d+'-04-'+y
      }else if(m=='May'){
        return d+'-05-'+y
      }else if(m=='Jun'){
        return d+'-06-'+y
      }else if(m=='Jul'){
        return d+'-07-'+y
      }else if(m=='Aug'){
        return d+'-08-'+y
      }else if(m=='Sep'){
        return d+'-09-'+y
      }else if(m=='Oct'){
        return d+'-10-'+y
      }else if(m=='Nov'){
        return d+'-11-'+y
      }else if(m=='Dec'){
        return d+'-12-'+y
      }
    }
    setClickId(ids(dates,monthss,year))
    tastList(ids(dates,monthss,year));
    thisMonth.push({id:ids(dates,monthss,year),Mydate:dates+" "+monthss})
    while(day.add(1, 'day').isSame(today, 'month')) {
        let totaldates=day.toString()
        let years=totaldates.substring(11,16)
        let mydates=totaldates.substring(8,10)
        let months=totaldates.substring(4,7)
        
        thisMonth.push({id:ids(mydates,months,years),Mydate:mydates+" "+months})
     }
      setData(thisMonth)
  }


  const tConvert = (time)=> {
    // Check correct time format and split into components
    time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
  
    if (time.length > 1) { // If time format correct
      time = time.slice (1);  // Remove full string match value
      time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM 
      time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    return time.join (''); // return adjusted time or original string
  }
  


  return (
     <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <Header Lable="Submit Task" Nav={() => props.navigation.goBack()} />

<View style={{marginTop:15}}>
<FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={{ alignItems: 'center' }}
              data={data}
              keyExtractor={item => item.id}
              renderItem={({ item }) => {
                return (
                    <View style={{width:90,height:45,backgroundColor:clickId==item.id ? 'blue':'transparent',borderRadius:10,padding:5,margin:10,justifyContent:'center'}}>
                    <View style={{}}>
                    <Text style={{color:MyColor.TEXTCOLOR,fontSize:20,alignSelf:'center',fontWeight:'bold'}} onPress={()=>{tastList(item.id)}}>{item.Mydate}</Text>  
                    </View>
                    </View>
                      )
              }}
            />
  
  </View>  
  {/* <Text style={{color:MyColor.TEXTCOLOR,fontSize:20,fontWeight:'bold',left:15,marginTop:10}}></Text>   */}
  <ScrollView 
      // refreshControl={
      //     <RefreshControl
      //       refreshing={refreshing}
      //       onRefresh={onRefresh}
      //     />
      //   }
        >   
<View style={{marginTop:10}}>

<View>
      <FlatList
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={{ alignItems: 'center' }}
              data={taskdata}
              keyExtractor={item => item.id}
              renderItem={({ item ,index}) => {
                return (
                    <View style={{width:dimensions.SCREEN_WIDTH-30,backgroundColor:MyColor.BOXCOLOR,borderRadius:10,padding:15,marginVertical:10,justifyContent:'center'}}>
                      <View style={{flexDirection:'row',alignItems:'center'}}>
                      <Image source={index%2==0 ? require('../assets/rgb.png'):require('../assets/fixcer.png')}style={{width:35,height:35,alignSelf:'center',zIndex:-999}}resizeMode='cover'></Image>
                     <View>
                      <Text style={{color:'#FFF',left:12,fontSize:12,fontWeight:'bold'}}>{item.project_name+'('+item.total_time+')'}</Text>
                      <Text style={{color:'#FFF',marginLeft:12,fontSize:12,marginVertical:6}}>Job Type : {item.task_type}</Text>
                      </View> 
                       </View> 
<View style={{width:'100%',height:2,backgroundColor:MyColor.SUBBOXCOLOR,marginVertical:10}} />
<Text style={{color:'rgba(256,256,256,0.7)',fontSize:12}}>Task Notes: {item.note}</Text>

<View style={{}}>
<View style={{flexDirection:'row',justifyContent:'space-between'}}>
<Text style={{color:'#FFF',fontSize:14,alignSelf:'center',marginTop:5}}>Working Hours</Text>
<Text style={{color:'#FFF',fontSize:14,alignSelf:'center',marginTop:5,color:'red'}} onPress={()=>{
   console.log('hello ji==>>',item);
   setregularization_model(true)
   setitemdata(item)
   cleareData()
  }}>Regularization</Text>
</View>

<Text style={{color:'#FFF',fontSize:14,alignSelf:'center',top:10}}>{item.total_taking_time}</Text>
<View style={{flexDirection:'row',justifyContent:'space-between',marginTop:20,width:'90%',alignSelf:'center'}}>
<View style={{width:'48%',height:30,backgroundColor:'green',borderRadius:15,justifyContent:'center',flexDirection:'row'}}>
<Text style={{color:MyColor.TEXTCOLOR,fontSize:13,alignSelf:'center'}} onPress={()=>{
   CheckIn_Out('Check-in',item)
  }}>CHECK-IN</Text>
{item.check_in !=null ? 
<View style={{width:18,height:18,borderRadius:9,alignSelf:'center',backgroundColor:'#FFF',justifyContent:'center',left:15}}>
<Image source={require('../assets/check.png')} style={{ width: 13, height: 13,borderRadius:8,alignSelf:'center'}}></Image> 
</View>
  : null }
</View>
<View style={{width:'48%',height:30,backgroundColor:'red',borderRadius:15,justifyContent:'center',flexDirection:'row'}}>
<Text style={{color:MyColor.TEXTCOLOR,fontSize:13,alignSelf:'center'}}onPress={()=>{
   CheckIn_Out('Check-out',item)
  }}>CHECK-OUT</Text>
{item.check_out !=null ? 
<View style={{width:18,height:18,borderRadius:9,alignSelf:'center',backgroundColor:'#FFF',justifyContent:'center',left:15}}>
<Image source={require('../assets/check.png')} style={{ width: 13, height: 13,borderRadius:8,alignSelf:'center'}}></Image> 
</View>
  : null }
</View>
</View>
<View style={{flexDirection:'row',justifyContent:'space-between',width:'77%',alignSelf:'center',marginTop:10}}>
<Text style={{color:MyColor.TEXTCOLOR,fontSize:13,alignSelf:'center'}}>{item.check_in}</Text>
<Text style={{color:MyColor.TEXTCOLOR,fontSize:13,alignSelf:'center'}}>{item.check_out}</Text>
</View>
</View>

</View>
                )
              }}
            />
 </View>
</View>
<View style={{width:100,height:100}} />
</ScrollView>  


<TouchableOpacity style={{width:'90%',height:55,backgroundColor:'#0000ff',alignSelf:'center',borderRadius:30,flexDirection:'row',justifyContent:'center',marginBottom:20}}
  onPress={()=>setmodlevisual(true)}>
<Text style={{color:'#FFF',fontSize:20,alignSelf:'center'}}>Add New Task</Text>
</TouchableOpacity>

{/* ##############&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&  Model1 Add Task&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&   */}

<Modal
        isVisible={modlevisual}
        swipeDirection="down"
        onSwipeComplete={(e) => {
          cleareData()
          setmodlevisual(false)
        }}
          scrollTo={() => {}}
          scrollOffset={1}
          propagateSwipe={true}
        coverScreen={false}
        backdropColor='transparent'
        style={{ justifyContent: 'flex-end', margin: 0, backgroundColor: 'rgba(20,20,20,1)' }}
      >
        <View style={{ height: '90%', backgroundColor: MyColor.SUBBOXCOLOR, borderTopLeftRadius: 30, borderTopRightRadius: 30, padding: 20,marginHorizontal:10 }}>
          <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
    <Text style={{color:'#FFF',fontSize:25,marginTop:10,}}>Add New Task</Text>

{/* ********************************************************   */}
<Text style={{color:'#fff',fontSize:15 ,marginTop:15}}>Select Date</Text>
<View style={{width:'100%',height:39,borderRadius:28,marginTop:10,zIndex:999}}>

            <DatePicker
                      customStyles={{
                        dateInput: {borderColor:'transparent',left:-90},
                        dateText: {color:'#fff'},
                        dateIcon: styles.dateIcon,
                        dateplaceholder: {
                          alignContent: 'flex-start',
                         
                        },
                        placeholderText: {
                          fontSize: 15,
                          color: '#FFF',
                          marginLeft: '5%',
                          left:-100
                        },
                        zIndex:99999
                      }}
                     
                      androidMode={'spinner'}
                      readOnly={true}
                      style={[styles.datePickerSelectInput]}
                      date={date}
                      mode="date"
                      placeholder={'Select date'}
                      minDate={new Date ()}
                      format='YYYY-MM-DD'
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      iconSource={require ('../assets/time-attendace.png')}
                      onDateChange={date => {
                        setDate(date)
                      }}
                    />



</View>
{/* ********************************************************   */}

<Text style={{color:'#fff',fontSize:15 ,marginTop:20}}>Job Type</Text>
 
<View style={{width:'100%',height:40,borderRadius:20,backgroundColor:'#34333a',marginTop:10}}>

      <Picker
        placeholder={"Select Job Type"}
        selectedValue={job}
        style={styles.input}
        textStyle={{color:'#fff'}}
        onValueChange={(itemValue, itemIndex) =>{
            setjob(itemValue)
        }}
      >
    <Picker.Item label="Select Job Type" value="" />
    {jobdata.map((item, index) => {
          return <Picker.Item key={index} label={item.label} value={item.value} />
      })}
  </Picker>

 </View>
{/* ********************************************************   */}

<Text style={{color:'#fff',fontSize:15 ,marginTop:20}}>Project Name</Text>
<View style={{width:'100%',height:47,borderRadius:28,marginTop:10}}>

  <TextInput
        value={projectname}
        onChangeText={(e) => setProjectName(e)}
        placeholder={''}
        placeholderTextColor="#bbbbbb"
       // keyboardType="number-pad"
        autoCapitalize = 'none'
        style={styles.input}
      />


 </View>
 {/* //////////////////////////////////////////////////// */}
{/* <View style={{width:'100%',height:60,borderRadius:28,marginTop:5,flexDirection:'row',justifyContent:'space-between'}}> */}
<View style={{width:'100%',height:60,marginTop:20}}>
  <Text style={{color:'#FFF',fontSize:15,top:-7}}>Start Time</Text>
<View style={{width:'100%',height:47,backgroundColor:'#34333a',borderRadius:10}}>
           {Platform.OS=='ios' ? 
            
              <DatePicker
                      customStyles={{
                        dateInput: {borderColor:'transparent'},
                        dateText: {color:'#fff'},
                        dateIcon: styles.dateIcon,
                        dateplaceholder: {
                          alignContent: 'flex-start',
                         
                        },
                        placeholderText: {
                          fontSize: 15,
                          color: '#FFF',
                          alignSelf:'flex-start',
                          left:10
                        },
                      }}
                      showIcon={false}
                      androidMode={'spinner'}
                      is24Hour={false}
                      readOnly={true}
                      style={[styles.datePickerSelectInput]}
                   //   date={startTime}
                      mode="Time"
                      placeholder={startTime!='' ?startTime:'Select Time' }
                     // maxDate={new Date ()}
                     //format='YYYY-MM-DD'
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      // iconSource={require ('../assets/time-attendace.png')}
                      onDateChange={sTime => {
                        let st=sTime.substring(11,19)
                        let mt= tConvert(st) 
                        setapiStartTime(st)
                        setStartTime(mt)
                      }}
                    />
                    : 
                    startTimeTrue ? 
                    <View>
                      <DateTimePicker
                        value={new Date()}
                        mode='time'
                        is24Hour={false}
                        display="spinner"
                        onChange={(event,sTime) => {
                          setStartTimeTrue(false) 
                        let st=sTime.toString()
                        let nst=st.substring(16,24) 
                        let mt= tConvert(nst) 
                          setapiStartTime(nst)
                          setStartTime(mt)
                        }}
                      /> 
                      </View>
                    :
                    <TouchableOpacity style={{width:'100%',height:'100%',justifyContent:'center'}}>
                    <Text style={{alignSelf:'center',fontSize:15,color:'#fff'}} onPress={()=>{setStartTimeTrue(true)}}>{startTime!='' ?startTime:'Select Time'}</Text>
                    </TouchableOpacity>
              }
       
</View>
</View>

<View style={{width:'100%',height:60,marginTop:25}}>
<Text style={{color:'#FFF',fontSize:15,top:-7}}>End Time</Text>
<View style={{width:'100%',height:47,backgroundColor:'#34333a',borderRadius:10}}>
            
{Platform.OS=='ios' ? 
               <DatePicker
                      customStyles={{
                        dateInput: {borderColor:'transparent'},
                        dateText: {color:'#fff'},
                        dateIcon: styles.dateIcon,
                        dateplaceholder: {
                          alignContent: 'flex-start',
                         
                        },
                        placeholderText: {
                          fontSize: 15,
                          color: '#FFF',
                          alignSelf:'flex-start',
                          left:10
                        },
                       
                      }}
                      showIcon={false}
                      androidMode={'spinner'}
                      is24Hour={false}
                      readOnly={true}
                      style={[styles.datePickerSelectInput]}
                     // date={endTime}
                      mode="Time"
                      placeholder={endTime!='' ?endTime:'Select Time' }
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      onDateChange={sTime => {
                        let st=sTime.substring(11,19)
                        let mt= tConvert(st) 
                        setapiEndTime(st)
                        setEndTime(mt)
                      }}
                    />
                    : 
                    endTimeTrue ? 
                    <View>
                       <DateTimePicker
                        value={new Date()}
                        mode='time'
                        is24Hour={false}
                        display="spinner"
                        onChange={(event,sTime) => {
                          setEndTimeTrue(false) 
                        let st=sTime.toString()
                        let nst=st.substring(16,24) 
                        let mt= tConvert(nst) 
                          setapiEndTime(nst)
                          setEndTime(mt)
                        }}
                      /> 
                      </View>
                    :
                    <TouchableOpacity style={{width:'100%',height:'100%',justifyContent:'center'}}>
                    <Text style={{alignSelf:'center',fontSize:15,color:'#fff'}} onPress={()=>{setEndTimeTrue(true)}}>{endTime!='' ?endTime:'Select Time'}</Text>
                    </TouchableOpacity>
                
      }
</View>
</View>

{/* </View> */}

{/* //////////////////////////////////////////////////// */}
<Text style={{color:'#fff',fontSize:15 ,marginTop:20}}>Enter Location</Text>
<View style={{width:'100%',height:47,borderRadius:28,marginTop:10}}>

  <TextInput
        value={location}
        onChangeText={(e) => setLocation(e)}
        placeholder={''}
        placeholderTextColor="#bbbbbb"
       // keyboardType="number-pad"
        autoCapitalize = 'none'
        style={styles.input}
      />
 </View>

 <Text style={{color:'#fff',fontSize:15 ,marginTop:10}}>Task Note</Text>
<View style={{width:'100%',height:100,borderRadius:20,marginTop:10}}>

  <TextInput
        value={tasknote}
        onChangeText={(e) => setTasknote(e)}
        placeholder={''}
        placeholderTextColor="#bbbbbb"
        multiline={true}
        maxLength={500}
       // keyboardType="number-pad"
        autoCapitalize = 'none'
        style={[styles.input,{height:100,borderRadius:20,paddingHorizontal:15,paddingVertical:10}]}
      />

 </View>

{/* ********************Submit Task button************************** */}
          <TouchableOpacity style={{marginTop:20,width:'90%',height:55,backgroundColor:'#0000ff',alignSelf:'center',borderRadius:30,flexDirection:'row',justifyContent:'center'}}
            onPress={()=>{
             submitTask()
                    }
            }>
            <Text style={{color:'#FFF',fontSize:20,alignSelf:'center'}}
            onPress={()=>{
              submitTask()
             
                }
                }
            >Submit Task</Text>
            </TouchableOpacity>
            <View style={{width:10,height:20}} />
{/* *********************end add button**************************** */}
<View style={{width:100,height:100}} />
            </KeyboardAwareScrollView>
            { loading ?
            <Activity_Indicator />
            :
            null
            }
            </View>
</Modal>

{/* ##############&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&  Model2 Task Submited success &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&   */}
<Modal
        isVisible={modlevisual2}
        swipeDirection="down"
        onSwipeComplete={(e) => {
          cleareData()  //
          setmodlevisual2(false)
        }}
          scrollTo={() => {}}
          scrollOffset={1}
          propagateSwipe={true}
        coverScreen={false}
        backdropColor='transparent'
        style={{ justifyContent: 'flex-end', margin: 0, backgroundColor: 'rgba(20,20,20,1)' }}
      >
        <View style={{ height: '60%', backgroundColor: MyColor.SUBBOXCOLOR, borderTopLeftRadius: 30, borderTopRightRadius: 30, padding: 20 }}>
          <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>

          <View style={{ alignSelf: 'center',marginTop:10 }}>
           <Image source={require('../assets/task-submit.png')} style={{ width: 155, height: 120, alignSelf: 'center', zIndex: -999 }} resizeMode='cover'></Image>
          </View>

<Text style={{color:'#FFF',fontSize:22,alignSelf:'center',marginTop:20}}>Task</Text>
<Text style={{color:'#FFF',fontSize:22,alignSelf:'center',marginTop:5}}>Submit Successfully</Text>


          <TouchableOpacity style={{width:'90%',height:55,backgroundColor:'#0000ff',alignSelf:'center',borderRadius:30,flexDirection:'row',justifyContent:'center',marginTop:20}}
            onPress={()=>
            setmodlevisual2(false)
            }>
            <Text style={{color:'#FFF',fontSize:20,alignSelf:'center'}}
             onPress={()=>
                setmodlevisual2(false)
                }>Close</Text>
            </TouchableOpacity>
            <View style={{width:100,height:100}} />
            </KeyboardAwareScrollView>
            { loading ?
              <Activity_Indicator />
              :
              null
              }
            </View>
</Modal>

{/* ##############&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&  Model3 Submit Regularization &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&   */}

<Modal
        isVisible={regularization_model}
        swipeDirection="down"
        onSwipeComplete={(e) => {
          cleareData()
          setregularization_model(false)
        }}
          scrollTo={() => {}}
          scrollOffset={1}
          propagateSwipe={true}
        coverScreen={false}
        backdropColor='transparent'
        style={{ justifyContent: 'flex-end', margin: 0, backgroundColor: 'rgba(25,25,25,10)',color:'white' }}
      >
        <View style={{ height: '79%', backgroundColor: MyColor.BOXCOLOR, borderTopLeftRadius: 30, borderTopRightRadius: 30, padding: 20 }}>
          <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
          <Text style={{ color: MyColor.TEXTCOLOR, fontSize: 21,alignSelf:'center',marginTop:20}}>Submit Regularization Request</Text>
{/* ********************************************************   */}
<Text style={{color:'#fff',fontSize:15 ,marginTop:15}}>Select Date</Text>
<View style={{width:'100%',height:39,borderRadius:10,marginTop:10,zIndex:999}}>

            <DatePicker
                      customStyles={{
                        dateInput: {borderColor:'transparent',left:-90},
                        dateText: {color:'#fff'},
                        dateIcon: styles.dateIcon,
                        dateplaceholder: {
                          alignContent: 'flex-start',
                         
                        },
                        placeholderText: {
                          fontSize: 15,
                          color: '#FFF',
                          marginLeft: '5%',
                          left:-100
                        },
                        zIndex:99999
                      }}
                      androidMode={'spinner'}
                      readOnly={true}
                      style={[styles.datePickerSelectInput]}
                      date={date}
                      mode="date"
                      placeholder={'Select date'}
                      maxDate={new Date ()}
                      format='YYYY-MM-DD'
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      iconSource={require ('../assets/time-attendace.png')}
                      onDateChange={date => {
                        setDate(date)
                      }}
                    />
         

</View>
{/* ******************************************************** */}

<View style={{width:'100%',height:60,borderRadius:28,marginTop:15,flexDirection:'row',justifyContent:'space-between'}}>
<View style={{width:'45%',height:60,}}>
  <Text style={{color:'#FFF',fontSize:15,marginVertical:4}}>Start Time</Text>
<View style={{width:'100%',height:45,backgroundColor:'#34333a',borderTopLeftRadius:10,borderBottomLeftRadius:10}}>
           {Platform.OS=='ios' ? 
            
              <DatePicker
                      customStyles={{
                        dateInput: {borderColor:'transparent'},
                        dateText: {color:'#fff'},
                        dateIcon: styles.dateIcon,
                        dateplaceholder: {
                          alignContent: 'flex-start',
                         
                        },
                        placeholderText: {
                          fontSize: 15,
                          color: '#FFF',
                 
                        },
                      }}
                      showIcon={false}
                      androidMode={'spinner'}
                      is24Hour={false}
                      readOnly={true}
                      style={[styles.datePickerSelectInput]}
                   //   date={startTime}
                      mode="Time"
                      placeholder={startTime!='' ?startTime:'Select Time' }
                     // maxDate={new Date ()}
                     //format='YYYY-MM-DD'
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      // iconSource={require ('../assets/time-attendace.png')}
                      onDateChange={sTime => {
                        let st=sTime.substring(11,19)
                        let mt= tConvert(st) 
                        setapiStartTime(st)
                        setStartTime(mt)
                      }}
                    />
                    : 
                    startTimeTrue ? 
                    <View>
                      <DateTimePicker
                        value={new Date()}
                        mode='time'
                        is24Hour={false}
                        display="spinner"
                        onChange={(event,sTime) => {
                          setStartTimeTrue(false) 
                        let st=sTime.toString()
                        let nst=st.substring(16,24) 
                        let mt= tConvert(nst) 
                          setapiStartTime(nst)
                          setStartTime(mt)
                          
                        }}
                      /> 
                      </View>
                    :
                    <TouchableOpacity style={{width:'100%',height:'100%',justifyContent:'center'}}>
                    <Text style={{alignSelf:'center',fontSize:15,color:'#fff'}} onPress={()=>{setStartTimeTrue(true)}}>{startTime!='' ?startTime:'Select Time'}</Text>
                    </TouchableOpacity>
                
      }
       
</View>
</View>

<View style={{width:'45%',height:60,}}>
<Text style={{color:'#FFF',fontSize:15,marginVertical:4}}>End Time</Text>
<View style={{width:'100%',height:45,backgroundColor:'#34333a',borderTopRightRadius:10,borderBottomRightRadius:10}}>
            
{Platform.OS=='ios' ? 
               <DatePicker
                      customStyles={{
                        dateInput: {borderColor:'transparent'},
                        dateText: {color:'#fff'},
                        dateIcon: styles.dateIcon,
                        dateplaceholder: {
                          alignContent: 'flex-start',
                         
                        },
                        placeholderText: {
                          fontSize: 15,
                          color: '#FFF',
                        },
                       
                      }}
                      showIcon={false}
                      androidMode={'spinner'}
                      is24Hour={false}
                      readOnly={true}
                      style={[styles.datePickerSelectInput]}
                     // date={endTime}
                      mode="Time"
                      placeholder={endTime!='' ?endTime:'Select Time' }
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      onDateChange={sTime => {
                        let st=sTime.substring(11,19)
                        let mt= tConvert(st) 
                        setapiEndTime(st)
                        setEndTime(mt)
                      }}
                    />
                    : 
                    endTimeTrue ? 
                    <View>
                       <DateTimePicker
                        value={new Date()}
                        mode='time'
                        is24Hour={false}
                        display="spinner"
                        onChange={(event,sTime) => {
                          setEndTimeTrue(false) 
                        let st=sTime.toString()
                        let nst=st.substring(16,24) 
                        let mt= tConvert(nst) 
                          setapiEndTime(nst)
                          setEndTime(mt)
                        }}
                      /> 
                      </View>
                    :
                    <TouchableOpacity style={{width:'100%',height:'100%',justifyContent:'center'}}>
                    <Text style={{alignSelf:'center',fontSize:15,color:'#fff'}} onPress={()=>{setEndTimeTrue(true)}}>{endTime!='' ?endTime:'Select Time'}</Text>
                    </TouchableOpacity>
                
      }
</View>
</View>

</View>

{/* ********************************************************   */}
<Text style={{color:'#fff',fontSize:15 ,marginTop:25}}>Request Type</Text>
<View style={{width:'100%',height:45,borderRadius:28,marginTop:5,backgroundColor:'#34333a',borderRadius:10}}>

     <Picker
        
        placeholder={"Select"}
        selectedValue={requestType}
        style={styles.input}
        textStyle={{color:'#fff'}}
        onValueChange={(itemValue, itemIndex) =>{
            setRequestType(itemValue)
        }}
      >
    <Picker.Item label="Select" value="" />
    {reg_data.map((item, index) => {
          return <Picker.Item key={index} label={item.label} value={item.value} />
      })}
  </Picker>


 </View>

 {/* ********************************************************   */}
{/* 
 <Text style={{color:'#fff',fontSize:15 ,marginTop:20}}>Job Type</Text>
 <View style={{width:'100%',height:40,borderRadius:20,backgroundColor:'#34333a',marginTop:5}}>

<Picker
  placeholder={"Select Job Type"}
  selectedValue={job}
  style={styles.input}
  textStyle={{color:'#fff'}}
  onValueChange={(itemValue, itemIndex) =>{
      setjob(itemValue)
  }}
>
<Picker.Item label="Select Job Type" value="" />
{jobdata.map((item, index) => {
    return <Picker.Item key={index} label={item.label} value={item.value} />
})}
</Picker>

</View> */}

 {/* ********************************************************   */}
<Text style={{color:'#fff',fontSize:15 ,marginTop:10}}>Reason</Text>
<View style={{width:'100%',height:100,borderRadius:20,marginTop:10}}>

  <TextInput
        value={reason}
        onChangeText={(e) => setReason(e)}
        placeholder={''}
        placeholderTextColor="#bbbbbb"
        multiline={true}
        maxLength={500}
       // keyboardType="number-pad"
        autoCapitalize = 'none'
        style={[styles.input,{height:100,borderRadius:20,paddingHorizontal:15,paddingVertical:10}]}
      />


 </View>


           <View style={{ width: '85%', height: 45, backgroundColor: '#0000ff', alignSelf: 'center', borderRadius: 30, flexDirection: 'row', justifyContent: 'center', marginTop: 40 }}  >
              <Text style={{ color: MyColor.TEXTCOLOR, fontSize: 16, alignSelf: 'center' }} onPress={() => { Myvalidation() }}>Submit for Admin Approval</Text>
            </View>

            <View style={{width:100,height:100}} />
        </KeyboardAwareScrollView>
        { loading ?
        <Activity_Indicator />
        :
        null
        }
        </View>
 </Modal>



       { loading ?
        <Activity_Indicator />
        :
        null
        }
      </SafeAreaView>
   
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
      backgroundColor:MyColor.BGCOLOR
    },
    input: {
        paddingLeft: 15,
        height: 45,
        width:'100%',
        fontSize: 17,
        borderColor: null,
        backgroundColor: '#34333a',
        borderRadius:10,
        color:'#fff'
    
      },
      dateIcon:{
        width:20,
        height:18,
        marginRight:20
      },
      datePickerSelectInput:{
        height: 45,
        width:'100%',
        fontSize: 15,
        borderColor: null,
        backgroundColor: '#34333a',
        borderRadius:10,
        color:'#fff',
      }
})

export default SubmitTask;
