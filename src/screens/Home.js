import React, { useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Text,
  ScrollView,
  View,
  Image,
  Dimensions,
  TextInput,
  FlatList,
  Platform,
  Linking,
  Alert,
  RefreshControl,
} from 'react-native';
import {TouchableOpacity } from 'react-native-gesture-handler';
import { dimensions,MyColor } from '../utils/constants';
import {  useSelector, useDispatch } from 'react-redux';
import * as types  from '../redux/types';
import { useState } from 'react';
import { SliderBox } from "react-native-image-slider-box";
import Modal from 'react-native-modal';
import { Picker} from "native-base";
import DatePicker from 'react-native-datepicker'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import DateTimePicker from '@react-native-community/datetimepicker';
import { paymentUrl,success_token ,paymentRes,requestGetApi,purchage_subscription} from '../WebApis/Service';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-simple-toast';
import Activity_Indicator from '../Component/Activity_Indicator';
import ImageBackground from 'react-native/Libraries/Image/ImageBackground';

var width = Dimensions.get('window').width
var height = Dimensions.get('window').height

const Home  = (props) => {
  const userData  = useSelector(state => state.user.user_details)
  const [notify,setNotify]=useState(false)
  const [forgot_check_out,setforgot_check_out]=useState(false) 
  const [checkin,setCheckin]=useState(false)
  const [checkin_time,setCheckin_time]=useState(false)
  const [checkintime,setCheckintime]=useState(false)
  const [checkout,setCheckout]=useState(false)
  const [checkout_time,setCheckout_time]=useState(false)
  const [checkincheck,setCheckincheck]=useState(false)
  const[myHours,setmyHours]=useState('00:00:00')
  const [modlevisual, setmodlevisual] = useState(false);
  const [data,setData]=useState([
    {label:"Forgot to check Out", value:"1"},
    {label:"Forgot to check Out & check In", value:"2"},
    {label:"Forgot to check In", value:"3"},
])
const[reason,setReason]=useState('')
const[requestType,setRequestType]=useState('')
const [date, setDate] = useState(new Date())
const [tasknote,setTasknote]=useState('');
const [loading,setLoading]=useState(false)
const dispatch =  useDispatch();
const tokens  = useSelector(state => state.user.token)
const [refreshing, setRefreshing] = React.useState(false);

// const [date, setDate] = useState(new Date())
useEffect(()=>{
  // fetchSuccessDetails()
 // AvoidPayment()
},[])



    const wait = (timeout) => {
      return new Promise(resolve => setTimeout(resolve, timeout));
    }
    
  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
   // fetchSuccessDetails()
    wait(2000).then(() => {
      setRefreshing(false)
    });
  }, []);



  
const fetchSuccessDetails=async()=>{
  setLoading(true)
  // const{responseJson,err}  = await requestGetApi(success_token+t_token+'&plan_id='+p_id,{},'GET',tokens)
  // const{responseJson,err}  = await requestGetApi(success_token+t_token,{},'GET',token)
   const{responseJson,err}  = await requestGetApi(paymentRes,{},'GET',tokens)
  setLoading(false)
  console.log('the resof success',responseJson)
       if(err==null){
            if(responseJson.status){
             console.log('Status True',responseJson)
                  //  MyBackButton()
                  AsyncStorage.setItem("user",JSON.stringify(responseJson.data));
                  // AsyncStorage.setItem("token",JSON.stringify(responseJson.data.token));
                    dispatch({
                    type:types.SAVE_USER_RESULTS,
                    user:responseJson.data
                    })
             props.navigation.navigate('Home');
            }else{ 
             // Toast.show(responseJson.message);
             console.log('URL',paymentUrl+userData.user_id)
             Linking.openURL(paymentUrl+userData.user_id) 
              props.navigation.navigate('Home');
             }
         }else{
          console.log('URL',paymentUrl+userData.user_id)
          Linking.openURL(paymentUrl+userData.user_id)
          // Toast.show(err);
         }
}


  const Mycomp=(img,lable,action,size)=>{
    return(
     <TouchableOpacity style={{width:width*45/100,height:100,backgroundColor:'#201f28',borderRadius:15,justifyContent:'center',borderWidth:1,borderColor:'blue',padding:10}} onPress={action}>
     <Text style={{textAlign:'center',color:'white',alignSelf:'center'}}>{lable}</Text>
     </TouchableOpacity>
    );
  };

const BySubscriptionPress=()=>{
 
  if(userData){
    if(userData.plan=='0' && userData.user_group=='1')
    {
      // https://www.nileprojects.in/my_production_assistant/subscription/plan/TWNDNFoxQit0M3FicG9ad3ROQzZEZz09
    //  props.navigation.navigate('Subscription') userData.id
      props.navigation.navigate('MyWebView',{url:paymentUrl+userData.id,planid:'2'})
    //  Linking.openURL(paymentUrl+userData.user_id)
    //  fetchSuccessDetails()
    }
  }else{
    props.navigation.navigate('Login')
       }
}

const myCheckout=()=>{
  setCheckout(true)
  setCheckout_time(new Date().toLocaleString().substring(11,24))
  const diffrent_Time= new Date()-new Date(checkintime);
  msToTime(diffrent_Time)
  setCheckincheck(false)
}

const msToTime = (duration) =>{
  var milliseconds = parseInt((duration % 1000) / 100),
  seconds = Math.floor((duration / 1000) % 60),
  minutes = Math.floor((duration / (1000 * 60)) % 60),
  hours = Math.floor((duration / (1000 * 60 * 60)));

hours = (hours < 10) ? "0" + hours : hours;
minutes = (minutes < 10) ? "0" + minutes : minutes;
seconds = (seconds < 10) ? "0" + seconds : seconds;
if(hours.length>2 || hours=='NaN')
{
  setmyHours('NaN')
}
  else{
  setmyHours(hours + ":" + minutes + ":" + seconds)
     }
}

const myCheckin=()=>{
  if(!checkincheck)
  {
    setCheckout(false)
    setCheckin(true)
    setCheckincheck(true)
    setCheckout_time('')
    setmyHours('00:00:00')
    setCheckintime(new Date())
    setCheckin_time(new Date().toLocaleString().substring(11,24))
  }
  
}

const MytimeAttendance_Click=()=>{
  if(userData){
     if(userData.plan=='1'){
      props.navigation.navigate('EmployeeList')  
     }else{
      // props.navigation.navigate('Subscription') 
      props.navigation.navigate('MyWebView',{url:paymentUrl+userData.id,planid:'2'})
      // Linking.openURL(paymentUrl+userData.user_id)
     // fetchSuccessDetails()
     }
  }else{
    props.navigation.navigate('Login')
  }
}






  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <ImageBackground source={require('../assets/home-bg.jpg')} style={{width:'100%',height:'100%',borderRadius:15,overflow:'hidden',}}>
      
     <ScrollView
        //contentContainerStyle={styles.scrollView}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
          />
        }
      >
 
<View style={{width:width*94/100,alignSelf:'center',marginTop:dimensions.SCREEN_HEIGHT*40/100}}>
<Text style={{fontWeight:'bold',fontSize:18,color:'#fff',textAlign:'center',margin:20}}>Self Help Tools</Text>
<View style={{flexDirection:'row',justifyContent:'space-between',width:width*94/100,alignSelf:'center'}}>
  {Mycomp(require('../assets/calculator.png'),'Truss Calculator',()=> {props.navigation.navigate('TrussCalculator')},33)}
  {Mycomp(require('../assets/DMX.png'),'DMX Analogue Dip switch calculator',()=> {props.navigation.navigate('DmxDigital')},27)}
</View>

{/* ************************************************************ */}
<View style={{flexDirection:'row',justifyContent:'space-between',width:width*94/100,alignSelf:'center',marginTop:15}}>
{Mycomp(require('../assets/rgb.png'),'RGB Color',()=> {props.navigation.navigate('RgbColor')},27)}
{Mycomp(require('../assets/calculator.png'),'String Light Calculator',()=> {props.navigation.navigate('StringLight')},33)}
</View>
{/* ************************************************************ */}
{/* {Mycomp(require('../assets/time-attendace.png'),'Time/Attendance Management',()=> MytimeAttendance_Click(),26)} */}
{/* ************************************************************ */}


</View>

       </ScrollView>
    
{loading ?
<Activity_Indicator />
: null }
</ImageBackground>
      </SafeAreaView>
  
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
      backgroundColor:'#15141b',
      alignItems:'center'
    },
    input: {
      height: 39,
      width:'100%',
      fontSize: 15,
      borderColor: null,
      backgroundColor: '#34333a',
      borderRadius:28,
      color:'#fff'
    },
    dateIcon:{
      width:20,
      height:18,
      marginRight:20
    },
    datePickerSelectInput:{
      height: 39,
      width:'100%',
      fontSize: 15,
      borderColor: null,
      backgroundColor: '#34333a',
      borderRadius:28,
      color:'#fff',
    }
})

export default Home;
