import React, { useEffect, useState } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Text,
  ScrollView,
  Dimensions,
  View,
  FlatList,
  Image,
  TextInput,
  Alert
} from 'react-native';
import { dimensions, MyColor } from '../utils/constants';
import Header from '../Component/Header'
import Activity_Indicator from '../Component/Activity_Indicator';
import {  useSelector, useDispatch } from 'react-redux';
import {get_attendance_detail,get_assign_task_list,get_task_list,requestPostApi,requestGetApi} from '../WebApis/Service'


const TaskAndTimeDetails = (props) => {
  const token  = useSelector(state => state.user.token)  
  const [data,setData]=useState([]);
  const [taskdata,setTaskData]=useState(null);
  const [taskAttendance,settaskAttendance]=useState(null);
  const [loading,setLoding]=useState(false)
useEffect(()=>{
 console.log('The Date is :--',props.route.params.day) 
    let totaldates=props.route.params.day
    let monthss=totaldates.substring(4,7)
    let dates=totaldates.substring(8,10)
    let year=totaldates.substring(11,15)
 var ids=(d,m,y)=>{
  if(m=='Jan'){
    return d+'-01-'+y
  }else if(m=='Fab'){
    return d+'-02-'+y
  } else if(m=='Mar'){
    return d+'-03-'+y
  }else if(m=='Apr'){
    return d+'-04-'+y
  }else if(m=='May'){
    return d+'-05-'+y
  }else if(m=='Jun'){
    return d+'-06-'+y
  }else if(m=='Jul'){
    return d+'-07-'+y
  }else if(m=='Aug'){
    return d+'-08-'+y
  }else if(m=='Sep'){
    return d+'-09-'+y
  }else if(m=='Oct'){
    return d+'-10-'+y
  }else if(m=='Nov'){
    return d+'-11-'+y
  }else if(m=='Dec'){
    return d+'-12-'+y
  }
}
TaskByDay(ids(dates,monthss,year))
 
},[])

  const TaskByDay= async (dates)=>{
   setLoding(true)
  // http://nileprojects.in/my_production_assistant/get-task-list?date=04-04-2022
  //  const{responseJson,err}  = await requestGetApi(get_attendance_detail+dates+'&worker_id='+props.route.params.workerId,{},'GET',token)
  // const{responseJson,err}  = await requestGetApi(get_assign_task_list+'?from_date='+fromdate+'&to_date='+todate+'&worker_id='+props.route.params.id,{},'GET',token)

   const{responseJson,err}  = await requestGetApi(get_assign_task_list+'?from_date='+dates+'&to_date='+dates+'&worker_id='+props.route.params.workerId,{},'GET',token)
   setLoding(false)
        if(err==null){
             if(responseJson.status){
                 console.log('the API responce for Cart data',responseJson)
                setData(responseJson)
                settaskAttendance(responseJson.attandance_detail)
                setTaskData(responseJson.data) //attandance_detail
              }else{ 
                Alert.alert(responseJson.message) 
              }
          }else{
           Alert.alert(err)
          }
  }

  return (
     <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <Header Lable="Task & Time Details" Nav={() => props.navigation.goBack()} />
<Text style={{color:MyColor.TEXTCOLOR,fontSize:20,fontWeight:'bold',left:15,marginTop:10}}>{props.route.params.day.substring(4,16) }</Text>  
<ScrollView>   

{/* 
{data!=''?
 <View style={{width:'93%',backgroundColor:MyColor.BOXCOLOR,alignSelf:'center',padding:15,borderRadius:10,marginTop:10}}>
<View style={{width:'100%',height:50,flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
<View style={{width:'47%',height:50,backgroundColor:'green',borderRadius:5,padding:5}}>
<Text style={{color:MyColor.TEXTCOLOR,fontSize:11,}}>CHECK-IN</Text>  
<Text style={{color:MyColor.TEXTCOLOR,fontSize:13,fontWeight:'bold',marginTop:5}}>{data.attandance_detail!==null ? data.attandance_detail.check_in : ''}</Text>  
</View>
<View style={{width:'47%',height:50,backgroundColor:'red',borderRadius:5,padding:5}}>
<Text style={{color:MyColor.TEXTCOLOR,fontSize:11,}}>CHECK-OUT</Text>  
<Text style={{color:MyColor.TEXTCOLOR,fontSize:13,fontWeight:'bold',marginTop:5}}>{data.attandance_detail!==null ? data.attandance_detail.check_out :''}</Text>  
</View>

</View>

</View>

:
null
} */}
  {/* <Text style={{color:MyColor.TEXTCOLOR,fontSize:20,fontWeight:'bold',left:15,marginTop:10}}>{data!=''?'All Task':''}</Text>   */}

<View>
      <FlatList
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={{ alignItems: 'center' }}
              data={taskdata}
            //   keyExtractor={item => item.id}
              renderItem={({ item ,index}) => {
                return (
                    <View style={{width:dimensions.SCREEN_WIDTH-30,backgroundColor:MyColor.BOXCOLOR,borderRadius:10,padding:15,marginVertical:10,justifyContent:'center'}}>
                      <View style={{flexDirection:'row',alignItems:'center'}}>
                      <Image source={index%2==0 ? require('../assets/rgb.png'):require('../assets/fixcer.png')}style={{width:35,height:35,alignSelf:'center',zIndex:-999}}resizeMode='cover'></Image>
                      <View>
                      <Text style={{color:'#FFF',left:12,fontSize:12,fontWeight:'bold'}}>{item.project_name+'('+item.total_time+')'}</Text>
                      <Text style={{color:'#FFF',marginLeft:12,fontSize:12,marginVertical:6}}>Job Type : {item.task_type}</Text>
                      </View> 
                       </View>
<View style={{width:'100%',height:2,backgroundColor:MyColor.SUBBOXCOLOR,marginVertical:10}} />
<Text style={{color:'rgba(256,256,256,0.7)',fontSize:12}}>Task Notes: {item.note}</Text>

<View style={{width:'100%',height:18,flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginTop:10}}>
<Text style={{fontSize:14,fontWeight:'bold',color:'white',lineHeight:15}}>Task Status: </Text>
<Text style={{fontSize:13,fontWeight:'bold',color:'white',lineHeight:15}}>{item.task_status}</Text>
</View>

<View style={{width:'100%',height:18,flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginTop:6}}>
<Text style={{fontSize:14,fontWeight:'bold',color:'white',lineHeight:15}}>Check In Time: </Text>
<Text style={{fontSize:13,fontWeight:'bold',color:'white',lineHeight:15}}>{item.check_in}</Text>
</View>

<View style={{width:'100%',height:18,flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginTop:6}}>
<Text style={{fontSize:14,fontWeight:'bold',color:'white',lineHeight:15}}>Check Out Time: </Text>
<Text style={{fontSize:13,fontWeight:'bold',color:'white',lineHeight:15}}>{item.check_out}</Text>
</View>
</View> 
                )
              }}
            />
 </View>

<View style={{width:100,height:100}} />
</ScrollView>  
       { loading ?
        <Activity_Indicator />
        :
        null
        }
      </SafeAreaView>
   
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
      backgroundColor:MyColor.BGCOLOR
    },
    input: {
        paddingLeft: 15,
        height: 45,
        width:'100%',
        fontSize: 17,
        borderColor: null,
        backgroundColor: '#34333a',
        borderRadius:28,
        color:'#fff'
    
      },
      dateIcon:{
        width:20,
        height:18,
        marginRight:20
      },
      datePickerSelectInput:{
        height: 45,
        width:'100%',
        fontSize: 15,
        borderColor: null,
        backgroundColor: '#34333a',
        borderRadius:28,
        color:'#fff',
      }
})

export default TaskAndTimeDetails;
