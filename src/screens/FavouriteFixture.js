import React, { useEffect, useState } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Text,
  ScrollView,
  Dimensions,
  View,
  FlatList,
  Image,
  Alert
} from 'react-native';
import {TouchableOpacity } from 'react-native-gesture-handler';
import { dimensions, MyColor } from '../utils/constants';
import Header from '../Component/Header'
var width = Dimensions.get('window').width
var height = Dimensions.get('window').height
import * as types  from '../redux/types';
import Activity_Indicator from '../Component/Activity_Indicator';
import {  useSelector, useDispatch } from 'react-redux';
import Toast from 'react-native-simple-toast';
import {favorite_fixcer_list,save_favorite_fixcer,remove_favorite_fixcer,add_task,get_task_list,get_attendance_detail,requestPostApi,requestGetApi} from '../WebApis/Service'
import MyAlert from '../Component/MyAlert';    
 

const FavouriteFixture = (props) => {
  const token  = useSelector(state => state.user.token)  
  const [loading,setLoading]=useState(false)
  const [favFixcer,setfavFixcer]=useState(null)
  const [removeid,setremoveid]=useState('')
  const [My_Alert, setMy_Alert] = useState(false)
  const [alert_sms, setalert_sms] = useState('')
 useEffect(()=>{
  getFavList();
 },[]) 
  const getFavList= async ()=>{
    setLoading(true)
    const{responseJson,err}  = await requestGetApi(favorite_fixcer_list,{},'GET',token)
    setLoading(false)
    if(err==null){
       if(responseJson.status){
        setfavFixcer(responseJson.data)
        setMy_Alert(false)
        }else{ 
          setfavFixcer(null)
          setMy_Alert(false)
          Toast.show(responseJson.message);
        }
    }else{
      setMy_Alert(false)
      Alert.alert(err)
    }
  }
  const removeFaveFixcers=async ()=>{
    setLoading(true)
    const{responseJson,err}  = await requestGetApi(remove_favorite_fixcer+removeid,{},'GET',token)
    setLoading(false)
    if(err==null){
       if(responseJson.status){
        getFavList()
        setMy_Alert(false)
        Alert.alert('Removed Successfully')
        }else{ 
          setMy_Alert(false)
          Toast.show(responseJson.message);
        }
    }else{
      setMy_Alert(false)
      Alert.alert(err)
    }
  }

  const removeFaveFixcerClick=async (id)=>{
    setremoveid(id)
    setalert_sms('Do  you want to remove the fixture from your favorites')
    setMy_Alert(true)
  }

  return (
     <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <Header Lable="Favorites Fixtures" Nav={() => props.navigation.goBack()} />
<ScrollView>
<View>
<FlatList
              showsHorizontalScrollIndicator={false}
              // numColumns={2}
              contentContainerStyle={{ alignItems: 'center' }}
              data={favFixcer}
              keyExtractor={item => item.id}
              renderItem={({ item }) => {
                return (
              <View style={{ width: width*93/100, height: 60, borderRadius: 20, marginVertical: 7,backgroundColor:MyColor.BOXCOLOR}}>
                  <View style={{flexDirection:'row',justifyContent:'space-between',padding:10,alignItems:'center',}}>
  {/* ************************************************* */}
  <View>
  <Text style={{color:MyColor.TEXTCOLOR,fontSize:11,fontWeight:'bold',left:10,top:3}}>{item.title}</Text>
  <Text style={{color:MyColor.TEXTCOLOR,fontSize:15,fontWeight:'bold',left:10,top:3}}>{item.fixcer}</Text>
</View>
  <TouchableOpacity style={{paddingHorizontal:20, height:25,backgroundColor:MyColor.SUBBOXCOLOR,flexDirection:'row',borderRadius:10,justifyContent:'center',alignItems:'center',marginTop:6}} onPress={()=>{removeFaveFixcerClick(item.id)}}>
<Image source={require('../assets/bin.png')} style={{ width: 11, height: 14,right:5}}></Image>
<Text style={{color:MyColor.TEXT2COLOR,fontSize:13}}>Remove</Text>
</TouchableOpacity>
       </View>
              {/* <Image source={item.img} style={{ width: '100%', height: '100%', borderRadius: 10 }}></Image> */}
              </View> 
                )
              }}
            />
  </View>     
{favFixcer==null?
<View style={{justifyContent:'center',height:height,width:width}}>
<Text style={{textAlign:'center',fontSize:22,fontWeight:'bold',color:MyColor.TEXTCOLOR}}></Text>
</View>
:
null
}
<View style={{width:100,height:100}} />
</ScrollView>  

{My_Alert ? <MyAlert sms={alert_sms}  okPress={()=>{ removeFaveFixcers() }} canclePress={()=>{setMy_Alert(false)}}/> : null }

      { loading ?
        <Activity_Indicator />
        :
        null
        }
      </SafeAreaView>
   
  );
};
let styles = StyleSheet.create({
    container:{
      flex: 1,
      backgroundColor:MyColor.BGCOLOR
    },
 
})

export default FavouriteFixture;
