import React, { useEffect, useState } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Text,
  ScrollView,
  Dimensions,
  View,
  FlatList,
  Image,
  Alert,
  ActivityIndicator,
  Linking,
} from 'react-native';
import { dimensions, MyColor,urls } from '../utils/constants';
import Header from '../Component/Header'
import Activity_Indicator from '../Component/Activity_Indicator';
import Toast from 'react-native-simple-toast';
import {get_subscription_plan,buy_subscription,paymentUrl,purchage_subscription,requestPostApi,requestGetApi} from '../WebApis/Service'
import {  useSelector, useDispatch } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import * as types  from '../redux/types';

const Subscription = (props) => {
  const [loading,setLoading]=useState(false)
  const [data,setData]=useState([])
  const token  = useSelector(state => state.user.token)
  const dispatch =  useDispatch();

useEffect(()=>{
  getPlan()
},[]);

const resetStacks=(page)=>{
  props.navigation.reset({
    index: 0,
    routes: [{name: page}],
    // routes:[NavigationActions.navigate({ routeName: page })],
    //  params: {items:data},
  });
 }

const randomColor=()=>{
 const my_color= Math.floor(Math.random()*16777215).toString(16)
return '#'+my_color
}
const getPlan=async()=>{
  setLoading(true)
  const{responseJson,err}  = await requestGetApi(get_subscription_plan,{},'GET','')
  setLoading(false)
       if(err==null){
            if(responseJson.status){
              setData(responseJson.data)
             }else{ 
               Toast.show(responseJson.message);
             }
         }else{
          Toast.show(err);
         }

                }

const getUrls=async(id)=>{
  AvoidPayment(id)
//   let formdata = new FormData();
//   formdata.append('plan_id',id);
// setLoading(true)
// const{responseJson,err}  = await requestPostApi(buy_subscription,formdata,'POST',token) 
// setLoading(false)
// if(err==null){
//   if(responseJson.status){
//    console.log('the urlsss==>>',responseJson)
//    props.navigation.navigate('MyWebView',{url:responseJson.usr,planid:id})
//   // Linking.openURL(responseJson.usr)
//   }else{
//     Toast.show(responseJson.message);
//        }
// }else{
// Toast.show(err);
// }
}

const AvoidPayment=async(id)=>{
  
setLoading(true)

const{responseJson,err}  = await requestGetApi(purchage_subscription+'?plan_id='+id,{},'GET',token) 
setLoading(false)
if(err==null){
  if(responseJson.status){
   console.log('the urlsss==>>',responseJson)
                      AsyncStorage.setItem("user",JSON.stringify(responseJson.data));
                    // AsyncStorage.setItem("token",JSON.stringify(responseJson.data.token));
                      dispatch({
                      type:types.SAVE_USER_RESULTS,
                      user:responseJson.data
                      })
              // props.navigation.navigate('Home');
              resetStacks('EmployeeList')
  }else{
    Toast.show(responseJson.message);
       }
}else{
Toast.show(err);
}
}

 
  return (
     <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <Header Lable="Our Subscription Plans" Nav={() => props.navigation.goBack()} />
<ScrollView>
<View>
            <FlatList
              showsHorizontalScrollIndicator={false}
              // numColumns={2}
              contentContainerStyle={{ alignItems: 'center' }}
              data={data}
              keyExtractor={item => item.id}
              renderItem={({ item }) => {
                return (
    <View style={{width:dimensions.SCREEN_WIDTH-20,backgroundColor:MyColor.BOXCOLOR,marginVertical:6,borderRadius:20,alignItems:'center'}}>
      <Text style={{color:MyColor.TEXTCOLOR,fontSize:17,marginTop:10}}>${item.price} / Month</Text>
    <View style={{width:60,height:1,backgroundColor:'#614393',margin:10}}  />
     <Text style={{color:MyColor.TEXTCOLOR,fontSize:17}}>{item.total_worker} Employee</Text>
    <View style={{width:'70%',marginBottom:55,marginTop:10}}>  
        <ScrollView>
    <Text style={{color:'rgba(256,256,256,0.6)',fontSize:13,marginTop:3,textAlign:'center'}}>{item.description}</Text>
   </ScrollView>
   </View>
            <View style={{width:'100%',height:35,position:'absolute',bottom:0,borderTopColor:MyColor.SUBBOXCOLOR,borderTopWidth:1.5,justifyContent:'center'}}>
            <Text style={{color:'#614393',fontSize:17,alignSelf:'center'}} onPress={()=>{getUrls(item.id)}}>Subscribe</Text>
            </View>    
                    </View>
                )
              }}
            />
  
  </View>     


  <View style={{width:100,height:150}} />

</ScrollView>  
         { loading ?
        <Activity_Indicator />
        :
        null
        }
      </SafeAreaView>
   
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
      backgroundColor:MyColor.BGCOLOR
    },
 
})

export default Subscription;
