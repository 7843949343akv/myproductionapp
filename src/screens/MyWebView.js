import React, { useEffect, useState } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Text,
  ScrollView,
  Image,
  View,
  Linking,
} from 'react-native';
import {TouchableOpacity } from 'react-native-gesture-handler';
import {  useSelector, useDispatch } from 'react-redux';
import { dimensions, MyColor,urls } from '../utils/constants';
import * as types  from '../redux/types';
import AsyncStorage from '@react-native-community/async-storage';
import {getAsyncStorage, setLoading,saveUserResult} from '../redux/actions/user_action';
import Activity_Indicator from '../Component/Activity_Indicator';
import Toast from 'react-native-simple-toast';
import {get_subscription_status,success_token,baseUrl,paymentUrl,requestPostApi,requestGetApi} from '../WebApis/Service'
import { WebView } from 'react-native-webview';
import Header from '../Component/Header'

const MyWebView = (props) => {
  const tokens  = useSelector(state => state.user.token)
  const [loading,setLoading]=useState(false)
  const userdata  = useSelector(state => state.user.user_details)
  const dispatch =  useDispatch();
 useEffect(()=>{

// console.log('the id is',props.route.params.planid)

 },[])
 const resetStacks=(page)=>{
  props.navigation.reset({
    index: 0,
    routes: [{name: page}],
    // routes:[NavigationActions.navigate({ routeName: page })],
    //  params: {items:data},
  });
 }

const MyBackButton=  async()=>{
  setLoading(true)
  const{responseJson,err}  = await requestGetApi(get_subscription_status,{},'GET',tokens)
  setLoading(false)
  console.log('the responce is==>>',responseJson)
       if(err==null){
            if(responseJson.status){
            
             // Toast.show(responseJson.message);
             // props.navigation.navigate('Home');
             }else{ 
              Toast.show(responseJson.message);
             // props.navigation.navigate('Home');
             }
           }else{
           Toast.show(err);
         }
}

const _onNavigationStateChange = (webViewState) => {

  console.log('The state change urls ==>>',webViewState)
   if(webViewState.url.includes(baseUrl+'success.php?success=false')){
              props.navigation.goBack()
            }
    else  if (webViewState.url.includes(baseUrl+'success.php?success=true')) {
      let url = webViewState.url
      let regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match
      while ((match = regex.exec(url))) {
        params[match[1]] = match[2]
      }
        const { token } = params
        const { ba_token } = params
        const { plan_id } = params
        console.log('matchhedd===>>>',url)
        console.log('matchhedd===>>>',token)
        console.log('matchhedd===>>>',ba_token)
        console.log('matchhedd===>>>',plan_id)
       fetchSuccessDetails(token,plan_id)
     }
  }

  const fetchSuccessDetails=async(t_token,p_id)=>{
    setLoading(true)
    const{responseJson,err}  = await requestGetApi(success_token+t_token+'&plan_id='+props.route.params.planid,{},'GET',tokens)
    // const{responseJson,err}  = await requestGetApi(success_token+t_token,{},'GET',token)
    setLoading(false)
         if(err==null){
              if(responseJson.status){
                console.log('the resof success',responseJson)
              //  MyBackButton()
                    AsyncStorage.setItem("user",JSON.stringify(responseJson.data));
                    // AsyncStorage.setItem("token",JSON.stringify(responseJson.data.token));
                      dispatch({
                      type:types.SAVE_USER_RESULTS,
                      user:responseJson.data
                      })
              // props.navigation.navigate('Home');
              resetStacks('EmployeeList')
             //  props.navigation.navigate('EmployeeList');
              }else{ 
                resetStacks('EmployeeList')
                Toast.show(responseJson.message);
               // props.navigation.navigate('EmployeeList');
              }
           }else{
             Toast.show(err);
             }
  }

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.container}>
      <Header Lable="Payment" Nav={() => props.navigation.goBack()} />

                  <WebView
                        style={{ height: '100%', width: '100%' ,marginTop: 20}}
                        // onLoadStart={() => this.setState({loading:true})}
                        // onLoadEnd={()=>this.setState({loading:false})}
                        source={{ uri: props.route.params.url}}
                        onNavigationStateChange={_onNavigationStateChange}
                        javaScriptEnabled={true}
                        domStorageEnabled={true}
                        onShouldStartLoadWithRequest={() => true}
                        startInLoadingState={false}
                    />

       { loading ?
        <Activity_Indicator />
        :
        null
        }

      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
   backgroundColor:MyColor.BGCOLOR,
    },
    button:{
      backgroundColor:'blue',
      padding:10,
      borderRadius:15,
      width:dimensions.SCREEN_WIDTH * 0.8,
      alignItems:'center'
  }
})

export default MyWebView;
