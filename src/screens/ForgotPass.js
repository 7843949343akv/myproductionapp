import React from 'react'
import { Alert, Text, Image, ScrollView,BackHandler, ToastAndroid, Platform, TextInput, View, StyleSheet, ActivityIndicator, StatusBar, Dimensions, ImageBackground } from 'react-native';
import {TouchableOpacity } from 'react-native-gesture-handler';
import { dimensions } from '../utils/constants';
import Modal from 'react-native-modal';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useState } from 'react';
 const ForgotPass = (props) => {
const[email,setEmail]=useState('')

const MyLogin=()=>{
    props.navigation.navigate('Otp')
} 


  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.container}>
     
      <ImageBackground source={require('../assets/bgimg.jpeg')}style={{width:'100%',height:'81%',position:'absolute', top:0}}resizeMode='cover'>
     
      <View style={{width:'100%',height:55,backgroundColor:'#000',marginTop: Platform.OS=='android'? 0 : 40}}>
         <TouchableOpacity style={{width:30,height:25,marginTop:15,marginLeft:15}} onPress={()=> props.navigation.navigate('Login')} hitSlop={{ top: 30, bottom: 30, left: 30, right: 30 }}>
         <Image source={require('../assets/backarrow.png')} style={{ height: '100%', width: '100%' }}></Image>
         </TouchableOpacity>
       </View>
</ImageBackground>




     <Modal isVisible={true}
    //  swipeDirection="down"
     onSwipeComplete={(e) => {
     }}
          scrollTo={() => {}}
          scrollOffset={1}
          propagateSwipe={true}
     coverScreen={false}
     backdropColor='transparent'
     style={{ justifyContent: 'flex-end', margin: 0 ,backgroundColor:'transparent'}}
     >
        <View style={{height:'45%',backgroundColor:'#d5f7f5',borderTopLeftRadius:50,borderTopRightRadius:50,borderColor:'red',borderWidth:1}}>
         <ScrollView>
        <View style={{alignSelf:'center',marginTop:50}}>
      <Text style={{color:'#000',fontSize:15 ,top:-5}}>Email</Text>
    <View style={{flexDirection:'row',borderColor:'#1b2c5b',borderWidth:1,width:'100%',height:55,borderRadius:15}}>
    
      <TextInput
            value={email}
            onChangeText={(e) => setEmail(e)}
            placeholder={'Email'}
            placeholderTextColor="#bbbbbb"
           // keyboardType="number-pad"
            autoCapitalize = 'none'
            style={styles.input}
          />


     </View>

{/* ***********************************Input 2********************************************* */}

       <TouchableOpacity onPress={()=> MyLogin()}
        style={styles.button}>  
        <Text style={{color:'white',fontWeight:'bold',fontSize:20}}>Send OTP</Text>
       </TouchableOpacity>
   

        </View>
        <View style={{width:100,height:100}} />
        </ScrollView>
        </View>
      </Modal>

      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
      backgroundColor:'#e9e9f0',
      justifyContent:'center',
      alignItems:'center'
    },
    button:{
      backgroundColor:'blue',
      padding:10,
      borderRadius:15,
      width:dimensions.SCREEN_WIDTH * 0.8,
      alignItems:'center',
      height:52,
      justifyContent:'center',
      marginTop:30
  },
  input: {
    paddingLeft: 15,
    height: 52,
    width:'80%',
    fontSize: 22,
    borderColor: null,
    backgroundColor: 'white',
    borderRadius:15

  },
   

})

export default ForgotPass;
