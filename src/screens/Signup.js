import React from 'react'
import { Alert, Text, Image, ScrollView,BackHandler, ToastAndroid, Platform, TextInput, View, StyleSheet, ActivityIndicator, StatusBar, Dimensions, ImageBackground } from 'react-native';

import {TouchableOpacity } from 'react-native-gesture-handler';
import DrawerNavigator from '../navigators/DrawerNavigator';
import {  useSelector, useDispatch } from 'react-redux';
import * as types  from '../redux/types';
import Modal from 'react-native-modal';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useState } from 'react';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {saveUserResult,saveUserToken} from '../redux/actions/user_action';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-simple-toast';
import Activity_Indicator from '../Component/Activity_Indicator';
import { dimensions, MyColor,urls } from '../utils/constants';
import {login,forgot_password,requestPostApi} from '../WebApis/Service'

const Signup = (props) => {

 const dispatch =  useDispatch();
const[email,setEmail]=useState('')
const[email2,setEmail2]=useState('')
const[pass,setPass]=useState('')
const[passView,setPassView]=useState(true)
const[loading,setLoading]=useState(false)
const [modlevisual, setmodlevisual] = useState(false);

const validation=()=>{
  if(email == '' || email.trim().length == 0){
    Toast.show('Enter Email Id')
  }
  else if(!(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(
    email))){
      Toast.show('Enter Valid Email Id')
  }else if(pass == '' || pass.trim().length == 0){
    Toast.show('Please enter Password')
  }else{
    LoginClick();
  }

}

const LoginClick= async()=>{
  setLoading(true)
  let formdata = new FormData();
  formdata.append("email",email);
  formdata.append("password",pass);
  const{responseJson,err}  = await requestPostApi(login,formdata,'POST','') 
  setLoading(false)
  if(err==null){
      if(responseJson.status){
        console.log('the user data is ==>>>',responseJson.data)
        dispatch(saveUserToken(responseJson.token))
        AsyncStorage.setItem('user',JSON.stringify(responseJson.data));
        AsyncStorage.setItem('token',JSON.stringify(responseJson.token));
       dispatch(saveUserResult(responseJson.data));
       
       props.navigation.navigate('Home');
      }else{
        Toast.show(responseJson.message);
      }
      }else{
    Toast.show(err);
       }

}


const forgotPasswordClick= async()=>{
  if(email2 == '' || email2.trim().length == 0){
    Toast.show('Enter Email Id')
  }
  else if(!(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(
    email2))){
      Toast.show('Enter Valid Email Id')
  }else{
     setLoading(true)
  let formdata = new FormData();
  formdata.append("email",email2);
  const{responseJson,err}  = await requestPostApi(forgot_password,formdata,'POST','') 
  setLoading(false)
  if(err==null){
      if(responseJson.status){
      Toast.show(responseJson.message);
      setmodlevisual(false)
      }else{
        Toast.show(responseJson.message);
      }
  }else{
    Toast.show(err);
  }
  }
 
}

  return (
    <SafeAreaView style={styles.container}>
           <StatusBar barStyle="dark-content" backgroundColor="#000" />
<View style={{flex:1,backgroundColor:'#15141b'}}>
          <View style={{ alignSelf: 'center',marginTop:40 }}>
           <Image source={require('../assets/newlogo.png')} style={{ width: 200, height: 70, alignSelf: 'center', zIndex: -999 }} resizeMode='cover'></Image>
          </View>
<View style={{width:'95%',height:dimensions.SCREEN_HEIGHT*65/100,marginHorizontal:10,paddingHorizontal:15,borderTopLeftRadius:30,borderTopRightRadius:30,backgroundColor:'#201f28',position:'absolute',bottom:-30,alignSelf:'center'}}>
<KeyboardAwareScrollView showsVerticalScrollIndicator={false}>

<Text style={{color:'#FFF',fontSize:25,marginTop:25,}}>Sign In</Text>
<Text style={{color:'#FFF',fontSize:13,marginTop:5,}}>Enter Login Information</Text>

 {/* ********************************************************   */}
<Text style={{color:'#fff',fontSize:15 ,marginTop:15}}>Email ID</Text>
<View style={{width:'100%',height:47,borderRadius:28,marginTop:10}}>

  <TextInput
        value={email}
        onChangeText={(e) => setEmail(e)}
        placeholder={''}
        placeholderTextColor="#bbbbbb"
       // keyboardType="number-pad"
        autoCapitalize = 'none'
        style={styles.input}
      />
 </View>

 {/* ********************************************************   */}
<Text style={{color:'#fff',fontSize:15 ,marginTop:15}}>Password</Text>
<View style={{width:'100%',height:47,borderRadius:28,marginTop:10}}>

  <TextInput
        value={pass}
        onChangeText={(e) => setPass(e)}
        placeholder={''}
        placeholderTextColor="#bbbbbb"
       // keyboardType="number-pad"
       secureTextEntry={passView ? true : false}
        autoCapitalize = 'none'
        style={styles.input}
      />
     <View style={{width:25,height:22,position:'absolute',right:20,top:12}}>
        <TouchableOpacity onPress={()=>setPassView(!passView)}>
         <Image source={passView ? require('../assets/eye.png') : require('../assets/eyes.png')}style={{width:'100%',height:'100%',alignSelf:'center',zIndex:-999}}resizeMode='cover'></Image>
        </TouchableOpacity>
      </View>

 </View>
 

 {/* ********************************************************   */}

 <TouchableOpacity style={{width:'75%',height:55,backgroundColor:'#0000ff',alignSelf:'center',borderRadius:30,flexDirection:'row',justifyContent:'center',marginTop:20}}
  onPress={()=>validation()}>
<Text style={{color:'#FFF',fontSize:16,alignSelf:'center'}}>Sign In</Text>
</TouchableOpacity>

<Text style={{color:'#FFF',fontSize:16,alignSelf:'center',marginTop:15}} onPress={()=>{props.navigation.navigate('Login')}}>Don't have an account? Sign Up</Text>

<Text style={{color:'#FFF',fontSize:16,alignSelf:'center',marginTop:15}} onPress={()=>{ setmodlevisual(true)}}>Forgot Password</Text>
<View style={{width:100,height:100}} />
       </KeyboardAwareScrollView>

</View>

</View>



<Modal
        isVisible={modlevisual}
         swipeDirection="down"
        onSwipeComplete={(e) => {
          setmodlevisual(false)
        }}
          scrollTo={() => {}}
          scrollOffset={1}
          propagateSwipe={true}
        hideModalContentWhileAnimating={true}
        coverScreen={false}
       // backdropColor='transparent'
        style={{ justifyContent: 'flex-end', margin: 0, backgroundColor: 'transparent' }}
      >
        <View style={{ height: 240,width:'90%',backgroundColor: '#15141b', borderRadius: 20, borderTopRightRadius: 20,alignSelf:'center',bottom:'25%',padding:20}}>
       

        <Text style={{color:'#fff',fontSize:15 ,marginTop:15}}>Enter Registered Email ID</Text>
        <View style={{width:'100%',height:47,borderRadius:28,marginTop:10,alignSelf:'center'}}>

          <TextInput
                value={email2}
                onChangeText={(e) => setEmail2(e)}
                placeholder={''}
                placeholderTextColor="#bbbbbb"
              // keyboardType="number-pad"
                autoCapitalize = 'none'
                style={styles.input}
              />
        </View>

        <TouchableOpacity style={{width:'75%',height:55,backgroundColor:'#0000ff',alignSelf:'center',borderRadius:30,flexDirection:'row',justifyContent:'center',marginTop:20}}
          onPress={()=>forgotPasswordClick()}>
        <Text style={{color:'#FFF',fontSize:16,alignSelf:'center'}}>SUBMIT</Text>
        </TouchableOpacity>
    
     </View>
       { loading ?
        <Activity_Indicator />
        :
        null
        }
      </Modal>


      { loading ?
        <Activity_Indicator />
        :
        null
        }
      </SafeAreaView>
  );
};


let styles = StyleSheet.create({
  container:{
    flex: 1,
    backgroundColor:'#15141b',
  },

input: {
  paddingLeft: 15,
  paddingRight:50,
  height: 45,
  width:'100%',
  fontSize: 17,
  borderColor: null,
  backgroundColor: '#34333a',
  borderRadius:28,
  color:'#fff'

},

})

export default Signup;
