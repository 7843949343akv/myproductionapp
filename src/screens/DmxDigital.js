import React, { useEffect, useState } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Text,
  ScrollView,
  Dimensions,
  View,
  TextInput,
  Image,
  Alert,
} from 'react-native';
import {TouchableOpacity } from 'react-native-gesture-handler';
import { dimensions,MyColor } from '../utils/constants';
import Header from '../Component/Header'
// import ToggleSwitch from 'toggle-switch-react-native';
import Activity_Indicator from '../Component/Activity_Indicator';
import {  useSelector, useDispatch } from 'react-redux';
import Toast from 'react-native-simple-toast';
import {save_favorite_fixcer,requestPostApi,requestGetApi} from '../WebApis/Service'
import Modal from 'react-native-modal';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'


const DmxDigital = (props) => {
  const token  = useSelector(state => state.user.token)  
const [loading,setLoading]=useState(false)
const [displaytext,setDisplayText]=useState('');
const [title, settitle] = useState('');
const [modlevisual, setmodlevisual] = useState(false);
const [like,setLike]=useState(false);
const [myarray,setMyarray]=useState([{row1:[1,2,3]},{row2:[4,5,6]},{row3:[7,8,9]}])
const [linearray,setLinearray]=useState([1,2,3,4,5,6,7,8,9])
const [line1,setLine1]=useState(false);
const [line2,setLine2]=useState(false);
const [line3,setLine3]=useState(false);
const [line4,setLine4]=useState(false);
const [line5,setLine5]=useState(false);
const [line6,setLine6]=useState(false);
const [line7,setLine7]=useState(false);
const [line8,setLine8]=useState(false);
const [line9,setLine9]=useState(false);
const [line10,setLine10]=useState(false);
const submitTask= async ()=>{
  if(token){
    if(displaytext!='' && title !=''){

    
    let formdata = new FormData();
    formdata.append('fixcer',displaytext);
    formdata.append('title',title);
               setLoading(true)
const{responseJson,err}  = await requestPostApi(save_favorite_fixcer,formdata,'POST',token)  
setLoading(false)
if(err==null){
    if(responseJson.status){
      setLike(true)
      Toast.show('Saved Successfully')
      setmodlevisual(false)
    }else{
        Alert.alert(responseJson.msg)
      } 
}else{
  Alert.alert(err)
}
    }else{
      Toast.show('All Filed are mandatory')
    }
  }else{
    props.navigation.navigate('Login')
   // setLike(!like)
  }
}

const powerCountFun=(n)=>{
var no=n;
var powercount=1;
for(let i=1;i<10;i++){
  var powerOftwo=1;
  for(let j=1;j<=i;j++){
    powerOftwo=powerOftwo*2
  }
  if(powerOftwo<=n){
    powercount++;
  }
}
return powercount;
}

const displayFun=(v)=>{
  var n=v;
  var arr=[];
  for(let i=1;i<=10;i++){
  var btn=powerCountFun(n);
  if(n<=0){

  }else{
    arr.push(btn)
  }

  if(btn==1){
    n=n-1;
  }else if(btn==2){
    n=n-2;
  }else if(btn==3){
    n=n-4;
  }else if(btn==4){
    n=n-8;
  }else if(btn==5){
    n=n-16;
  }else if(btn==6){
    n=n-32;
  }else if(btn==7){
    n=n-64;
  }else if(btn==8){
    n=n-128;
  }else if(btn==9){
    n=n-256;
  }else if(btn==10){
    n=n-512;
  }
  }
  setLine1(false)
  setLine2(false)
  setLine3(false)
  setLine4(false)
  setLine5(false)
  setLine6(false)
  setLine7(false)
  setLine8(false)
  setLine9(false)
  setLine10(false)
  
  for(let k=0;k<=arr.length;k++){
    if(arr[k]=='1'){
     setLine1(true)
    }else if(arr[k]=='2'){
      setLine2(true)
    }else if(arr[k]=='3'){
      setLine3(true)
    }else if(arr[k]=='4'){
      setLine4(true)
    }else if(arr[k]=='5'){
      setLine5(true)
    }else if(arr[k]=='6'){
      setLine6(true)
    }else if(arr[k]=='7'){
      setLine7(true)
    }else if(arr[k]=='8'){
      setLine8(true)
    }else if(arr[k]=='9'){
      setLine9(true)
    }else if(arr[k]=='10'){
      setLine10(true)
    }
  }
  //console.log('the On button List is===>>>',arr)
}

const MyButton=(text)=>{
  const mycalculator=()=>{
    const D_text=displaytext
    // if(D_text+text>1023){
    if(D_text+text>511){
    }else{
       setDisplayText(D_text+text);
       displayFun(D_text+text)
    }
   
  }
  return(
    <TouchableOpacity style={{width:dimensions.SCREEN_WIDTH*24.5/100,height:dimensions.SCREEN_HEIGHT*4/100,backgroundColor:MyColor.BOXCOLOR,justifyContent:'center',
    marginVertical:7,marginHorizontal:5,borderRadius:8,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 1,
    shadowOpacity: 1,
    justifyContent: 'center',
    elevation: 5,
    alignSelf: 'center'}}
    onPress={()=>mycalculator()}
    >
    <Text style={{alignSelf:'center',color:MyColor.TEXTCOLOR,fontWeight:'bold',fontSize:20}}>{text}</Text>
    </TouchableOpacity>
  )
}

const ClearPressed=()=>{
let mytext=displaytext
let removedtext=mytext.substring(0,mytext.length-1)
setDisplayText(removedtext)
displayFun(removedtext)
}

const plusSixteenPressed=()=>{
  let mytext=displaytext
  let addtext= +mytext+16 
  // if(addtext>1023){
    if(addtext>511){
  }else{
    setDisplayText(addtext.toString())
    displayFun(addtext)
  }
  
}
const MyLine1=(num)=>{
  return(
    <View style={{top:4}}>
  <Text style={{color:'gray',fontSize:8,top:5}}>ON</Text>
  {/* <Text style={{color:'gray',fontSize:10,top:-1,left:3}}>{num}</Text>
  <View style={{width:6,height:6,backgroundColor:'gray',borderRadius:3,left:3}}></View> */}
  </View>
  )
}
const MyLine=(num)=>{
  return(
    <View style={{}}>
  {/* <Text style={{color:'gray',fontSize:8,top:-7}}>OFF</Text> */}
  <Text style={{color:'gray',fontSize:12,top:-1,left:3,fontWeight:'bold'}}>{num}</Text>
  {/* <View style={{width:6,height:6,backgroundColor:'gray',borderRadius:3,left:3}}></View> */}
  </View>
  )
}

const VirticalLine=(click,conditation,value)=>{
  
  const mycalculator=()=>{
    click()
    var D_text=displaytext==''?'0':displaytext
    if(conditation){
      D_text=parseInt(D_text) - value
    }else{
      D_text=parseInt(D_text) + value
    }
    if(D_text<0){
      setDisplayText('0');
    // }else if(D_text>1023){
    }else if(D_text>511){
      setDisplayText('Invalid Number');
    }else{
      setDisplayText(D_text.toString());
    }
   }
  return(
<TouchableOpacity onPress={()=>{mycalculator()}} style={{justifyContent:'center',alignItems:'center',right:8,}}>
<View style={{width:20,height:35,borderRadius:7,backgroundColor:conditation?'blue':'transparent',top:35,right:0,borderWidth:4.5,borderColor:conditation?'white':'transparent',zIndex:999}}/>
<View style={{width:12,height:dimensions.SCREEN_HEIGHT*11/100,backgroundColor:conditation?'blue':'gray',borderRadius:5,zIndex:-999}} />
<View style={{width:20,height:35,borderRadius:7,backgroundColor:conditation?'transparent':'blue',top:-35,right:0,borderWidth:4.5,borderColor:conditation?'transparent':'white'}}/>
</TouchableOpacity>
  )
}

  return (
     <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <Header Lable="DMX Analogue Dip switch calculator" Nav={() => props.navigation.goBack()} />
<ScrollView>
      <View style={{width:'90%',height:110,borderRadius:8,marginTop:25,backgroundColor:MyColor.BOXCOLOR,alignSelf:'center'}}>
      <Text style={{position:'absolute',right:15,bottom:30,color:'#FFF',fontSize:45,fontWeight:'500'}}>{displaytext}</Text>
      </View>
<View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',width:'91%',alignSelf:'center',marginTop:10}}>
  <Text></Text>
{/* <View style={{borderColor:MyColor.SUBBOXCOLOR,borderWidth:2,padding:1,borderRadius:100,width:togalIson?60:'auto'}}>
<ToggleSwitch
  isOn={!togalIson}
  onColor="blue"
  offColor="transparent"
  label=""
  labelStyle={{ color: "black", fontWeight: "900",borderColor:MyColor.SUBBOXCOLOR,borderWidth:2 }}
  size="small"
  onToggle={isOn => {
      setTogalIson(!togalIson)
      }}
/>          
{togalIson ? 
  <View style={{position:'absolute',right:0,justifyContent:'center',top:5}}>
<Text style={{fontSize:9,color:'gray',fontWeight:'bold',right:5}}>OFF</Text>
  </View>
  :
  null
  }
</View> */}
<TouchableOpacity style={{height:30,borderColor:MyColor.SUBBOXCOLOR,borderWidth:2,borderRadius:9,flexDirection:'row',
alignItems:'center',paddingHorizontal:10,backgroundColor: like ? 'red' :'transparent'}}
onPress={()=>{
  setmodlevisual(true)
  // submitTask()
  }}
>
{!like ?
<Image source={require('../assets/ic-favourite.png')}style={{width:14,height:12,alignSelf:'center'}}resizeMode='cover'></Image>
:
<Image source={require('../assets/ic-favourite.png')}style={{width:14,height:12,alignSelf:'center'}}resizeMode='cover'></Image>
}
<View>
  <Text style={{color:'white',fontSize:13,marginLeft:8}}>{like? 'Saved as favourite':'Save as favourite'}</Text>
</View>
</TouchableOpacity>
</View>
{/* ******************************************** */}
{/* <View style={{flexDirection:'row',justifyContent:'space-between'}}>
<View style={{}}>
  <Text style={{color:'gray',fontSize:10,top:-1}}></Text>
  <View style={{width:5,height:1,backgroundColor:'transparent',borderRadius:3}} />
</View>

<View style={{}}>
  <Text style={{color:'gray',fontSize:10,top:-1}}></Text>
  <View style={{width:5,height:1,backgroundColor:'transparent',borderRadius:3}} />
</View>
</View> */}



<View style={{flexDirection:'row',justifyContent:'space-between',backgroundColor:MyColor.HEADERCOLOR,marginHorizontal:20,borderRadius:10,borderTopRightRadius:10,marginTop:10}}>
<View style={{}}>
  <Text style={{color:'gray',fontSize:10,top:-1}}></Text>
</View>
{VirticalLine(()=>{setLine1(!line1)},line1,1)}
{VirticalLine(()=>{setLine2(!line2)},line2,2)}
{VirticalLine(()=>{setLine3(!line3)},line3,4)}
{VirticalLine(()=>{setLine4(!line4)},line4,8)}
{VirticalLine(()=>{setLine5(!line5)},line5,16)}
{VirticalLine(()=>{setLine6(!line6)},line6,32)}
{VirticalLine(()=>{setLine7(!line7)},line7,64)}
{VirticalLine(()=>{setLine8(!line8)},line8,128)}
{VirticalLine(()=>{setLine9(!line9)},line9,256)}
{/* {VirticalLine(()=>{setLine10(!togalIson? !line10 : false)},line10,!togalIson? 512 : 0)} */}
{/* <View style={{}}>
  <Text style={{color:'gray',fontSize:10,top:-1}}></Text>
  <View style={{width:5,height:5,backgroundColor:'transparent',borderRadius:3}} />
</View> */}

</View>

 
<View style={{top:-27,flexDirection:'row',justifyContent:'space-between',backgroundColor:MyColor.HEADERCOLOR,marginHorizontal:20,borderBottomLeftRadius:10,borderBottomRightRadius:10,paddingHorizontal:16}}>
{linearray.map(MyLine)}
</View>

{/* <View style={{width:dimensions.SCREEN_WIDTH,height:2,backgroundColor:'gray'}} /> */}
{/* ******************************************** */}   
</ScrollView> 
<View style={{position:'absolute',bottom:30,width:dimensions.SCREEN_WIDTH-40,alignSelf:'center',backgroundColor:MyColor.SUBBOXCOLOR,padding:5,borderRadius:10}}>
  {/* **************row 1 ******************** */}    
<View style={{flexDirection:'row',width:dimensions.SCREEN_WIDTH-40,alignSelf:'center',justifyContent:'space-between',backgroundColor:MyColor.SUBBOXCOLOR,paddingHorizontal:10,borderTopLeftRadius:10,borderTopRightRadius:10}}>
{myarray[0].row1.map(MyButton)}
</View>
{/* **************end row 1 start row 2 ******************** */}  
<View style={{flexDirection:'row',width:dimensions.SCREEN_WIDTH-40,alignSelf:'center',justifyContent:'space-between',backgroundColor:MyColor.SUBBOXCOLOR,paddingHorizontal:10}}>
{myarray[1].row2.map(MyButton)}
</View>
{/* **************end row 2 start row 3 ******************** */}  
<View style={{flexDirection:'row',width:dimensions.SCREEN_WIDTH-40,alignSelf:'center',justifyContent:'space-between',backgroundColor:MyColor.SUBBOXCOLOR,paddingHorizontal:10}}>
{myarray[2].row3.map(MyButton)}
</View>
{/* **************end row 3 start row 4 ******************** */}  
<View style={{flexDirection:'row',width:dimensions.SCREEN_WIDTH-40,alignSelf:'center',justifyContent:'space-between',backgroundColor:MyColor.SUBBOXCOLOR,paddingHorizontal:15,borderBottomLeftRadius:10,borderBottomRightRadius:10}}>
<TouchableOpacity style={{width:dimensions.SCREEN_WIDTH*24.5/100,height:dimensions.SCREEN_HEIGHT*4/100,backgroundColor:MyColor.BOXCOLOR,justifyContent:'center',marginVertical:7,borderRadius:10
,shadowColor: '#000',
shadowOffset: {
  width: 0,
  height: 2
},
shadowRadius: 1,
shadowOpacity: 1,
justifyContent: 'center',
elevation: 5,
alignSelf: 'center'}}
    onPress={()=>plusSixteenPressed()} >
    <Text style={{alignSelf:'center',color:MyColor.TEXTCOLOR,fontWeight:'bold',fontSize:20}}>+16</Text>
    </TouchableOpacity>
{MyButton(0)}
<TouchableOpacity style={{width:dimensions.SCREEN_WIDTH*24.5/100,height:dimensions.SCREEN_HEIGHT*4/100,backgroundColor:MyColor.BOXCOLOR,justifyContent:'center',marginVertical:7,borderRadius:10,
shadowColor: '#000',
shadowOffset: {
  width: 0,
  height: 2
},
shadowRadius: 1,
shadowOpacity: 1,
justifyContent: 'center',
elevation: 5,
alignSelf: 'center'}}
    onPress={()=>ClearPressed()}>
    <Text style={{alignSelf:'center',color:MyColor.TEXTCOLOR,fontWeight:'bold',fontSize:20}}>Clear</Text>
    </TouchableOpacity>
</View>
{/* **************end rows  ******************** */}  

</View>
<Modal
        isVisible={modlevisual}
        swipeDirection="down"
        onSwipeComplete={(e) => {
          setmodlevisual(false)
        }}
          scrollTo={() => {}}
          scrollOffset={1}
          propagateSwipe={true}
        coverScreen={false}
        backdropColor='transparent'
        style={{ justifyContent: 'flex-end', margin: 0, backgroundColor: 'rgba(0,0,0,0.5)' }}
      >
        <View style={{ height: '60%', backgroundColor: MyColor.SUBBOXCOLOR, borderTopLeftRadius: 30, borderTopRightRadius: 30, padding: 20 }}>
          <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>

            <Text style={{ color: '#FFF', fontSize: 25, fontWeight: 'bold' }}>Save as favorites</Text>
            {/* <Text style={{ color: '#FFF', fontSize: 13, marginTop: 5, }}>Add your favourite fixture for future purposes</Text> */}
            <Text style={{ color: '#FFF', fontSize: 15, marginTop: 25, }}>DMX Value</Text>

            <View style={{ width: '100%', height: 47, borderRadius: 28, marginTop: 10 }}>
              <TextInput
                value={displaytext}
               // onChangeText={(e) => setTitle(e)}
                placeholder={''}
                placeholderTextColor="#bbbbbb"
                // keyboardType="number-pad"
                editable={false}
                autoCapitalize='none'
                style={styles.input}
              />
            </View>

            <Text style={{ color: '#FFF', fontSize: 15, marginTop: 25, }}>Enter Title</Text>

            <View style={{ width: '100%', height: 47, borderRadius: 28, marginTop: 10 }}>
              <TextInput
                value={title}
                onChangeText={(e) => settitle(e)}
                placeholder={''}
                placeholderTextColor="#bbbbbb"
                // keyboardType="number-pad"
                // editable={false}
                autoCapitalize='none'
                style={[styles.input,{paddingLeft:20,fontSize:13}]}
              />
            </View>
          
            <View style={{ width: '75%', height: 45, backgroundColor: '#0000ff', alignSelf: 'center', borderRadius: 30, flexDirection: 'row', justifyContent: 'center', marginTop: 40 }}  >
              <Image source={require('../assets/ic-favourite.png')} style={{ width: 27, height: 23, alignSelf: 'center', marginRight: 10 }} resizeMode='cover'></Image>
              <Text style={{ color: MyColor.TEXTCOLOR, fontSize: 16, alignSelf: 'center' }} onPress={() => { submitTask() }}>Save as favorites</Text>
            </View>

            <Text style={{ color: MyColor.TEXTCOLOR, fontSize: 14, alignSelf: 'center', marginVertical: 10 }} onPress={() => { props.navigation.navigate('FavouriteFixture') }}>See all recent favorites</Text>

          </KeyboardAwareScrollView>
          { loading ?
        <Activity_Indicator />
        :
        null
        }
        </View>
      </Modal>

       { loading ?
        <Activity_Indicator />
        :
        null
        }
      </SafeAreaView>
  );
};

let styles = StyleSheet.create({
    container:{
      flex: 1,
    backgroundColor:MyColor.BGCOLOR
    },
    input: {
      paddingLeft: 15,
      height: 45,
      width:'95%',
      fontSize: 25,
      borderColor: null,
      backgroundColor: MyColor.BOXCOLOR,
      borderRadius:28,
      color:'#fff',
      alignSelf:'center'
  
    },
})

export default DmxDigital;

