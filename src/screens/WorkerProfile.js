import React, { useEffect, useState } from 'react'
import { SafeAreaView, StyleSheet, TouchableOpacity, FlatList, StatusBar, Text, ScrollView, ImageBackground, View, Image, Dimensions, TextInput, Alert, Linking, } from 'react-native';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import Header from '../Component/Header'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Modal from 'react-native-modal';
import ToggleSwitch from 'toggle-switch-react-native'
import CalendarPicker from 'react-native-calendar-picker';
import * as types from '../redux/types';
import { Picker } from "native-base";
import Activity_Indicator from '../Component/Activity_Indicator';
import Toast from 'react-native-simple-toast';
import { dimensions, MyColor, urls } from '../utils/constants';
import { useSelector, useDispatch } from 'react-redux';
import { my_profile, change_status_of_worker,download_task_list,get_assign_task_list, add_task,get_job_type, get_worker_list, requestPostApi, requestGetApi, remove_worker ,baseUrl} from '../WebApis/Service'
import { setEmpolyeeList } from '../redux/actions/user_action'
import DateTimePicker from '@react-native-community/datetimepicker';
import DatePicker from 'react-native-datepicker'
import { Mycolors } from '../utils/Mycolors';

const WorkerProfile = (props) => {
  const token = useSelector(state => state.user.token)
  const userData  = useSelector(state => state.user.user_details)  
  const dispatch = useDispatch()
  const [loading, setLoading] = useState(false);
  const [togalIson, setTogalIson] = useState(false);
  const [removeWorker, setRemoveWorker] = useState(false);
  const [deactivate, setDeactivate] = useState(false)
  const [worker_Profile, setWorker_Profile] = useState(null);
  let HolidayArray = ["08/03/2021", "08/16/2021", "08/13/2021"];
  let NotPresent = ["08/19/2021", "08/26/2021", "08/11/2021"];

  const [sch_job, setsch_job] = useState(false);
  const [date, setDate] = useState('');
  const [fromdate, setfromDate] = useState('');
  const [todate, settoDate] = useState('');
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);
  const [num, setnum] = useState('D1');

 
  // const [loading,setLoading]=useState(false);
  const [taskdata, setTaskData] = useState(null);
  const [downlodtask, setdownlodtask] = useState(false);
  const [projectname, setProjectName] = useState('');
  const [location, setLocation] = useState('');
  const [tasknote, setTasknote] = useState('');
  const [totalWorkingTime, settotalWorkingTime] = useState('')
  const [startTime, setStartTime] = useState('');
  const [endTime, setEndTime] = useState('');
  const [apistartTime,setapiStartTime]=useState('');
  const [apiendTime,setapiEndTime]=useState('');
  const [startTimeTrue, setStartTimeTrue] = useState(false);
  const [endTimeTrue, setEndTimeTrue] = useState(false);
  const [job, setjob] = useState('')
  const [jobdata, setjobdata] = useState([])
  // useEffect(() => {
  //   console.log('the id is', props.route.params.id)
  //  // workerProfile()
  // }, [])
  useEffect(() => {
    //   console.log('The Date is :--',props.route.params.day) 
    //      let totaldates=new Date().toString()
    //      let monthss=totaldates.substring(4,7)
    //      let dates=totaldates.substring(8,10)
    //      let year=totaldates.substring(11,15)
    //      var ids=(d,m,y)=>{
    //    if(m=='Jan'){
    //      return d+'-01-'+y
    //    }else if(m=='Fab'){
    //      return d+'-02-'+y
    //    } else if(m=='Mar'){
    //      return d+'-03-'+y
    //    }else if(m=='Apr'){
    //      return d+'-04-'+y
    //    }else if(m=='May'){
    //      return d+'-05-'+y
    //    }else if(m=='Jun'){
    //      return d+'-06-'+y
    //    }else if(m=='Jul'){
    //      return d+'-07-'+y
    //    }else if(m=='Aug'){
    //      return d+'-08-'+y
    //    }else if(m=='Sep'){
    //      return d+'-09-'+y
    //    }else if(m=='Oct'){
    //      return d+'-10-'+y
    //    }else if(m=='Nov'){
    //      return d+'-11-'+y
    //    }else if(m=='Dec'){
    //      return d+'-12-'+y
    //    }
    //  }
    //  TaskByDay(ids(dates,monthss,year))

  }, [])

  const DownloadLink=()=>{
    var url=baseUrl+download_task_list+'?from_date='+fromdate+'&to_date='+todate+'&user_id='+userData.user_id
    console.log('the url is',url);
    Linking.openURL(baseUrl+download_task_list+'?from_date='+fromdate+'&to_date='+todate+'&user_id='+userData.user_id)
  }

  const GetApi=async()=>{
   
     if(fromdate!='' && todate!=''){
    setLoading(true)
  const{responseJson,err}  = await requestGetApi(get_assign_task_list+'?from_date='+fromdate+'&to_date='+todate+'&worker_id='+props.route.params.id,{},'GET',token)
  setLoading(false)
        if(err==null){
             if(responseJson.status){
               setTaskData(responseJson.data)
               settotalWorkingTime(responseJson.total_working_time)
              }else{ 
                setTaskData(null)
                Toast.show(responseJson.message);
              }
          }else{
            Toast.show(err);
          }
  }else{
    Toast.show('Please Add select From and To date both')
  }
}

  const getJobtype=async()=>{
     setLoading(true)
    const{responseJson,err}  = await requestGetApi(get_job_type,{},'GET',token)
     setLoading(false)
         if(err==null){
              if(responseJson.status){
                console.log('tasktype is==>',responseJson.data);
                var dumyArr=[]
                for(let i=0;i<responseJson.data.length;i++){
                  dumyArr.push( {label:responseJson.data[i].job_type, value:responseJson.data[i].id})
                }
                setjobdata(dumyArr) 
               }else{ 
                Toast.show(responseJson.message);
               }
           }else{
           // Alert.alert(err)
           }
  }
  
  const assignNewTask = async () => {
    if (projectname == '' || apistartTime == '' || apiendTime == '' || location == '' || tasknote == '' || date == '' || job == '') {
      Alert.alert('All are mandatory fields *')
    } else {
      let formdata = new FormData();
      formdata.append('worker_id', props.route.params.id);
      formdata.append('project_name', projectname);
      formdata.append('start_time', apistartTime);
      formdata.append('end_time', apiendTime);
      formdata.append('location', location);
      formdata.append('note', tasknote);
      formdata.append('date', date);
      formdata.append("task_type", job);
      console.log('the formdata is==>', formdata)
      setLoading(true)
      const { responseJson, err } = await requestPostApi(add_task, formdata, 'POST', token)
      setLoading(false)
      if (err == null) {
        if (responseJson.status) {
          setsch_job(false)
          Toast.show(responseJson.message);
        } else {
          Toast.show(responseJson.message);
        }
      } else {
        Toast.show(err);
      }
    }
  }

  const workerProfile = async () => {
    setLoading(true)
    const { responseJson, err } = await requestGetApi(my_profile + '/' + props.route.params.id, {}, 'GET', token)
    setLoading(false)
    if (err == null) {
      if (responseJson.status) {
        setWorker_Profile(responseJson.data)
        if (responseJson.data.status != 1) {
          setTogalIson(true)
        }
      } else {
        Toast.show(responseJson.message);
      }
    } else {
      Toast.show(err);
    }
  }

  const MycustomDatesStylesCallback = date => {
    // Holly Days..........
    for (let i = 0; i < HolidayArray.length; i++) {
      let formatedDate = new Date(HolidayArray[i]).toString()
      let formatedDate2 = date.toString()
      if (formatedDate.substring(0, 15) == formatedDate2.substring(0, 15)) {
        return {
          textStyle: {
            color: 'white', //blue
          }
        }
      }
    }
    // sunday Days..........
    switch (date.isoWeekday()) {
      case 7: // Sunday
        return {
          textStyle: {
            color: 'white', //blue
          }
        };
    }

    // Not Present..........
    for (let i = 0; i < NotPresent.length; i++) {
      let formatedDate = new Date(NotPresent[i]).toString()
      let formatedDate2 = date.toString()
      if (formatedDate.substring(0, 15) == formatedDate2.substring(0, 15)) {
        return {
          textStyle: {
            color: 'white', //red
          }
        }
      }
    }
  }

  const RemoveWorkerApi = async () => {
    let formdata = new FormData();
    formdata.append('worker_id', props.route.params.id);
    setLoading(true)
    const { responseJson, err } = await requestPostApi(remove_worker, formdata, 'POST', token)
    // setLoading(false)
    if (err == null) {
      if (responseJson.status) {
        employee_List()
        //   setRemoveWorker(false)
        //  props.navigation.navigate('EmployeeList')

        // props.navigation.reset({
        //   routes: [{ name: 'EmployeeList' }]
        // });

        // Toast.show('Worker Removed');
      } else {
        Toast.show(responseJson.message);
        setLoading(false)
      }
    } else {
      Toast.show(err);
      setLoading(false)
    }
  }

  const employee_List = async () => {

    const { responseJson, err } = await requestGetApi(get_worker_list, {}, 'GET', token)
    setLoading(false)
    if (err == null) {
      if (responseJson.status) {

        dispatch(setEmpolyeeList(responseJson.data))
        setRemoveWorker(false)
        props.navigation.navigate('EmployeeList')
        Toast.show('Worker Removed');
      } else {
        // Toast.show(responseJson.message);
      }
    } else {
      // Alert.alert(err)
    }
  }

  const changeWorkerStatus = async (ChangedStatus) => {
    setLoading(true)
    let formdata = new FormData();
    formdata.append("worker_id", props.route.params.id);
    formdata.append("status", ChangedStatus ? '1' : '0');
    const { responseJson, err } = await requestPostApi(change_status_of_worker, formdata, 'POST', token)
    setLoading(false)
    if (err == null) {
      if (responseJson.status) {
        Toast.show(responseJson.message);
        if (ChangedStatus) {
          setTogalIson(false)
          setDeactivate(false)
        } else {
          setTogalIson(true)
          setDeactivate(false)
        }
      } else {
        //Toast.show(responseJson.message);
      }
    } else {
      Toast.show(err);
    }
  }

  const changeMod = (mod, mynum) => {
    setMode(mod)
    setnum(mynum)
    setShow(true)

  }

  const tConvert = (time)=> {
    // Check correct time format and split into components
    time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
  
    if (time.length > 1) { // If time format correct
      time = time.slice (1);  // Remove full string match value
      time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM 
      time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    return time.join (''); // return adjusted time or original string
  }

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.container}>
        {/* ****************Header********************** */}
        <View style={{ backgroundColor: MyColor.BOXCOLOR }}>
          <Header Lable="Workers Profile" Nav={() => props.navigation.goBack()} />

        </View>
        {/* ****************Header end********************** */}
        <ScrollView>
          {/* <View style={{ width: '100%', height: 140, backgroundColor: MyColor.BOXCOLOR, top: -15, zIndex: -999, borderBottomLeftRadius: 20, borderBottomRightRadius: 20 }}>
            <View style={{ width: 150, height: 150, alignSelf: 'center', borderRadius: 75, backgroundColor: '#fff', top: 50 }}>
              <Image source={require('../assets/profile.png')} style={{ width: 150, height: 150, alignSelf: 'center', zIndex: -999 }} resizeMode='cover'></Image>
            </View>
            <View style={{ top: 35 }}>
              <TouchableOpacity style={{ width: 150, height: 25, alignSelf: 'center', backgroundColor: 'blue', borderRadius: 9, justifyContent: 'center' }} onPress={() => { setRemoveWorker(true) }}>
                <Text style={{ alignSelf: 'center', color: MyColor.TEXTCOLOR, fontSize: 11 }} onPress={() => { setRemoveWorker(true) }}>Remove Worker</Text>
              </TouchableOpacity>
              <Text style={{ alignSelf: 'center', color: MyColor.TEXTCOLOR, margin: 5 }}>{worker_Profile != null ? worker_Profile.name : ''}</Text>
              <Text style={{ alignSelf: 'center', color: MyColor.TEXT2COLOR }}>{worker_Profile != null ? worker_Profile.email : ''}</Text>
            </View>
          </View> */}

          <ImageBackground source={require('../assets/profile-bgCover.png')} style={{ width: '100%', height: 160, alignSelf: 'center', overflow: 'hidden', borderBottomLeftRadius: 20, borderBottomRightRadius: 20, top: -15, zIndex: -999 }}></ImageBackground>
          <View style={{ width: 150, height: 150, alignSelf: 'center', borderRadius: 75, backgroundColor: '#fff', top: -110 }}>
            <Image source={require('../assets/profile.png')} style={{ width: 150, height: 150, alignSelf: 'center', zIndex: -999 }} resizeMode='cover'></Image>
          </View>
          <View style={{ top: -130 }}>
            <TouchableOpacity style={{ width: 150, height: 25, alignSelf: 'center', backgroundColor: 'blue', borderRadius: 9, justifyContent: 'center' }} onPress={() => { setRemoveWorker(true) }}>
              <Text style={{ alignSelf: 'center', color: MyColor.TEXTCOLOR, fontSize: 11 }} onPress={() => { setRemoveWorker(true) }}>Remove Worker</Text>
            </TouchableOpacity>
            <Text style={{ alignSelf: 'center', color: MyColor.TEXTCOLOR, margin: 5 }}>{worker_Profile != null ? worker_Profile.name : ''}</Text>
            <Text style={{ alignSelf: 'center', color: MyColor.TEXT2COLOR }}>{worker_Profile != null ? worker_Profile.email : ''}</Text>
          </View>

          {/* <View style={{ width: '100%', height: 2, marginTop: 120 }}></View> */}
          <View style={{ top: -100 }}>
            <View style={{ width: dimensions.SCREEN_WIDTH - 20, alignSelf: 'center', flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 10, backgroundColor: MyColor.BOXCOLOR, height: 45, borderRadius: 10, alignItems: 'center' }}>
              <Text style={{ color: MyColor.TEXTCOLOR }}>Active Worker</Text>
              <ToggleSwitch
                isOn={!togalIson}
                onColor="blue"
                offColor="gray"
                label=""
                labelStyle={{ color: "black", fontWeight: "900", borderColor: 'red', borderWidth: 1 }}
                size="small"
                onToggle={isOn => {
                  setTogalIson(!togalIson)
                  setDeactivate(true)
                }}
              />
            </View>

            <View style={{ backgroundColor: MyColor.BOXCOLOR, width: dimensions.SCREEN_WIDTH - 20, alignSelf: 'center', borderRadius: 10, marginTop: 15, padding: 10, marginBottom: 10 }}>
              <CalendarPicker
                onDateChange={(e) => { props.navigation.navigate('TaskAndTimeDetails', { day: e.toString(), workerId: props.route.params.id }) }}
                textStyle={{ color: '#FFF' }}
                width={dimensions.SCREEN_WIDTH - 30}
                height={dimensions.SCREEN_HEIGHT * 45 / 100}
                todayTextStyle={{ fontWeight: 'bold', color: '#FFF' }}
                todayBackgroundColor={'transparent'}
                // customDatesStyles={customDatesStyles}
                // minDate={today}
                customDatesStyles={MycustomDatesStylesCallback}
              />

              {/* <View style={{ flexDirection: 'row', alignSelf: 'center', marginVertical: 20 }}>
              <View style={{ width: 15, height: 15, borderRadius: 8, backgroundColor: '#FFF' }}></View>
              <Text style={{ color: '#FFF', fontSize: 11, top: 1, marginLeft: 5 }}>PRESENT</Text>
              <View style={{ width: 15, height: 15, borderRadius: 8, backgroundColor: 'red', marginLeft: 20 }}></View>
              <Text style={{ color: '#FFF', fontSize: 11, top: 1, marginLeft: 5 }}>NOT PRESENT</Text>
              <View style={{ width: 15, height: 15, borderRadius: 8, backgroundColor: 'blue', marginLeft: 20 }}></View>
              <Text style={{ color: '#FFF', fontSize: 11, top: 1, marginLeft: 5 }}>HOLIDAY</Text>
            </View> */}
            </View>
          </View>
          <View style={{ width: 10, height: 30 }}></View>
        </ScrollView>
        <View style={{ flexDirection: 'row', width: dimensions.SCREEN_WIDTH - 20, justifyContent: 'space-between', alignSelf: 'center' }}>
          <TouchableOpacity style={{
            height: 50, justifyContent: 'center', width: dimensions.SCREEN_WIDTH * 30 / 100,
            alignSelf: 'center', backgroundColor: MyColor.BUTTONCOLOR, borderRadius: 15, marginBottom: 10
          }}
            onPress={() => { props.navigation.navigate('RegularisationReq', { id: props.route.params.id }) }}>
            <Text style={{ color: MyColor.TEXTCOLOR, textAlign: 'center', fontWeight: '600', paddingHorizontal: 5, fontSize: 10 }}>See Regularization Requests</Text>
          </TouchableOpacity>

          <TouchableOpacity style={{
            height: 50, justifyContent: 'center', width: dimensions.SCREEN_WIDTH * 30 / 100,
            alignSelf: 'center', backgroundColor: MyColor.BUTTONCOLOR, borderRadius: 15, marginBottom: 10
          }}
            onPress={() => {  setdownlodtask(true) }}>
            <Text style={{ color: MyColor.TEXTCOLOR, textAlign: 'center', fontWeight: '600', paddingHorizontal: 5, fontSize: 10 }}>Download report</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{
            height: 50, justifyContent: 'center', width: dimensions.SCREEN_WIDTH * 30 / 100,
            alignSelf: 'center', backgroundColor: MyColor.BUTTONCOLOR, borderRadius: 15, marginBottom: 10
          }}
            onPress={() => { 
              getJobtype()
              setsch_job(true)
               }}>
            <Text style={{ color: MyColor.TEXTCOLOR, textAlign: 'center', fontWeight: '600', paddingHorizontal: 5, fontSize: 10 }}>Schedule a job</Text>
          </TouchableOpacity>
        </View>


        {/* *****************************deactivate************************************* */}
        <Modal
          isVisible={deactivate}
          swipeDirection="down"
          onSwipeComplete={(e) => {
            setTogalIson(false)
            setDeactivate(false)
          }}
          scrollTo={() => { }}
          scrollOffset={1}
          propagateSwipe={true}
          coverScreen={false}
          backdropColor='transparent'
          style={{ justifyContent: 'flex-end', margin: 0, backgroundColor: 'rgba(0,0,0,0.5)' }}
        >
          <View style={{ height: 380, backgroundColor: MyColor.SUBBOXCOLOR, borderTopLeftRadius: 30, borderTopRightRadius: 30, padding: 20 }}>
            <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
              {togalIson ?
                <>
                  <View style={{ alignSelf: 'center' }}>
                    <Image source={require('../assets/deactivate-worker.png')} style={{ width: 150, height: 122, alignSelf: 'center', zIndex: -999 }} resizeMode='cover'></Image>
                  </View>
                  <Text style={{ color: MyColor.TEXTCOLOR, alignSelf: 'center', fontWeight: 'bold', fontSize: 22, marginTop: 15 }}>Deactivate Worker</Text>
                  <View style={{ width: '80%', alignSelf: 'center' }}>
                    <Text style={{ color: MyColor.TEXTCOLOR, textAlign: 'center', fontSize: 13, marginTop: 15 }}>Are you sure to deactivate this worker ?</Text>
                  </View>
                  <TouchableOpacity style={{ width: '85%', height: 40, backgroundColor: '#0000ff', alignSelf: 'center', borderRadius: 30, flexDirection: 'row', justifyContent: 'center', marginTop: 20, paddingHorizontal: 10 }} onPress={() => {
                    changeWorkerStatus(false)
                  }}>
                    <Text style={{ color: '#FFF', fontSize: 14, alignSelf: 'center', textAlign: 'center' }} onPress={() => {
                      changeWorkerStatus(false)
                    }}>Yes I'am sure to deactivate this worker</Text>
                  </TouchableOpacity>

                  <TouchableOpacity style={{ width: '85%', height: 40, backgroundColor: 'transparent', alignSelf: 'center', borderRadius: 30, flexDirection: 'row', justifyContent: 'center', marginTop: 20, paddingHorizontal: 10, borderColor: '#0000ff', borderWidth: 1 }} onPress={() => {
                    setTogalIson(false)
                    setDeactivate(false)
                  }}>
                    <Text style={{ color: '#FFF', fontSize: 14, alignSelf: 'center', textAlign: 'center' }} onPress={() => {
                      setTogalIson(false)
                      setDeactivate(false)
                    }}>No, Thanks</Text>
                  </TouchableOpacity>
                  {/* 
<Text style={{color:MyColor.TEXTCOLOR,alignSelf:'center',marginTop:15}} onPress={()=>{
    setTogalIson(false)
    setDeactivate(false)
}}>No, Thanks</Text> */}
                </>
                :
                <>
                  <View style={{ alignSelf: 'center' }}>
                    <Image source={require('../assets/activate-account.png')} style={{ width: 185, height: 122, alignSelf: 'center', zIndex: -999 }} resizeMode='cover'></Image>
                  </View>
                  <Text style={{ color: MyColor.TEXTCOLOR, alignSelf: 'center', fontWeight: 'bold', fontSize: 22, marginTop: 15 }}>Activate Worker</Text>
                  <View style={{ width: '80%', alignSelf: 'center' }}>
                    <Text style={{ color: MyColor.TEXTCOLOR, textAlign: 'center', fontSize: 13, marginTop: 15 }}>Are you sure to activate this worker ?</Text>
                  </View>
                  <TouchableOpacity style={{ width: '75%', height: 40, backgroundColor: '#0000ff', alignSelf: 'center', borderRadius: 30, flexDirection: 'row', justifyContent: 'center', marginTop: 20 }} onPress={() => {
                    changeWorkerStatus(true)
                  }}>
                    <Text style={{ color: '#FFF', fontSize: 14, alignSelf: 'center' }} onPress={() => {
                      changeWorkerStatus(true)
                    }}>Yes I'am sure to activate worker</Text>
                  </TouchableOpacity>

                  <TouchableOpacity style={{ width: '85%', height: 40, backgroundColor: 'transparent', alignSelf: 'center', borderRadius: 30, flexDirection: 'row', justifyContent: 'center', marginTop: 20, paddingHorizontal: 10, borderColor: '#0000ff', borderWidth: 1 }} onPress={() => {
                    setTogalIson(true)
                    setDeactivate(false)
                  }}>
                    <Text style={{ color: '#FFF', fontSize: 14, alignSelf: 'center', textAlign: 'center' }} onPress={() => {
                      setTogalIson(true)
                      setDeactivate(false)
                    }}>No, Thanks</Text>
                  </TouchableOpacity>

                  {/* <Text style={{color:MyColor.TEXTCOLOR,alignSelf:'center',marginTop:15}} onPress={()=>{
    setTogalIson(true)
    setDeactivate(false)
}}>No, Thanks</Text> */}
                </>
              }

              <View style={{ width: 100, height: 100 }} />
            </KeyboardAwareScrollView>
            {loading ?
              <Activity_Indicator />
              :
              null
            }
          </View>
        </Modal>
        {/* *****************************removeWorker************************************* */}
        <Modal
          isVisible={removeWorker}
          swipeDirection="down"
          onSwipeComplete={(e) => {
            setRemoveWorker(false)
          }}
          scrollTo={() => { }}
          scrollOffset={1}
          propagateSwipe={true}
          coverScreen={false}
          backdropColor='transparent'
          style={{ justifyContent: 'flex-end', margin: 0, backgroundColor: 'rgba(0,0,0,0.5)' }}
        >
          <View style={{ height: 250, backgroundColor: MyColor.SUBBOXCOLOR, borderTopLeftRadius: 30, borderTopRightRadius: 30, padding: 20 }}>
            <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
              <Text style={{ color: MyColor.TEXTCOLOR, alignSelf: 'center', fontWeight: 'bold', fontSize: 22, marginTop: 15 }}>Remove Worker</Text>
              <View style={{ width: '80%', alignSelf: 'center' }}>
                <Text style={{ color: MyColor.TEXTCOLOR, textAlign: 'center', fontSize: 13, marginTop: 15 }}>Worker Profile will be deleted permanently</Text>
              </View>
              <TouchableOpacity style={{ width: '75%', height: 40, backgroundColor: '#0000ff', alignSelf: 'center', borderRadius: 30, flexDirection: 'row', justifyContent: 'center', marginTop: 20 }} onPress={() => {
                RemoveWorkerApi()
              }}>
                <Text style={{ color: '#FFF', fontSize: 14, alignSelf: 'center' }} onPress={() => { RemoveWorkerApi() }}>Yes I'am sure to delete the worker</Text>
              </TouchableOpacity>
              <Text style={{ color: MyColor.TEXTCOLOR, alignSelf: 'center', marginTop: 15 }} onPress={() => {
                setRemoveWorker(false)
              }}>No, Thanks</Text>
              <View style={{ width: 100, height: 100 }} />
            </KeyboardAwareScrollView>
            {loading ?
              <Activity_Indicator />
              :
              null
            }
          </View>
        </Modal>

        {/* *******************************sch_job*********************************** */}
        {/* *********************modle1**************************** */}
        <Modal
          isVisible={sch_job}
          swipeDirection="down"
          onSwipeComplete={(e) => {
            setsch_job(false)
          }}
          scrollTo={() => { }}
          scrollOffset={1}
          propagateSwipe={true}
          coverScreen={false}
          backdropColor='transparent'
          style={{ justifyContent: 'flex-end', margin: 0, backgroundColor: 'rgba(20,20,20,1)' }}
        >
          <View style={{ height: '90%', backgroundColor: MyColor.SUBBOXCOLOR, borderTopLeftRadius: 30, borderTopRightRadius: 30, padding: 20, marginHorizontal: 10 }}>
            <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps='always'>
              {/* <TouchableWithoutFeedback activeOpacity={1}> */}

              <View style={{ alignSelf: 'center' }}>
                <Image source={require('../assets/assign-new-task.png')} style={{ width: 120, height: 120, alignSelf: 'center', zIndex: -999 }} resizeMode='cover'></Image>
              </View>

              <Text style={{ color: '#FFF', fontSize: 25, marginTop: 10, }}>Assign New Task</Text>

              {/* ********************************************************   */}
              <Text style={{ color: '#fff', fontSize: 15, marginTop: 15 }}>Select Date</Text>
              <View style={{ width: '100%', height: 39, borderRadius: 28, marginTop: 10, zIndex: 999 }}>

                <DatePicker
                  customStyles={{
                    dateInput: { borderColor: 'transparent', left: -90 },
                    dateText: { color: '#fff' },
                    dateIcon: styles.dateIcon,
                    dateplaceholder: {
                      alignContent: 'flex-start',

                    },
                    placeholderText: {
                      fontSize: 15,
                      color: 'gray',
                      marginLeft: '5%',
                      left: -100
                    },
                    zIndex: 999
                  }}

                  androidMode={'spinner'}
                  readOnly={true}
                  style={[styles.datePickerSelectInput]}
                  date={date}
                  mode="date"
                  placeholder={'Select date'}
                  minDate={new Date()}
                  format='YYYY-MM-DD'
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  iconSource={require('../assets/time-attendace.png')}
                  onDateChange={date => {
                    setDate(date)
                  }}
                />



              </View>
              {/* ********************************************************   */}

              <Text style={{ color: '#fff', fontSize: 15, marginTop: 20 }}>Job Type</Text>

              <View style={{ width: '100%', height: 40, borderRadius: 20, backgroundColor: '#34333a', marginTop: 10 }}>

                <Picker
                  placeholder={"Select Job Type"}
                  selectedValue={job}
                  style={[styles.input]}
                  textStyle={{ color: '#fff' }}
                  onValueChange={(itemValue, itemIndex) => {
                    setjob(itemValue)
                  }}
                >
                  <Picker.Item label="Select Job Type" value="" />
                  {jobdata.map((item, index) => {
                    return <Picker.Item key={index} label={item.label} value={item.value} />
                  })}
                </Picker>

              </View>
              {/* ********************************************************   */}

              <Text style={{ color: '#fff', fontSize: 15, marginTop: 20 }}>Project Name</Text>
              <View style={{ width: '100%', height: 47, borderRadius: 28, marginTop: 10 }}>

                <TextInput
                  value={projectname}
                  onChangeText={(e) => setProjectName(e)}
                  placeholder={''}
                  placeholderTextColor="gray"
                  // keyboardType="number-pad"
                  autoCapitalize='none'
                  style={styles.input}
                />


              </View>
              {/* //////////////////////////////////////////////////// */}
              {/* <View style={{ width: '100%', height: 60, borderRadius: 28, marginTop: 5, flexDirection: 'row', justifyContent: 'space-between' }}> */}
              <View style={{ width: '100%', height: 60, }}>
                <Text style={{ color: '#FFF', fontSize: 15, marginTop: 18, top: -8 }}>Start Time</Text>
                <View style={{ width: '100%', height: 47, backgroundColor: '#34333a', borderRadius: 10 }}>
                  {Platform.OS == 'ios' ?

                    <DatePicker
                      customStyles={{
                        dateInput: { borderColor: 'transparent' },
                        dateText: { color: '#fff' },
                        dateIcon: styles.dateIcon,
                        dateplaceholder: {
                          alignContent: 'flex-start',

                        },
                        placeholderText: {
                          fontSize: 15,
                          color: 'gray',
                          alignSelf: 'flex-start',
                          left: 10
                        },
                      }}
                      showIcon={false}
                      androidMode={'spinner'}
                      is24Hour={false}
                      readOnly={true}
                      style={[styles.datePickerSelectInput]}
                      //   date={startTime}
                      mode="Time"
                      placeholder={startTime != '' ? startTime : 'Select Time'}
                      // maxDate={new Date ()}
                      //format='YYYY-MM-DD'
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      // iconSource={require ('../assets/time-attendace.png')}
                      onDateChange={sTime => {
                        console.log('stime',sTime)
                        let st = sTime.substring(11, 19)
                        var mt= tConvert(st)
                            setapiStartTime(st)
                            setStartTime(mt)
                      }}
                    />
                    :
                    startTimeTrue ?
                      <View>
                        <DateTimePicker
                          value={new Date()}
                          mode='time'
                          is24Hour={false}
                          display="spinner"
                          onChange={(event, sTime) => {
                            setStartTimeTrue(false)
                            let st = sTime.toString()
                            let nst = st.substring(16, 24)
                           var mt= tConvert(nst)
                            setapiStartTime(nst)
                            setStartTime(mt)

                          }}
                        />
                      </View>
                      :
                      <TouchableOpacity style={{ width: '100%', height: '100%', justifyContent: 'center' }}>
                        <Text style={{ alignSelf: 'center', fontSize: 15, color: '#fff' }} onPress={() => { setStartTimeTrue(true) }}>{startTime != '' ? startTime : 'Select Time'}</Text>
                      </TouchableOpacity>

                  }

                </View>
              </View>

              <View style={{ width: '100%', height: 60, marginTop: 40 }}>
                <Text style={{ color: '#FFF', fontSize: 15, top: -8 }}>End Time</Text>
                <View style={{ width: '100%', height: 47, backgroundColor: '#34333a', borderRadius: 10 }}>

                  {Platform.OS == 'ios' ?
                    <DatePicker
                      customStyles={{
                        dateInput: { borderColor: 'transparent' },
                        dateText: { color: '#fff' },
                        dateIcon: styles.dateIcon,
                        dateplaceholder: {
                          alignContent: 'flex-start',

                        },
                        placeholderText: {
                          fontSize: 15,
                          color: 'gray',
                          alignSelf: 'flex-start',
                          left: 10
                        },

                      }}
                      showIcon={false}
                      androidMode={'spinner'}
                      is24Hour={false}
                      readOnly={true}
                      style={[styles.datePickerSelectInput]}
                      // date={endTime}
                      mode="Time"
                      placeholder={endTime != '' ? endTime : 'Select Time'}
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      onDateChange={sTime => {
                        let st = sTime.substring(11, 19)
                        var mt= tConvert(st)
                        setapiEndTime(st)
                        setEndTime(mt)
                      }}
                    />
                    :
                    endTimeTrue ?
                      <View>
                        <DateTimePicker
                          value={new Date()}
                          mode='time'
                          is24Hour={false}
                          display="spinner"
                          onChange={(event, sTime) => {
                            setEndTimeTrue(false)
                            let st = sTime.toString()
                            let nst = st.substring(16, 24)
                            var mt= tConvert(nst)
                            setapiEndTime(nst)
                            setEndTime(mt)
                          }}
                        />
                      </View>
                      :
                      <TouchableOpacity style={{ width: '100%', height: '100%', justifyContent: 'center' }}>
                        <Text style={{ alignSelf: 'center', fontSize: 15, color: '#fff' }} onPress={() => { setEndTimeTrue(true) }}>{endTime != '' ? endTime : 'Select Time'}</Text>
                      </TouchableOpacity>

                  }
                </View>
              </View>

              {/* </View> */}

              {/* //////////////////////////////////////////////////// */}
              <Text style={{ color: '#fff', fontSize: 15, marginTop: 20 }}>Enter Location</Text>
              <View style={{ width: '100%', height: 47, borderRadius: 28, marginTop: 10 }}>

                <TextInput
                  value={location}
                  onChangeText={(e) => setLocation(e)}
                  placeholder={''}
                  placeholderTextColor="gray"
                  // keyboardType="number-pad"
                  autoCapitalize='none'
                  style={styles.input}
                />
              </View>

              <Text style={{ color: '#fff', fontSize: 15, marginTop: 10 }}>Task Note</Text>
              <View style={{ width: '100%', height: 100, borderRadius: 20, marginTop: 10 }}>

                <TextInput
                  value={tasknote}
                  onChangeText={(e) => setTasknote(e)}
                  placeholder={''}
                  placeholderTextColor="gray"
                  multiline={true}
                  maxLength={500}
                  // keyboardType="number-pad"
                  autoCapitalize='none'
                  style={[styles.input, { height: 100, borderRadius: 20, paddingHorizontal: 15, paddingVertical: 10 }]}
                />

              </View>

              {/* ********************Add button************************** */}
              <TouchableOpacity style={{ marginTop: 20, width: '90%', height: 55, backgroundColor: '#0000ff', alignSelf: 'center', borderRadius: 30, flexDirection: 'row', justifyContent: 'center' }}
                onPress={() => {
                  assignNewTask()
                  // setmodlevisual(false)
                  // setmodlevisual2(true)
                }
                }>
                <Text style={{ color: '#FFF', fontSize: 20, alignSelf: 'center' }}
                  onPress={() => {
                    assignNewTask()

                  }
                  }
                >Assign Task</Text>
              </TouchableOpacity>
              <View style={{ width: 10, height: 20 }} />
              {/* *********************end add button**************************** */}
              <View style={{ width: 100, height: 200 }} />
              {/* </TouchableWithoutFeedback> */}

            </ScrollView>

            {loading ?
              <Activity_Indicator />
              :
              null
            }
          </View>
        </Modal>

        {/* *****************************deactivate************************************* */}
        <Modal
          isVisible={downlodtask}
          swipeDirection="down"
          onSwipeComplete={(e) => {
            setdownlodtask(false)
          }}
          scrollTo={() => { }}
          scrollOffset={1}
          propagateSwipe={true}
          coverScreen={false}
          backdropColor='transparent'
          style={{ justifyContent: 'flex-end', margin: 0, backgroundColor: 'rgba(0,0,0,0.5)' }}
        >
          <View style={{ height: '90%', backgroundColor: MyColor.SUBBOXCOLOR, borderTopLeftRadius: 30, borderTopRightRadius: 30, padding: 20 }}>
            <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>

              <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: '100%' }}>
                <Text style={{ color: Mycolors.BG_COLOR, fontSize: 13, marginVertical: 5, textAlign: 'left', width: '50%', left: 5 }}>Select From Date</Text>
                <Text style={{ color: Mycolors.BG_COLOR, fontSize: 13, marginVertical: 5, textAlign: 'left', width: '50%', left: 10 }}>Select To Date</Text>
              </View>

              <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: '100%' }}>
                {/* <Text style={{ color: '#fff', fontSize: 15, marginTop: 15 }}>Select From Date</Text> */}
                <View style={{ width: '48%', height: 39, borderRadius: 28, marginTop: 10, zIndex: 999 }}>

                  <DatePicker
                    customStyles={{
                      dateInput: { borderColor: 'transparent', },
                      dateText: { color: '#fff' },
                      dateIcon: styles.dateIcon,
                      dateplaceholder: {
                        alignContent: 'flex-start',
                        //left: 80
                      },
                      placeholderText: {
                        fontSize: 15,
                        color: 'gray',
                        marginLeft: '5%',
                        left: -20
                      },
                      zIndex: 999
                    }}

                    androidMode={'spinner'}
                    readOnly={true}
                    style={[styles.datePickerSelectInput]}
                    date={fromdate}
                    mode="date"
                    placeholder={'Select date'}
                    maxDate={new Date()}
                    format='YYYY-MM-DD'
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    iconSource={require('../assets/time-attendace.png')}
                    onDateChange={date => {
                      setfromDate(date)
                    }}
                  />



                </View>

                <View style={{ width: '48%', height: 39, borderRadius: 28, marginTop: 10, zIndex: 999 }}>
                  {/* <Text style={{color:Mycolors.BG_COLOR,fontSize:13,marginVertical:5,left:4}}>Select To Date</Text> */}
                  <DatePicker
                    customStyles={{
                      dateInput: { borderColor: 'transparent', },
                      dateText: { color: '#fff' },
                      dateIcon: styles.dateIcon,
                      dateplaceholder: {
                        alignContent: 'flex-start',

                      },
                      placeholderText: {
                        fontSize: 15,
                        color: 'gray',
                        marginLeft: '5%',
                        left: -20
                      },
                      zIndex: 999
                    }}

                    androidMode={'spinner'}
                    readOnly={true}
                    style={[styles.datePickerSelectInput]}
                    date={todate}
                    mode="date"
                    placeholder={'Select date'}
                    maxDate={new Date()}
                    //minDate={todate}
                    format='YYYY-MM-DD'
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    iconSource={require('../assets/time-attendace.png')}
                    onDateChange={date => {
                      settoDate(date)
                    }}
                  />
                </View>

              </View>
{taskdata !=null ?
<View style={{width:'95%',alignSelf:'center',flexDirection:'row',alignItems:'center',marginTop:15}}>
<Text style={{color:'#fff',fontSize:12,fontWeight:'bold'}}>Total Working Time :- </Text>
<Text style={{color:'#fff',fontSize:11}}>{totalWorkingTime}</Text>
</View>
: null
}
              <View>
                <FlatList
                  showsHorizontalScrollIndicator={false}
                  contentContainerStyle={{ alignItems: 'center' }}
                  data={taskdata}
                  //   keyExtractor={item => item.id}
                  renderItem={({ item, index }) => {
                    return (
                      <View style={{ width: dimensions.SCREEN_WIDTH - 30, backgroundColor: MyColor.BOXCOLOR, borderRadius: 10, padding: 15, marginVertical: 10, justifyContent: 'center' }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                          <Image source={index % 2 == 0 ? require('../assets/rgb.png') : require('../assets/fixcer.png')} style={{ width: 35, height: 35, alignSelf: 'center', zIndex: -999 }} resizeMode='cover'></Image>
                          <View>
                            <Text style={{ color: '#FFF', left: 12, fontSize: 18 }}>{item.project_name + '(' + item.total_time + ')'}</Text>
                            <Text style={{ color: '#FFF', marginLeft: 12, fontSize: 12, marginVertical: 6 }}>Job Type : {item.task_type}</Text>
                          </View>
                        </View>
                        <View style={{ width: '100%', height: 2, backgroundColor: MyColor.SUBBOXCOLOR, marginVertical: 10 }} />
                        <Text style={{ color: 'rgba(256,256,256,0.7)', fontSize: 12 }}>Task Notes: {item.note}</Text>

                        <View style={{ width: '100%', height: 18, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 10 }}>
                          <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'white',lineHeight:15 }}>Task Status: </Text>
                          <Text style={{ fontSize: 13, fontWeight: 'bold', color: 'white',lineHeight:15 }}>{item.task_status}</Text>
                        </View>

                        <View style={{ width: '100%', height: 18, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 6 }}>
                          <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'white',lineHeight:15 }}>Check In Time: </Text>
                          <Text style={{ fontSize: 13, fontWeight: 'bold', color: 'white',lineHeight:15 }}>{item.check_in}</Text>
                        </View>

                        <View style={{ width: '100%', height: 18, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 6 }}>
                          <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'white' ,lineHeight:15}}>Check Out Time: </Text>
                          <Text style={{ fontSize: 13, fontWeight: 'bold', color: 'white',lineHeight:15 }}>{item.check_out}</Text>
                        </View>
                      </View>
                    )
                  }}
                />
              </View>
              {taskdata?
              <View>
              <TouchableOpacity style={{
                marginTop: 20, width: '100%', height: 45, backgroundColor: '#0000ff',
                alignSelf: 'center', borderRadius: 30, flexDirection: 'row', justifyContent: 'center'
              }}
              onPress={() => {
                DownloadLink()
              }}>
                <Text style={{ color: '#FFF', fontSize: 13, alignSelf: 'center' }}
                  // onPress={() => {
                  //   DownloadLink()
                  // }}
                >Download Report</Text>
              </TouchableOpacity>
            </View>
            : null
              }
              <View style={{ width: 100, height: 100 }} />



              {/* ********************Add button************************** */}


              <View style={{ width: 100, height: 150 }} />
            </KeyboardAwareScrollView>
            {loading ?
              <Activity_Indicator />
              :
              null
            }
            <View>
              <TouchableOpacity style={{
                marginTop: 20, width: '100%', height: 45, backgroundColor: '#0000ff',
                alignSelf: 'center', borderRadius: 30, flexDirection: 'row', justifyContent: 'center'
              }}
              onPress={() => {
                GetApi()
              }}>
                <Text style={{ color: '#FFF', fontSize: 13, alignSelf: 'center' }}
                  // onPress={() => {
                  //   DownloadeApi()
                  // }}
                >Get Report</Text>
              </TouchableOpacity>
            </View>


          </View>
        </Modal>
        {/* *****************************removeWorker************************************* */}


        {loading ?
          <Activity_Indicator />
          :
          null
        }

      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: MyColor.BGCOLOR
  },
  button: {
    backgroundColor: 'blue',
    padding: 10,
    borderRadius: 15,
    width: dimensions.SCREEN_WIDTH * 0.8,
    alignItems: 'center'
  },
  input: {
    paddingLeft: 15,
    height: 45,
    width: '100%',
    fontSize: 17,
    borderColor: null,
    backgroundColor: '#34333a',
    borderRadius: 10,
    color: '#fff'

  },
  dateIcon: {
    width: 20,
    height: 18,
    marginRight: 20
  },
  datePickerSelectInput: {
    height: 39,
    width: '100%',
    fontSize: 10,
    borderColor: null,
    backgroundColor: '#34333a',
    borderRadius: 10,
    color: '#fff',
    paddingLeft: 20
  }
})

export default WorkerProfile;
