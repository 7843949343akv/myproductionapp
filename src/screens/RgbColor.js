import React, { useState } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Text,
  ScrollView,
  Dimensions,
  View,
  TextInput,
  Alert,
  Image,
  Platform,
} from 'react-native';
import Modal from 'react-native-modal';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Header from '../Component/Header'
var width = Dimensions.get('window').width
var height = Dimensions.get('window').height
var tinycolor = require("tinycolor2");
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {  useSelector, useDispatch } from 'react-redux';
import * as types  from '../redux/types';
import Activity_Indicator from '../Component/Activity_Indicator';
import Toast from 'react-native-simple-toast';
import { dimensions, MyColor,urls } from '../utils/constants';
import {add_task,save_favorite_color,get_attendance_detail,requestPostApi,requestGetApi} from '../WebApis/Service'
// var Slider = require('react-native-slider');
import Slider from '@react-native-community/slider'

const RgbColor = (props) => {
  const token  = useSelector(state => state.user.token)
  const [loading, setLoading] = useState(false);
  const [redColor, setredColor] = useState(0);
  const [greenColor, setgreenColor] = useState(0);
  const [blueColor, setblueColor] = useState(0);
  const [modlevisual, setmodlevisual] = useState(false);
  const [sv, setsv] = useState(0);
  const[title,settitle]=useState('')

  const color1 = tinycolor({ r: redColor, g: greenColor, b: blueColor });
  const hexaColorCode = color1.toHexString();
  const MySaveFavourite_Click=()=>{
    if(token){
      setmodlevisual(true)
    }else{
      props.navigation.navigate('Login')
    }
  }

const In_Modle_MyFavorate_Click=async()=>{
  setLoading(true)
  let formdata = new FormData();
    formdata.append("color_code",hexaColorCode);
    formdata.append("title",title);
  const{responseJson,err}  = await requestPostApi(save_favorite_color,formdata,'POST',token) 
  setLoading(false)
  if(err==null){
      if(responseJson.status){
        Toast.show(responseJson.message);
        setmodlevisual(false)
      }else{
        Toast.show(responseJson.message);
      }
  }else{
    Toast.show(err);
  }
     
}

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <Header Lable="RGB COLOR" Nav={() => props.navigation.goBack()} />
      {/* <ScrollView> */}
        <View style={{ width: dimensions.SCREEN_WIDTH - 20, alignSelf: 'center', height: dimensions.SCREEN_HEIGHT * 14 / 100, backgroundColor: hexaColorCode, borderRadius: 20, marginTop: 20, justifyContent: 'center' }}>
          <Text style={{ color: MyColor.TEXTCOLOR, alignSelf: 'center', fontSize: 22, fontWeight: 'bold' }}>{hexaColorCode}</Text>
        </View>
{/* 
<View style={{width:'85%',height:50,alignSelf:'center',top:-20}}>
<Image source={require('../assets/rgbscreenshot.png')} style={{ width: '100%', height: 50,borderRadius:10}}></Image>
</View> */}

<View style={{backgroundColor:'#1e1e1e',marginTop:15,marginHorizontal:20,paddingVertical:20,borderRadius:20}}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: '9%', marginTop: 10 }}>
          <View style={{ width: 60, height: 40, backgroundColor: MyColor.SUBBOXCOLOR, borderRadius: 10, justifyContent: 'center' }}>
          <TextInput
                value={redColor}
                onChangeText={(e) => {
                  e==''?setredColor(parseInt(0))
                  :
                  setredColor(parseInt(e))}}
                placeholder={redColor.toString()} 
                placeholderTextColor="#bbbbbb" 
                keyboardType="number-pad"
                autoCapitalize = 'none'
                editable = {false}
                maxLength={3}
                style={{width:60,height:45,color:'#FFF',textAlign:'center'}}
              />
          </View>

          <View style={{ width: 60, height: 40, backgroundColor: MyColor.SUBBOXCOLOR, borderRadius: 10, justifyContent: 'center' }}>
            <TextInput
                value={greenColor}
                onChangeText={(e) => {
                  e==''?setgreenColor(parseInt(0))
                  :
                  setgreenColor(parseInt(e))}}
                placeholder={greenColor.toString()}
                placeholderTextColor="#bbbbbb"
                keyboardType="number-pad"
                autoCapitalize = 'none'
                editable = {false}
                maxLength={3}
                style={{width:60,height:45,color:'#FFF',textAlign:'center'}}
              />
          </View> 

          <View style={{ width: 60, height: 40, backgroundColor: MyColor.SUBBOXCOLOR, borderRadius: 10, justifyContent: 'center' }}>
            <TextInput
                value={blueColor}
                onChangeText={(e) => {
                  e==''?setblueColor(parseInt(0))
                  :
                  setblueColor(parseInt(e))}}
                placeholder={blueColor.toString()}
                placeholderTextColor="#bbbbbb"
                keyboardType="number-pad"
                autoCapitalize = 'none'
                editable = {false}
                maxLength={3}
                style={{width:60,height:45,color:'#FFF',textAlign:'center'}}
              />
          </View>

        </View>

        <View style={{marginTop: 15,marginLeft:'14%',transform:[{rotate: "-90deg"}],alignContent:'space-between',width:250,height:240,justifyContent:'space-between'}}>
         <Slider
          maximumValue={255}
          minimumValue={0}
          thumbTintColor={"red"}
          style={{width:250,height:20}}
          minimumTrackTintColor={"red"}
          maximumTrackTintColor={MyColor.SUBBOXCOLOR}
          step={1}
          value={redColor} 
          onValueChange={
            (value) => value==setredColor(value)
          }
        />
        <Slider
          maximumValue={255}
          minimumValue={0}
          thumbTintColor={"green"}
          style={{width:250,height:20,top:Platform.OS=='android'? 0: 0}}
          minimumTrackTintColor={"green"}
            maximumTrackTintColor={MyColor.SUBBOXCOLOR}
          step={1}
          value={greenColor}
          onValueChange={
            (value) => setgreenColor(value)
          }
        />
         <Slider
          maximumValue={255}
          minimumValue={0}
          thumbTintColor={'blue'}
          style={{width:250,height:20,top:Platform.OS=='android'? 0: 0}}
          minimumTrackTintColor={"blue"}
          maximumTrackTintColor={MyColor.SUBBOXCOLOR}
          step={1}
          value={blueColor}
          onValueChange={
            (value) =>  setblueColor(value)
          }
        />
  
        </View>

        </View>
        {/* ********************************************************   */}

        <TouchableOpacity style={{ width: '75%', height: 45, backgroundColor: '#f64d50', alignSelf: 'center', borderRadius: 30, flexDirection: 'row', justifyContent: 'center', marginTop: 10 }} onPress={() => { MySaveFavourite_Click() }}>
          <Image source={require('../assets/ic-favourite.png')} style={{ width: 27, height: 23, alignSelf: 'center', marginRight: 10 }} resizeMode='cover'></Image>
          <Text style={{ color: '#FFF', fontSize: 16, alignSelf: 'center' }}>SAVE AS FAVORITES</Text>
        </TouchableOpacity>
      {/* </ScrollView> */}

      <Modal
        isVisible={modlevisual}
        swipeDirection="down"
        onSwipeComplete={(e) => {
          setmodlevisual(false)
        }}
          scrollTo={() => {}}
          scrollOffset={1}
          propagateSwipe={true}
        coverScreen={false}
        backdropColor='transparent'
        style={{ justifyContent: 'flex-end', margin: 0, backgroundColor: 'rgba(0,0,0,0.5)' }}
      >
        <View style={{ height: '60%', backgroundColor: MyColor.SUBBOXCOLOR, borderTopLeftRadius: 30, borderTopRightRadius: 30, padding: 20 }}>
          <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>

            <Text style={{ color: '#FFF', fontSize: 25, fontWeight: 'bold' }}>Save as favorites</Text>
            {/* <Text style={{ color: '#FFF', fontSize: 13, marginTop: 5, }}>Add your favourite fixture for future purposes</Text> */}
            <Text style={{ color: '#FFF', fontSize: 15, marginTop: 25, }}>Color Code</Text>

            <View style={{ width: '100%', height: 47, borderRadius: 28, marginTop: 10 }}>
              <TextInput
                value={hexaColorCode}
               // onChangeText={(e) => setTitle(e)}
                placeholder={''}
                placeholderTextColor="#bbbbbb"
                // keyboardType="number-pad"
                editable={false}
                autoCapitalize='none'
                style={styles.input}
              />
            </View>

            <Text style={{ color: '#FFF', fontSize: 15, marginTop: 25, }}>Enter Title</Text>

            <View style={{ width: '100%', height: 47, borderRadius: 28, marginTop: 10 }}>
              <TextInput
                value={title}
                onChangeText={(e) => settitle(e)}
                placeholder={''}
                placeholderTextColor="#bbbbbb"
                // keyboardType="number-pad"
                // editable={false}
                autoCapitalize='none'
                style={[styles.input,{paddingLeft:20}]}
              />
            </View>
          
            <View style={{ width: '75%', height: 45, backgroundColor: '#0000ff', alignSelf: 'center', borderRadius: 30, flexDirection: 'row', justifyContent: 'center', marginTop: 40 }}  >
              <Image source={require('../assets/ic-favourite.png')} style={{ width: 27, height: 23, alignSelf: 'center', marginRight: 10 }} resizeMode='cover'></Image>
              <Text style={{ color: MyColor.TEXTCOLOR, fontSize: 16, alignSelf: 'center' }} onPress={() => { In_Modle_MyFavorate_Click() }}>Save as favorites</Text>
            </View>

            <Text style={{ color: MyColor.TEXTCOLOR, fontSize: 14, alignSelf: 'center', marginVertical: 10 }} onPress={() => { props.navigation.navigate('AllFavourite') }}>See all recent favorites</Text>

          </KeyboardAwareScrollView>
          { loading ?
        <Activity_Indicator />
        :
        null
        }
        </View>
      </Modal>

      { loading ?
        <Activity_Indicator />
        :
        null
        }

    </SafeAreaView>
  );
};

let styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: MyColor.BGCOLOR,
  },
  input: {
    height: 45,
    width: '100%',
    fontSize: 17,
    borderColor: null,
    backgroundColor: MyColor.HEADERCOLOR,
    borderRadius: 28,
    color: '#fff',
    paddingHorizontal: 10

  },


})

export default RgbColor;
