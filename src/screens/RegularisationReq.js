import React, { useEffect, useState } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Text,
  ScrollView,
  Dimensions,
  View,
  FlatList,
  Image,
} from 'react-native';
import {TouchableOpacity } from 'react-native-gesture-handler';
import Header from '../Component/Header'
import * as types  from '../redux/types';
import Activity_Indicator from '../Component/Activity_Indicator';
import Toast from 'react-native-simple-toast';
import { dimensions, MyColor,urls } from '../utils/constants';
import {  useSelector, useDispatch } from 'react-redux';
import {regularization_request_list,get_worker_list,get_attendance_detail,requestPostApi,requestGetApi,change_status_request} from '../WebApis/Service'

const RegularisationReq = (props) => {
  const token  = useSelector(state => state.user.token)
  const [loading,setLoading]=useState(false)
  const [approved,setApproved]=useState(false)
  const [rejected,setRejected]=useState(false)
  const [data,setData]=useState(null)

useEffect(()=>{
  RegularisationList()
},[])

const RegularisationList=async()=>{
  setLoading(true)
  const{responseJson,err}  = await requestGetApi(regularization_request_list+props.route.params.id,{},'GET',token)
  setLoading(false)
       if(err==null){
            if(responseJson.status){
              setData(responseJson.data)
              
             }else{ 
              setData(null)
              Toast.show(responseJson.message);
             }
         }else{
          Toast.show(err);
         }

}
const ChangedRegularizationStatus=async(mystatus,id)=>{
    setLoading(true)
    let formdata = new FormData();
    formdata.append("status",mystatus);
    formdata.append("regularization_id",id);
    const{responseJson,err}  = await requestPostApi(change_status_request,formdata,'POST',token) 
    setLoading(false)
    if(err==null){
        if(responseJson.status){
            RegularisationList();
            Toast.show(responseJson.message);
        }else{
          Toast.show(responseJson.message);
        }
    }else{
      Toast.show(err);
    }
}
 const MyComponent=(item)=>{
  return (
    <View style={{width:dimensions.SCREEN_WIDTH-20,backgroundColor:MyColor.BOXCOLOR,marginVertical:6,borderRadius:10,paddingHorizontal:10}}>
    <View style={{flexDirection:'row',justifyContent:'space-between',padding:10}}>
    <View style={{flexDirection:'row'}}>
    <Image source={require('../assets/profile.png')} style={{ width: 30, height: 30,right:5}}></Image>
    <Text style={{color:MyColor.TEXTCOLOR,fontSize:18,left:5}}>{item.date}</Text>
    </View>
    <Text style={{color:MyColor.TEXT2COLOR,fontSize:11,top:5}}></Text>
    </View>
<View style={{flexDirection:'row',width:'100%',paddingHorizontal:10}}>
<Text style={{color:MyColor.TEXTCOLOR,fontSize:12}}>Request: </Text>
<Text style={{color:MyColor.TEXT2COLOR,fontSize:12}}> {item.request_type=='1'?'Forgot to check Out':item.request_type=='2'?'Forgot to check Out & check In':'Forgot to check In'}</Text>
</View>

<View style={{flexDirection:'row',width:'90%',paddingHorizontal:10,marginTop:5}}>
<Text style={{color:MyColor.TEXTCOLOR,fontSize:12}}>Reason: </Text>
<Text style={{color:'rgba(250,250,250,0.6)',fontSize:12}}>{item.reason}</Text>
</View>
{
  item.status=='1' ?
<View style={{flexDirection:'row',width:'95%',paddingHorizontal:10,marginTop:5,justifyContent:'space-between',alignSelf:'center'}}>
<View style={{width:'48%',height:35,borderRadius:20,backgroundColor:'green',justifyContent:'center',alignSelf:'center'}}>
<TouchableOpacity onPress={()=>{
  ChangedRegularizationStatus('2',item.regularization_id)
  // setApproved(true)
  }}>
<Text style={{color:'#FFF',alignSelf:'center'}}>{approved ? 'Approved' : 'Approve'}</Text>
</TouchableOpacity>
</View>

<View style={{width:'48%',height:35,borderRadius:20,backgroundColor:'red',justifyContent:'center',alignSelf:'center'}}>
<TouchableOpacity onPress={()=>{
  ChangedRegularizationStatus('3',item.regularization_id)
  // setRejected(true)
  }}>
<Text style={{color:'#FFF',alignSelf:'center'}}>{rejected ? 'Rejected' : 'Reject'}</Text>
</TouchableOpacity>
</View>
</View>
:
item.status=='2' ? 
  <View>
      <View style={{width:'48%',height:35,borderRadius:20,backgroundColor:'green',justifyContent:'center',alignSelf:'center'}}>
      <View>
      <Text style={{color:'#FFF',alignSelf:'center'}}>Approved</Text>
      </View>
      </View>
  </View>
: 
    <View>
      <View style={{width:'48%',height:35,borderRadius:20,backgroundColor:'red',justifyContent:'center',alignSelf:'center'}}>
      <View>
      <Text style={{color:'#FFF',alignSelf:'center'}}>Rejected</Text>
      </View>
      </View>
  </View>
}
<View style={{width:'100%',height:20}} />
    </View>
)

 }

  return (
     <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <Header Lable="Regularization Request" Nav={() => props.navigation.goBack()} />
<ScrollView>
<Text style={{color:MyColor.TEXTCOLOR,fontSize:20,fontWeight:'bold',margin:20,}}>{data !=null ? 'Recent Request' : ''}</Text>
  <View style={{alignSelf:'center'}}>
  { data !=null ?  MyComponent(data[0]) : null}
  </View>
  
  <Text style={{color:MyColor.TEXTCOLOR,fontSize:20,fontWeight:'bold',margin:15,}}>{data !=null ? 'Previous Request'  : ''}</Text>

<View>
          <FlatList
              showsHorizontalScrollIndicator={false}
              // numColumns={2}
              contentContainerStyle={{ alignItems: 'center' }}
              data={data}
              keyExtractor={item => item.regularization_id}
              renderItem={({ item ,index}) =>
               index!=0 ? MyComponent(item) : null
                }
            />
  
  </View>     



  <View style={{width:100,height:100}} />
</ScrollView> 
      { loading ?
        <Activity_Indicator />
        :
        null
        } 
      </SafeAreaView>
   
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
      backgroundColor:MyColor.BGCOLOR
    },
 
})

export default RegularisationReq;
