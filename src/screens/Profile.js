import React, { useEffect, useState } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Text,
  ScrollView,
  View,
  Image,
  TextInput,
  Alert,
} from 'react-native';
import {TouchableOpacity } from 'react-native-gesture-handler';
import { dimensions ,MyColor,urls} from '../utils/constants';
import {  useSelector, useDispatch } from 'react-redux';
import * as types  from '../redux/types';
import AsyncStorage from '@react-native-community/async-storage';
import {onLogoutUser} from '../redux/actions/user_action';
import Modal from 'react-native-modal';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Toast from 'react-native-simple-toast';
import {get_attendance_detail,get_task_list,change_password,requestPostApi,requestGetApi} from '../WebApis/Service'
import Activity_Indicator from '../Component/Activity_Indicator';
import ImageBackground from 'react-native/Libraries/Image/ImageBackground';

const Profile = (props) => {
  const dispatch =  useDispatch();
  const userData  = useSelector(state => state.user.user_details)  
  const token  = useSelector(state => state.user.token)
  const [modlevisual, setmodlevisual] = useState(false);
  const [loading,setLoading]=useState(false)
  const[oldpass,setoldpass]=useState('')
  const[pass,setPass]=useState('')
  const[cpass,setCpass]=useState('')
  const[oldpassView,setoldpassView]=useState(true)
  const[passView,setPassView]=useState(true)
  const[cpassView,setCpassView]=useState(true)
  const MyLogout= async ()=>{
  await AsyncStorage.clear()   
  dispatch(onLogoutUser())
  dispatch({
    type: types.TOKEN,
    user:null
    });
      props.navigation.reset({
      index: 0,
      routes: [{ name: 'Home' }]
        })
} 


const changePass= async ()=>{
  if(oldpass.trim().length == 0 || oldpass =='' || pass=='' || pass.trim().length == 0 || cpass=='' || cpass.trim().length == 0){
    Toast.show('All fields are mandatory');
    // Alert.alert('All are mandatory fields')
  }else if (pass!=cpass){
    Toast.show('Password and confirm password should be same');
    // Alert.alert('Password and confirm password should be same')
  }else{
   let formdata = new FormData();
  
    formdata.append('old_password',oldpass);
    formdata.append('new_password',pass);
    formdata.append('confirm_password',cpass);
setLoading(true)
const{responseJson,err}  = await requestPostApi(change_password,formdata,'POST',token) 
setLoading(false)
if(err==null){
    if(responseJson.status){
      Toast.show(responseJson.message);
      setoldpass('')
      setPass('')
      setCpass('')
    setmodlevisual(false)
    }else{
      Toast.show(responseJson.message);
    }
}else{
  Toast.show(err);
}
  }
}

const Mycomp=(img,lable,action,size)=>{
  return(
   <TouchableOpacity style={{width:'100%',height:dimensions.SCREEN_HEIGHT*8/100,backgroundColor:'#201f28',borderRadius:15,flexDirection:'row',justifyContent:'center',marginBottom:10}} onPress={action}>
   <View style={{backgroundColor:'#282732',width:'25%',height:'100%',borderBottomLeftRadius:15,borderTopLeftRadius:15,borderBottomRightRadius:55,position:'absolute',left:0,justifyContent:'center'}}>
   <Image source={img}style={{width:27,height:size,alignSelf:'center'}}resizeMode='cover'></Image>
   </View>
   <View style={{width:'50%',height:'100%',justifyContent:'center',marginLeft:'10%'}}>
    <Text style={{color:'white'}}>{lable}</Text>
   </View>
   </TouchableOpacity>
  );
};

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.container}>
        {/* ****************Header********************** */}
        <View style={{backgroundColor:MyColor.BOXCOLOR}}>
      <View style={{width:'100%',height:55,backgroundColor:MyColor.HEADERCOLOR,alignSelf:'center',justifyContent:'center',borderBottomLeftRadius:20,borderBottomRightRadius:20}}>
      <View style={{position:'absolute',left:10,top:0,height:55,justifyContent:'center'}}>
      </View>
       <Text style={{fontWeight:'bold',fontSize:18,color:MyColor.TEXTCOLOR,textAlign:'center'}}>Profile</Text>
       </View>
       </View>
        {/* ****************Header end********************** */}
<ScrollView>

{/* <View style={{width:'100%',height:160,backgroundColor:MyColor.BOXCOLOR,top:-15,zIndex:-999,borderBottomLeftRadius:20,borderBottomRightRadius:20}}> */}
<ImageBackground source={require('../assets/profile-bgCover.png')}style={{width:'100%',height:160,alignSelf:'center',overflow:'hidden',borderBottomLeftRadius:20,borderBottomRightRadius:20,top:-15,zIndex:-999}}>


</ImageBackground>

<View style={{width:150,height:150,alignSelf:'center',borderRadius:75,backgroundColor:'#fff',top:-100}}>
<Image source={require('../assets/profile.png')}style={{width:150,height:150,alignSelf:'center',zIndex:999}}resizeMode='cover'></Image>
</View>
<View style={{top:-120}}>
<View style={{width:100,height:25,alignSelf:'center',backgroundColor:'blue',borderRadius:15,justifyContent:'center'}}>
<Text style={{alignSelf:'center',color:MyColor.TEXTCOLOR}}>{userData.user_group==1 ? 'Admin' : 'User'}</Text>
</View>
<Text style={{alignSelf:'center',color:MyColor.TEXTCOLOR,margin:5}}>{userData.name}</Text>
<Text style={{alignSelf:'center',color:MyColor.TEXT2COLOR}}>{userData.email}</Text>
</View>
{/* </View> */}
<View style={{top:-40}}>
{Mycomp(require('../assets/rgb.png'),'Favorites RGB',()=> {props.navigation.navigate('AllFavourite')},27)}
{Mycomp(require('../assets/ic-favourite.png'),'Favorites Fixture',()=> {props.navigation.navigate('FavouriteFixture')},21)}
{Mycomp(require('../assets/key.png'),'Change Password',()=> {setmodlevisual(true)},28)}
{Mycomp(require('../assets/logout.png'),'Logout',()=> {MyLogout()},27)}
</View>

<View style={{width:100,height:100}} />
       </ScrollView>
       {/* *************************** Change Password Modle *****************************   */}

       <Modal
        isVisible={modlevisual}
        swipeDirection="down"
        onSwipeComplete={(e) => {
          setmodlevisual(false)
        }}
          scrollTo={() => {}}
          scrollOffset={1}
          propagateSwipe={true}
        coverScreen={false}
        backdropColor='transparent'
        style={{ justifyContent: 'flex-end', margin: 0, backgroundColor: 'rgba(25,25,25,10)',color:'white' }}
      >
        <View style={{ height: '74%', backgroundColor: MyColor.BOXCOLOR, borderTopLeftRadius: 30, borderTopRightRadius: 30, padding: 20 }}>
          <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>

          <View style={{ alignSelf: 'center' }}>
           <Image source={require('../assets/change-password.png')} style={{ width: 141, height: 122, alignSelf: 'center', zIndex: -999 }} resizeMode='cover'></Image>
          </View>

          <Text style={{ color: MyColor.TEXTCOLOR, fontSize: 25,marginTop:20}}>Change Password</Text>

{/* ********************************************************   */}
<Text style={{color:'#fff',fontSize:15 ,marginTop:15,}}>Old Password</Text>
<View style={{width:'100%',height:47,borderRadius:28,marginTop:10}}>

  <TextInput
        value={oldpass}
        onChangeText={(e) => setoldpass(e)}
        placeholder={''}
        placeholderTextColor="#bbbbbb"
       // keyboardType="number-pad"
        secureTextEntry={oldpassView ? true : false}
        autoCapitalize = 'none'
        style={styles.input}
      />
       <View style={{width:25,height:22,position:'absolute',right:20,top:12}}>
        <TouchableOpacity onPress={()=>setoldpassView(!oldpassView)}>
         <Image source={oldpassView ? require('../assets/eye.png') : require('../assets/eyes.png')}style={{width:'100%',height:'100%',alignSelf:'center',zIndex:-999}}resizeMode='cover'></Image>
        </TouchableOpacity>
      </View>
 </View>
 
 {/* ********************************************************   */}
<Text style={{color:'#fff',fontSize:15 ,marginTop:15}}>New Password</Text>
<View style={{width:'100%',height:47,borderRadius:28,marginTop:10}}>

  <TextInput
        value={pass}
        onChangeText={(e) => setPass(e)}
        placeholder={''}
        placeholderTextColor="#bbbbbb"
       // keyboardType="number-pad"
       secureTextEntry={passView ? true : false}
        autoCapitalize = 'none'
        style={styles.input}
      />
       <View style={{width:25,height:22,position:'absolute',right:20,top:12}}>
        <TouchableOpacity onPress={()=>setPassView(!passView)}>
         <Image source={passView ? require('../assets/eye.png') : require('../assets/eyes.png')}style={{width:'100%',height:'100%',alignSelf:'center',zIndex:-999}}resizeMode='cover'></Image>
        </TouchableOpacity>
      </View>
  </View>
{/* ********************************************************   */}
<Text style={{color:'#fff',fontSize:15 ,marginTop:15}}>Confirm Password</Text>
<View style={{width:'100%',height:47,borderRadius:28,marginTop:10}}>

  <TextInput
        value={cpass}
        onChangeText={(e) => setCpass(e)}
        placeholder={''}
        placeholderTextColor="#bbbbbb"
       // keyboardType="number-pad"
       secureTextEntry={cpassView ? true : false}
        autoCapitalize = 'none'
        style={styles.input}
      />
        <View style={{width:25,height:22,position:'absolute',right:20,top:12}}>
        <TouchableOpacity onPress={()=>setCpassView(!cpassView)}>
         <Image source={cpassView ? require('../assets/eye.png') : require('../assets/eyes.png')}style={{width:'100%',height:'100%',alignSelf:'center',zIndex:-999}}resizeMode='cover'></Image>
        </TouchableOpacity>
      </View>
 </View>

 <TouchableOpacity style={{width:'50%',height:50,backgroundColor:'#0000ff',alignSelf:'center',borderRadius:30,flexDirection:'row',justifyContent:'center',marginTop:20,
 shadowColor: 'rgba(256,256,250,0.3)',
 shadowOffset: {
   width: 0,
   height: 3
 },
 shadowRadius: 1,
 shadowOpacity: 1,
 justifyContent: 'center',
 elevation: 5, 
 alignSelf: 'center'
}}
onPress={()=>{changePass()}} >
<Text style={{color:'#FFF',fontSize:20,alignSelf:'center'}}>Change</Text>
</TouchableOpacity>
<View style={{width:100,height:100}} />
        </KeyboardAwareScrollView>
        { loading ?
        <Activity_Indicator />
        :
        null
        }
        </View>
     </Modal>
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{ 
      flex: 1,
   backgroundColor:MyColor.BGCOLOR
    },
    button:{
      backgroundColor:'blue',
      padding:10,
      borderRadius:15,
      width:dimensions.SCREEN_WIDTH * 0.8,
      alignItems:'center'
  },input: {
    paddingLeft: 15,
    paddingRight:50,
    height: 45,
    width:'100%',
    fontSize: 17,
    borderColor: null,
    backgroundColor: '#34333a',
    borderRadius:28,
    color:'#fff'

  },
   
})

export default Profile;
