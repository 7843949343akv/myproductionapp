import React, { useEffect, useState } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Text,
  ScrollView,
  Dimensions,
  View,
  TextInput,
  Alert,
  FlatList,
  Image,
} from 'react-native';
import {TouchableOpacity } from 'react-native-gesture-handler';
import { dimensions , MyColor} from '../utils/constants';
import Header from '../Component/Header'
import DropDownPicker from 'react-native-dropdown-picker';


const TrussCalculator = (props) => {
    const[result,setResult]=useState('');
    const [myarr,setMyarr]=useState([{l_cm:'',l_ft:'',n_op:'',open:false,value:'',lable:''}]);
    const[plush,setPlush]=useState('');
    const [open, setOpen] = useState(false);
    const [value, setValue] = useState(null);
    const [lod, setLod] = useState(true);
    const [items, setItems] = useState([
      {label: 'SQ-F24-22           0.72ft/0.22m', value: '0.72'},
      {label: 'SQ-F24-50            1.64ft/0.5m', value: '1.64'},
      {label: 'SQ-F24-75           2.46ft/0.75m', value: '2.46'},
      {label: 'SQ-F24-875          2.87ft/0.875m', value: '2.87'},
      {label: 'SQ-F24-100          3.28ft/1m', value: '3.28'},
      {label: 'SQ-F24-150           4.92ft/1.5m', value: '4.92'},
      {label: 'SQ-F24-200           6.56ft/2m', value: '6.56'},
      {label: 'SQ-F24-215           7.05ft/2.15m', value: '7.05'},
      {label: 'SQ-F24-250           8.20ft/2.5m', value: '8.20'}, 
      {label: 'SQ-F24-275           9.02ft/2.75m', value: '9.02'},
      {label: 'SQ-F24-300          9.84ft/3m', value: '9.84'},
      {label: 'SQ-F24-350           11.48ft/3.5m', value: '11.48'},
      // {label: 'Other', value: 'Other'}
    ]);


const lengthInCm=(i,e,clear)=>{
  console.log('the no calc',e)
if(!clear){
  let mydata=myarr
  mydata[i].l_cm=e
  setMyarr(mydata);  
  setMycalculator(i,e); 
  var tempPlush=plush
  setPlush(tempPlush+1)
}else{ 
  let mydata=myarr  
  mydata[i].l_cm=e 
  mydata[i].value=e 
  setMyarr(mydata);
  setMycalculator(i,e); 
  var tempPlush=plush
  setPlush(tempPlush-1)
} 
}

const setMycalculator=(i,e)=>{
  console.log('the calc',e)
  let mydata=myarr
  let myIthData=myarr[i].l_cm
  //mydata[i].l_ft=myIthData/30.48   //3.28084
  mydata[i].l_ft=myIthData * 3.28084 

}

const numberOfPic=(i,e,clear)=>{
  if(!clear){
   let mydata=myarr
   mydata[i].n_op=e
  setMyarr(mydata);  
    var tempPlush=plush
    setPlush(tempPlush+1)
  }else{ 
    let mydata=myarr  
    mydata[i].n_op=e 
    setMyarr(mydata);
    var tempPlush=plush
    setPlush(tempPlush-1)
  } 
  }

const mySubmit=()=>{
var res=0
for(let i=0;i<myarr.length;i++)
{
  let lcm=0
  let nop=0
  let lnf=0
  let valu=0
  if(myarr[i].value!='')
  {
    valu= +myarr[i].value
  }
   if(myarr[i].l_cm!=''){
    lcm=myarr[i].l_cm
   }
   if(myarr[i].n_op!='')
   {
     nop=myarr[i].n_op
   }
   if(myarr[i].l_ft!='')
   {
     lnf=myarr[i].l_ft
   }
  // res+=lcm*nop   3.28084
    // res+=lnf
    res+=valu
}
// var resultdata=res*3.28084
setResult(res)

}

const plushClick=()=>{
  var tempPlush=plush
  setPlush(tempPlush+1)
  var coustomarr=myarr
  coustomarr.push({l_cm:'',l_ft:'',n_op:'',open:false,value:'',lable:''})
  setMyarr(coustomarr)
}

const minusClick=(i)=>{
  // const removed = fruits.splice(2, 1);
  var tempminus=plush
  setPlush(tempminus-1)
  var coustomarr=myarr
  // coustomarr.push({l_cm:'',l_ft:'',n_op:'',open:false,value:'',lable:''})
  coustomarr.splice(i, 1);
  setMyarr(coustomarr)
}
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <Header Lable="Truss Calculator" Nav={()=>props.navigation.goBack()}/>
      <ScrollView>
{/* ********************************** */}
<View>

{myarr.map((item,index)=>

<View style={{backgroundColor:'transparent',width:dimensions.SCREEN_WIDTH-20,alignSelf:'center',paddingHorizontal:15,borderRadius:5,marginTop:10,zIndex:-index}}>

    {item.value=='Other' ?
<View style={{width:'100%',height:47,}}>
<Text style={{color:MyColor.TEXTCOLOR,marginBottom:5}}>Length in Meter</Text>
      <TextInput
        value={myarr[index].l_cm}
        onChangeText={(e) => lengthInCm(index,e,false)}
        placeholder={'Enter value'}
        placeholderTextColor="#bbbbbb"
        autoCapitalize = 'none'
        keyboardType="number-pad"
        onKeyPress={(e)=>{lengthInCm(index,e,true)}}
        style={styles.input}
      /> 
 </View>
 :
 <>
 <Text style={{color:MyColor.TEXTCOLOR,marginBottom:5}}>Select Length</Text>
 <View style={{width:'85%',height:60,zIndex:-999,flexDirection:'row'}}>
<DropDownPicker 
      open={item.open}
      value={item.value} 
      items={items}
       setOpen={(ddd)=>{
         var arr=myarr
         arr[index].open=ddd
         setMyarr(arr)
         setLod(!lod)
        }}  
        mode="SIMPLE"
      //  setValue={set_Value} 
      //  setItems={set_Items}
      onChangeValue={(mvalues) => {
       console.log('hiiii',mvalues)
        lengthInCm(0,mvalues,false)
      }}
      onSelectItem={(item) => {
        // console.log(item.value);
        var arr=myarr
        arr[index].value=item.value
        arr[index].lable=item.lable
        setMyarr(arr)
        setLod(!lod)
      }}
      textStyle={{
        fontSize: 15,
        color:'black' 
      }}
      placeholder="Select the value"
      placeholderStyle={{
        color: "grey",
        fontWeight: "bold"
      }} 
      containerStyle={{
        zIndex:999,
      }}
      listMode="MODAL"
      // dropDownContainerStyle={{
      //   backgroundColor: '#fff',
      //   width:'100%',
      //   borderColor:'transparent',
      //   shadowColor: '#000000',
      //   shadowOffset: {
      //     width: 0,
      //     height: 3
      //   },
      //   shadowRadius: 5,
      //   shadowOpacity: 1.0,
      //   elevation: 5
      // }}
      labelStyle={{
        fontWeight: "bold",
        color:'white'
                  }}
      style={{backgroundColor:MyColor.HEADERCOLOR,zIndex:999,borderColor:'transparent',borderRadius:9,zIndex:-999}}
      disableBorderRadius={true}
    />
 <View style={{height:50,width:50,backgroundColor:'transparent',marginLeft:10,borderRadius:10}}>
<TouchableOpacity onPress={()=>{
if(index==myarr.length-1){
  plushClick()
}else{
  minusClick(index)
}

}}>
<Image source={index==myarr.length-1 ? require('../assets/add-card.png'):require('../assets/minus-card.png')}style={{width:50,height:50,alignSelf:'center',zIndex:-999,borderRadius:10}}resizeMode='cover'></Image>
</TouchableOpacity>
 </View>
 </View>
 </>
} 
 
   

</View> 

)

} 

  </View>     

<Text style={{color:'transparent'}}>{plush}</Text>

{/* ********************************** */}

{result != '' ? 
<View style={{backgroundColor:MyColor.BOXCOLOR,width:dimensions.SCREEN_WIDTH-40,alignSelf:'center',paddingVertical:15,borderRadius:15,marginTop:10,flexDirection:'row',justifyContent:'space-between',paddingHorizontal:25,zIndex:-999}}>
<Text style={{color:MyColor.TEXTCOLOR}}>Result (In feets)</Text>
<Text style={{color:MyColor.TEXTCOLOR}}>{parseFloat(result).toFixed(2)}</Text>
</View>
:
null
}


<View style={{zIndex:-999}}>
  <TouchableOpacity style={{width:'75%',height:55,backgroundColor:result !='' ? MyColor.SUBBOXCOLOR :'#0000ff',
alignSelf:'center',borderRadius:30,flexDirection:'row',justifyContent:'center',
marginTop:10}} onPress={()=>mySubmit()}>
<Text style={{color:'#FFF',fontSize:16,alignSelf:'center'}}>SUBMIT</Text>
</TouchableOpacity>
</View>

<View style={{width:100,height:100}} />

</ScrollView>

{/* <TouchableOpacity style={{width:50,height:50,backgroundColor:'blue',borderRadius:40,justifyContent:'center',alignItems:'center',marginLeft:dimensions.SCREEN_WIDTH-70}}
onPress={()=>{plushClick()}}>
<Text style={{color:'white',fontSize:40,top:-2}}>+</Text>
</TouchableOpacity> */}

      </SafeAreaView>
  );
};

let styles = StyleSheet.create({
    container:{
      flex: 1,
      backgroundColor:MyColor.BGCOLOR,
    },
    input: {
        height: 45,
        width:'100%',
        fontSize: 17,
        borderColor: null,
        backgroundColor: MyColor.HEADERCOLOR,
        // borderRadius:28,
        color:'#fff',
        paddingHorizontal:10
    
      },
       
 
})

export default TrussCalculator;



