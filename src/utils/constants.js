import {Dimensions} from 'react-native';



export const urls = {
  base_url: 'https://www.nileprojects.in/my_production_assistant/',
  getSubscriptionPlan:'get-subscription-plan',
  register : 'register',
  login : 'login',
  get_subscription_status : 'get-subscription-status',
  add_worker : 'add-worker',
  save_favorite_color : 'save-favorite-color',
  remove_favorite_color : 'remove-favorite-color',
  favorite_color_list : 'favorite-color-list',
  employee_list : 'employee-list',
  my_profile :'my-profile',
  change_status_of_worker : 'change-status-of-worker',
  
  regularization_request : 'regularization-request',
  regularization_request_list : 'regularization-request-list',
  delete_regularization_request : 'delete-regularization-request',
  change_status_request : 'change-status-request',
  check_in_and_check_out : 'check-in-and-check-out',
  get_worker_list : 'get-worker-list',
  worker_attendance_list : 'worker-attendance-list',
};


export const dimensions = {
  SCREEN_WIDTH : Dimensions.get('window').width,
  SCREEN_HEIGHT : Dimensions.get('window').height
};
export const MyColor = {
  BGCOLOR : '#15141b',
  BOXCOLOR : '#201f28',
  SUBBOXCOLOR:'#282732',
  TEXTCOLOR :'#FFF',
  HEADERCOLOR:'#34333a',
  BUTTONCOLOR:'#0000ff',
  TEXT2COLOR:'rgba(256,256,256,0.2)',
};


