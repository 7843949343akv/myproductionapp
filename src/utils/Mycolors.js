import {useColorScheme,Dimensions} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';

// const isDarkMode = useColorScheme() === 'dark';
export const Mycolors = {
    BG_COLOR : '#fff',
    TEXT_COLOR : '#3e5869',
    InputBoxColor:'rgba(100,100,100,0.5)',
    HomeScreenBoxColor:'rgba(256,130,150,0.1)',
    THEME_COLOR:"#fff6e6",
    ORANGE:"#ff8c00",
    BLUE_DARK:"#3e5869",
    DrawerBGcolor:'#fff6e6',
    GrayColor:'#808080',
  };
  export const dimensions = {
    SCREEN_WIDTH : Dimensions.get('window').width,
    SCREEN_HEIGHT : Dimensions.get('window').height
  };
// const fun=(props)=>{
//   const isDarkMode = useColorScheme() === 'dark';
//   const  BG_COLOR : isDarkMode ? Colors.darker : Colors.lighter,
//   const  TEXT_COLOR : isDarkMode ? '#FFF' : '#000'
// }


export const bgcolor = '#fff'
export const textcolor = '#000'