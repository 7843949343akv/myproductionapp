import * as types  from '../types';
import Axios from 'axios';
import {dimensions, MyColor,urls} from '../../utils/constants'
import { Platform } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

export const SignUp = (name, email, password) => dispatch =>
    new Promise((resolve ,reject) =>{
        dispatch({
            type: types.LOADING,
            isLoading:true
        });
        let formdata = new FormData();
        formdata.append("name",name);
        formdata.append("email",email);
        formdata.append("password",password);
        Axios.post(urls.base_url+urls.register,formdata)
        .then(response => {
                              dispatch({
                                        type: types.LOADING,
                                        isLoading:false
                                    });
                                    if(response.data.status){
                                       AsyncStorage.setItem("user",JSON.stringify(response.data.data));
                                       AsyncStorage.setItem("token",JSON.stringify(response.data.token));
                                      
                                         dispatch({
                                            type:types.SAVE_USER_RESULTS,
                                            user:response.data.data
                                               })
                                               dispatch({
                                                type:types.TOKEN,
                                                user:response.data.token
                                            })
                                    }
                                    resolve(response)
                                })
                                    .catch(error => {
                                        dispatch({
                                            type: types.LOADING,
                                            isLoading:false
                                        });
                                        reject(error)
                                    })
      
 })

 
    export const myProfile = (token) => dispatch =>
    new Promise((resolve ,reject) =>{
        dispatch({
            type: types.LOADING,
            isLoading:true
        });
        fetch(urls.base_url+urls.my_profile, {
            method: 'get',
             headers: {
              'Content-Type': 'multipart/form-data',
              'Accept':'application/json',
              'Authorization': 'Bearer '+ token
            },
          }).then((responseJson) => responseJson.json())
                .then((responseJson) => {
                              dispatch({
                                        type: types.LOADING,
                                        isLoading:false
                                    });
                                    resolve(responseJson)
                                })
                                    .catch(error => {
                                        console.log("error",error);
                                        dispatch({
                                            type: types.LOADING,
                                            isLoading:false
                                        });
                                        reject(error)
                                    })
    
    })

export const getAsyncStorage = (user) => {

    return {
        type:types.SAVE_USER_RESULTS,
        user:user
    }
    
}
export const onLogoutUser = () => {
    return {
        type:types.LOGOUT_USER,
        user:null
    }
}

export const saveUserResult = (user) => {

    return {
        type:types.SAVE_USER_RESULTS,
        user:user
    }
}
export const saveUserToken = (token) => {

    return {
        type:types.TOKEN,
        user:token
    }
}

export const setLoading = (lodings) => {

    return {
        type:types.LOADING,
        isLoading:lodings
    }
}

export const setEmpolyeeList = (data) => {

    return {
        type:types.EMPLIST,
        emp_list:data
    }
}


