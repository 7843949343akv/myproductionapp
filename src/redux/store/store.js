import { createStore,combineReducers,applyMiddleware } from "redux";
import common_reducer from "../reducers/common_reducer";
import user_reducer from "../reducers/user_reducer";
import thunk from 'redux-thunk';


const reducer = combineReducers({
  common : common_reducer,
  user : user_reducer
  })

const store = createStore(reducer,applyMiddleware(thunk));


export default store;





